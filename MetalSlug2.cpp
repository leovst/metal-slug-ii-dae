//-----------------------------------------------------------------
// Game File
// C++ Source - MetalSlug2.cpp - version v2_13 jan 2014 
// Copyright Kevin Hoefman - kevin.hoefman@howest.be
// http://www.digitalartsandentertainment.be/
//-----------------------------------------------------------------

//-----------------------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------------------

//-----------------------------------------------------------------
// Include Files
//-----------------------------------------------------------------
#include "MetalSlug2.h"																				

#include "GameInfo.h"
#include "Mission1.h"
#include "HUD.h"
#include "BitFont.h"

//-----------------------------------------------------------------
// Defines
//-----------------------------------------------------------------
#define GAME_ENGINE (GameEngine::GetSingleton())
#define GAME_INFO (GameInfo::GetSingleton())
#define BITFONT (BitFont::GetSingleton())

//-----------------------------------------------------------------
// MetalSlug2 methods																				
//-----------------------------------------------------------------

MetalSlug2::MetalSlug2()
	:m_IsResetting(false)
	,m_IsGameOver(false)
	,m_PlayTimer(0.0)
	,m_DeltaTime(0.0)
	,m_Speed(1.0)
{
	// Nothing to create
}

MetalSlug2::~MetalSlug2()																						
{
	// Nothing to delete
}

void MetalSlug2::GameInitialize(HINSTANCE hInstance)			
{
	// Set the required values
	AbstractGame::GameInitialize(hInstance);
	GAME_ENGINE->SetTitle("MetalSlug2 - Vansteenkiste Leo - 1DAE6");					
	GAME_ENGINE->RunGameLoop(true);
	GAME_ENGINE->SetBitmapInterpolationModeNearestNeighbor();

	// Set the optional values
	GAME_ENGINE->SetWidth(GAMEWIDTH*GAMESCALE);
	GAME_ENGINE->SetHeight(GAMEHEIGHT*GAMESCALE);
	GAME_ENGINE->SetKeyList(String("DIMOPR") + (TCHAR) VK_ESCAPE);
}

void MetalSlug2::GameStart()
{
	// Reset & Game over
	m_BmpMetalSlug2Ptr = new Bitmap(IDB_METALSLUG2);
	m_BmpGameOverPtr = new Bitmap(IDB_GAMEOVER);
	m_MP3GameOverPtr = new Audio(IDR_GAMEOVER, "MP3");

	// Create Managers
	GAME_INFO->AttachResourceManager(new ResourceManager());
	GAME_INFO->AttachObjectManager(new ObjectManager());

	// Set Mission 1
	Environment *missionPtr = new Mission1();
	GAME_INFO->AttachEnvironment(missionPtr);

	// Make Hero
	GAME_INFO->AttachHero(new Hero());

	// HUD
	GAME_INFO->AttachHUD(new HUD());

}

void MetalSlug2::GameEnd()
{
	// Bitmaps
	delete m_BmpMetalSlug2Ptr;
	m_BmpMetalSlug2Ptr = nullptr;
	delete m_BmpGameOverPtr;
	m_BmpGameOverPtr = nullptr;

	// Audio
	delete m_MP3GameOverPtr;
	m_MP3GameOverPtr = nullptr;

	// Everything
	GAME_INFO->Release();
	BITFONT->Release();
}

void MetalSlug2::KeyPressed(TCHAR cKey)
{
	if (m_IsResetting)
	{}
	else if (m_IsGameOver)
	{
		switch (cKey)
		{
		case 'R':
			ResetGame();
			m_MP3GameOverPtr->Stop();
			break;
		case VK_ESCAPE:
			GAME_ENGINE->QuitGame();
			break;
		}
	}
	else
	{
		switch (cKey)
		{
		case 'D':
			GAME_INFO->ToggleDebug();
			break;
		case 'I':
			GAME_INFO->GetHero()->ToggleInvincibility();
			break;
		case 'M':
			GAME_INFO->GetResourceManager()->ToggleMuteAudio();
			break;
		case 'O':
			m_Speed /= 2;
			break;
		case 'P':
			if (m_Speed < 8)
				m_Speed *= 2;
			break;
		case 'R':
			ResetGame();
			break;
		case VK_ESCAPE:
			GAME_ENGINE->QuitGame();
			break;
		}
	}
}

void MetalSlug2::GameTick(double deltaTime)
{
	// Reset Tick
	if (m_IsResetting)
	{
		TickReset();
	}
	// Game over Tick
	else if (m_IsGameOver)
	{
		// Stop Everything
		GAME_INFO->Release();

		// Game over music
		m_PlayTimer += deltaTime;
		if (!m_MP3GameOverPtr->IsPlaying())
		{
			if (m_PlayTimer <= deltaTime)
				m_MP3GameOverPtr->Play();
		}
		else
			m_MP3GameOverPtr->Tick();
	}
	// Default Tick
	else
	{

		// Speed
		deltaTime *= m_Speed;

		//// SlowMo Button
		//if (GAME_ENGINE->IsKeyDown('C'))
		//	deltaTime /= 4;

		GAME_INFO->GetEnvironment()->Tick(deltaTime);
		GAME_INFO->GetObjectManager()->Tick(deltaTime);
		GAME_INFO->GetHero()->Tick(deltaTime);
		GAME_INFO->GetHUD()->Tick(deltaTime);
		GAME_INFO->GetResourceManager()->Tick();

		m_IsGameOver = GAME_INFO->GetGameOver();

		// Debug
		m_DeltaTime = deltaTime;
	}

}

void MetalSlug2::GamePaint(RECT rect)
{
	if (m_IsResetting)
	{
		MATRIX3X2 matScale, matTotal;
		matScale.SetAsScale(GAMESCALE);
		matTotal = matScale;
		GAME_ENGINE->SetTransformMatrix(matTotal);

		GAME_ENGINE->DrawBitmap(m_BmpMetalSlug2Ptr);
	}
	else if (m_IsGameOver)
	{
		// Draw Background
		MATRIX3X2 matScale, matTotal;
		matScale.SetAsScale(GAMESCALE);
		matTotal = matScale;
		GAME_ENGINE->SetTransformMatrix(matTotal);
		GAME_ENGINE->DrawBitmap(m_BmpGameOverPtr);

		// Draw Blinking Text
		if ((int)(m_PlayTimer *2) %2 == 0)
		{
			MATRIX3X2 matTranslate;
			matTranslate.SetAsTranslate(10, 200);
			matTotal = matTranslate;
			GAME_ENGINE->SetTransformMatrix(matTotal);
			BITFONT->PaintString("Press R to restart \nPress ESC to quit", matTotal, 2);
		}
	}
	else
	{

		// PAINT BACKGROUND ENVIRONMENT
		GAME_INFO->GetEnvironment()->PaintBack();

		// PAINT OBJECTS
		GAME_INFO->GetObjectManager()->Paint();

		// PAINT HERO
		GAME_INFO->GetHero()->Paint();

		// PAINT FRONT ENVIRONMENT
		GAME_INFO->GetEnvironment()->PaintFront();

		// HUD
		GAME_INFO->GetHUD()->Paint();

		// DEBUG
		if (GAME_INFO->GetShowDebug())
		{
			// FPS
			GAME_ENGINE->SetColor(COLOR(0,0,0));
			MATRIX3X2 matID;
			GAME_ENGINE->SetTransformMatrix(matID);
			RECT fpsBox = {
				GAME_ENGINE->GetWidth() - (int)(m_DeltaTime*1000),
				0,
				GAME_ENGINE->GetWidth(),
				30};
			GAME_ENGINE->FillRect(fpsBox);
		}
	}

}

void MetalSlug2::TickReset()
{
	// Delete everything
	GAME_INFO->Release();

	// Create everything again
	// Create Managers
	GAME_INFO->AttachResourceManager(new ResourceManager());
	GAME_INFO->AttachObjectManager(new ObjectManager());

	// Set Mission 1
	Environment *missionPtr = new Mission1();
	GAME_INFO->AttachEnvironment(missionPtr);

	// Make Hero
	GAME_INFO->AttachHero(new Hero());

	// HUD
	GAME_INFO->AttachHUD(new HUD());

	m_PlayTimer = 0.0;
	m_DeltaTime = 0.0;
	m_Speed = 1.0;
	m_IsResetting = false;
	m_IsGameOver = false;
}
void MetalSlug2::ResetGame()
{
	m_IsResetting = true;
}



