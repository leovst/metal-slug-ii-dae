#pragma once
//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//-----------------------------------------------------
// Include Files
//-----------------------------------------------------
#include "GameEngine.h"

#include "EnvironmentObject.h"

// Forward Declarations
class Sprite;

//-----------------------------------------------------
// DestroyableBuilding Class									
//-----------------------------------------------------
class DestroyableBuilding : public EnvironmentObject
{
public:
	DestroyableBuilding(DOUBLE2 pos);				// Constructor
	virtual ~DestroyableBuilding();		// Destructor

	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------

	void Tick(double deltaTime);
	void Paint();

	void Hit(int strength);

private: 

	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------

	// Paints
	//-------------------------------------------------

	void PaintDusts(double length);


	//-------------------------------------------------
	// Datamembers								
	//-------------------------------------------------

	// Behaviour
	int m_Strength;

	static const int 
		STRENGTH_COMPLETE = 15,
		STRENGTH_STATE2 = 10,
		STRENGTH_STATE3 = 5;

	// Bitmaps
	Bitmap* m_BmpBuildingPtr;

	Sprite* m_BuildingSpritePtr, *m_DustSpritePtr;

	static const int DUSTS = 10;
	DOUBLE2 m_DustArr[DUSTS];

	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	DestroyableBuilding(const DestroyableBuilding& yRef);									
	DestroyableBuilding& operator=(const DestroyableBuilding& yRef);	
};

 
