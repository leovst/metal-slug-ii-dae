//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//---------------------------
// Includes
//---------------------------
#include "Vehicle.h"

#include "MetalSlug2.h"
#include "GameInfo.h"
#include "Sprite.h"

#include "HeroBehaviour.h"
#include "EnvironmentObject.h"

#include <string>
using namespace std;

//---------------------------
// Defines
//---------------------------
#define GAME_ENGINE (GameEngine::GetSingleton())
#define GAME_INFO (GameInfo::GetSingleton())

//---------------------------
// Constructor & Destructor
//---------------------------

Vehicle::Vehicle(DOUBLE2 pos, HeroBehaviour *heroBehaviourPtr)
	:ObjectEntity(pos, true)
	,m_HeroBehaviourPtr(heroBehaviourPtr)
	,m_Direction(1)
	,m_FeetOnGround(false)
{
	// SPRITES
	//-----------------------------------------------------

	// Bitmap
	m_BmpVehiclePtr = GAME_INFO->GetResourceManager()->GetBitmap(IDB_MARCOLEGS);

	// Sprites
	string sIniFile("Resources/SpriteInfo_MarcoLegs.ini");
	m_SpritePtrArr[0] = new Sprite(sIniFile, "STILL");
	m_SpritePtrArr[1] = new Sprite(sIniFile, "RUN");
	m_SpritePtrArr[2] = new Sprite(sIniFile, "JUMP");
	m_SpritePtrArr[3] = new Sprite(sIniFile, "FALL");

	// HITREGION
	//-----------------------------------------------------
	m_HitMainPtr->CreateFromRect(-10, -35, 10, 0);
	m_HitMainPtr->SetPos(m_Pos);
}
Vehicle::~Vehicle()
{
	// SPRITES
	//-----------------------------------------------------

	// Sprites
	for (int i = 0; i < SPRITE_AMOUNT; ++i)
	{
		delete m_SpritePtrArr[i];
		m_SpritePtrArr[i] = nullptr;
	}

	// Bitmap
	GAME_INFO->GetResourceManager()->ReleaseBitmap(m_BmpVehiclePtr);
	m_BmpVehiclePtr = nullptr;
}

//---------------------------
// Methods - Member functions
//---------------------------

void Vehicle::Tick(double deltaTime)
{
	// Default Velocity
	SetVelocity(0, m_Velocity.y);

	// Only if hero is alive
	if (!GAME_INFO->GetHero()->HasDied())
	{
		// Movement
		HeroBehaviour::MovementState movementState = m_HeroBehaviourPtr->GetMovementState();
		Hero *heroPtr = GAME_INFO->GetHero();

		switch (movementState)
		{
		case HeroBehaviour::MovementState::STILL:
			TickStill(deltaTime);
			break;
		case HeroBehaviour::MovementState::RUN:
			TickRun(deltaTime);
			break;
		case HeroBehaviour::MovementState::JUMP:
			TickJump(deltaTime);
			break;
		case HeroBehaviour::MovementState::FALL:
			TickFall(deltaTime);
			break;
		}

		// Hitregion
		TickHitregion(deltaTime);
	}

	// Inheritance
	ObjectEntity::Tick(deltaTime);

	// Collision
	TickLevelCollisionHandling(deltaTime);

	// Fall
	if (!m_FeetOnGround && m_Velocity.y > m_Gravity.y/5)
		m_HeroBehaviourPtr->SetMovementState(HeroBehaviour::MovementState::FALL);

}
void Vehicle::Paint()
{
	// Inheritance
	ObjectEntity::Paint();

	HeroBehaviour::MovementState movementState = m_HeroBehaviourPtr->GetMovementState();
	HeroBehaviour::BehaviourState behaviourState = m_HeroBehaviourPtr->GetBehaviourState();

	// PAINT
	// Calculate matrices
	MATRIX3X2 matCenter, matFlip, matTranslate, matTotal, matPlayer;
	matFlip.SetAsScale(m_Direction, 1);
	matTranslate.SetAsTranslate(m_Pos);
	matTotal = matFlip * matTranslate;

	// Exceptions: don't paint if
	// - Ducking
	if (m_HeroBehaviourPtr->IsLookingDown() && !(movementState  == HeroBehaviour::MovementState::JUMP || movementState == HeroBehaviour::MovementState::FALL))
		return;
	// - Attacking
	if (behaviourState == HeroBehaviour::BehaviourState::ATTACK  && !(movementState == HeroBehaviour::MovementState::JUMP || movementState == HeroBehaviour::MovementState::FALL))
		return;
	// - Dieing or respawning
	if (behaviourState == HeroBehaviour::BehaviourState::DIE || behaviourState == HeroBehaviour::BehaviourState::RESPAWN)
		return;
	// - Endaction
	if (behaviourState == HeroBehaviour::BehaviourState::ENDACTION)
		return;

	// Set sprite index
	int index = -1;
	switch (movementState)
	{
	case HeroBehaviour::MovementState::STILL:
		index = 0;
		break;

	case HeroBehaviour::MovementState::RUN:
		index = 1;
		break;

	case HeroBehaviour::MovementState::JUMP:
		index = 2;
		break;

	case HeroBehaviour::MovementState::FALL:
		index = 3;
		break;
	}

	// Paint Legs
	if (index >= 0)
	{
		double animationCount = m_HeroBehaviourPtr->GetMovementAnimation();

		// Calculate matrix
		matCenter.SetAsTranslate(m_SpritePtrArr[index]->GetCenter().x, -m_SpritePtrArr[index]->GetSize().y);
		matPlayer = matCenter * matTotal;

		m_SpritePtrArr[index]->PaintSprite(m_BmpVehiclePtr, matPlayer, animationCount, m_Direction, GAME_INFO->GetHero()->IsInvincible());
	}
}

// Ticks	
//-------------------------------------------------

void Vehicle::TickStill(double deltaTime)
{
	// Start to run
	if (GAME_ENGINE->IsKeyDown(VK_RIGHT) || GAME_ENGINE->IsKeyDown(VK_LEFT))
		m_HeroBehaviourPtr->SetMovementState(HeroBehaviour::MovementState::RUN);

	// Jumpstart
	if (GAME_ENGINE->IsKeyDown(VK_SPACE) && m_FeetOnGround)
	{
		m_HeroBehaviourPtr->SetMovementState(HeroBehaviour::MovementState::JUMP);
		m_Velocity.y = -JUMP_SPEED;
	}

	// Start falling
	if (m_Velocity.y > 0.1 && m_HeroBehaviourPtr->GetBehaviourState() != HeroBehaviour::BehaviourState::ENDACTION && !m_FeetOnGround)
		m_HeroBehaviourPtr->SetMovementState(HeroBehaviour::MovementState::FALL);
}
void Vehicle::TickRun(double deltaTime)
{
	// End to run
	if (!GAME_ENGINE->IsKeyDown(VK_RIGHT) && !GAME_ENGINE->IsKeyDown(VK_LEFT))
		m_HeroBehaviourPtr->SetMovementState(HeroBehaviour::MovementState::STILL);

	// Jumpstart
	if (GAME_ENGINE->IsKeyDown(VK_SPACE) && m_FeetOnGround)
	{
		m_HeroBehaviourPtr->SetMovementState(HeroBehaviour::MovementState::JUMP);
		m_Velocity.y = -JUMP_SPEED;
	}

	// Move
	MoveHero(deltaTime);
}
void Vehicle::TickJump(double deltaTime)
{
	// Top of jump: start to fall
	if (m_Velocity.y >= 0)
		m_HeroBehaviourPtr->SetMovementState(HeroBehaviour::MovementState::FALL);

	// Move
	if (GAME_ENGINE->IsKeyDown(VK_RIGHT) || GAME_ENGINE->IsKeyDown(VK_LEFT))
		MoveHero(deltaTime);
}
void Vehicle::TickFall(double deltaTime)
{
	// Check if falling has ended
	if (m_FeetOnGround)
	{
		if (GAME_ENGINE->IsKeyDown(VK_RIGHT) || GAME_ENGINE->IsKeyDown(VK_LEFT))
			m_HeroBehaviourPtr->SetMovementState(HeroBehaviour::MovementState::RUN);
		else
			m_HeroBehaviourPtr->SetMovementState(HeroBehaviour::MovementState::STILL);
	}

	// Move
	if (GAME_ENGINE->IsKeyDown(VK_RIGHT) || GAME_ENGINE->IsKeyDown(VK_LEFT))
		MoveHero(deltaTime);
}
void Vehicle::TickHitregion(double deltaTime)
{
	// Get Hitregion
	RECT2 hit = m_HitMainPtr->GetBounds();
	hit.left -= m_Pos.x;
	hit.top -= m_Pos.y;
	hit.right -= m_Pos.x;
	hit.bottom -= m_Pos.y;

	// Change Hitregion
	if (m_HeroBehaviourPtr->IsLookingDown() 
		&& m_HeroBehaviourPtr->GetMovementState() != HeroBehaviour::MovementState::JUMP 
		&& m_HeroBehaviourPtr->GetMovementState() != HeroBehaviour::MovementState::FALL)
		hit.top = -22;
	else if (m_HeroBehaviourPtr->GetMovementState() == HeroBehaviour::MovementState::RUN 
		|| m_HeroBehaviourPtr->GetMovementState() == HeroBehaviour::MovementState::JUMP 
		|| m_HeroBehaviourPtr->GetMovementState() == HeroBehaviour::MovementState::FALL)
		hit.top = -40;
	else
		hit.top = -35;

	// Set Hitregion again
	m_HitMainPtr->CreateFromRect(hit.left, hit.top, hit.right, hit.bottom);
	m_HitMainPtr->SetPos(m_Pos);
}
// Collision Handling
void Vehicle::TickLevelCollisionHandling(double deltaTime)
{
	// Check down hit (Raycast)
	DOUBLE2 startPoint, vector;
	startPoint = m_Pos;
	vector.y = (m_HitMainPtr->GetBounds().top - m_Pos.y);

	HIT hitArr[1] = {};
	int numHits = GAME_INFO->GetEnvironment()->GetHitRegion()->Raycast(startPoint, vector, hitArr, 1);

	if (numHits > 0 && m_Velocity.y >= 0 && m_Pos.y - hitArr[0].point.y < 10*m_Velocity.y*deltaTime)
	{
		SetPos(m_Pos.x, hitArr[0].point.y);
		SetVelocity(m_Velocity.x, 0);
		m_FeetOnGround = true;
	}
	else
		m_FeetOnGround = false;

	// Check for other objects (vertical)
	for (EnvironmentObject* eoPtr : GAME_INFO->GetObjectManager()->GetEnvironmentObjects())
	{
		if (eoPtr->GetOutBounds() < 40 && !eoPtr->canPass())
		{
			// AABB
			RECT2 heroHit = m_HitMainPtr->GetBounds();
			RECT2 objectHit = eoPtr->GetHitRegion()->GetBounds();
			// Check for collision
			if (!(heroHit.right < objectHit.left || heroHit.left > objectHit.right || 
				heroHit.top > objectHit.bottom || heroHit.bottom < objectHit.top))
			{
				// Get hit rect
				RECT2 hitRect;
				hitRect.left = max(heroHit.left,objectHit.left);
				hitRect.top = max(heroHit.top,objectHit.top);
				hitRect.right = min(heroHit.right,objectHit.right);
				hitRect.bottom = min(heroHit.bottom,objectHit.bottom);

				double hitWidth = hitRect.right - hitRect.left;
				if (hitWidth > 0)
				{
					// Check if right hit
					if (abs(hitRect.left - objectHit.left) < 0.1)
						Translate(-hitWidth, 0);
					// Check if left hit
					if (abs(hitRect.right - objectHit.right) < 0.1)
						Translate(hitWidth, 0);
				}
			}
		}
	}
}


// Movement
//-------------------------------------------------

void Vehicle::MoveHero(double deltaTime)
{
	// Direction: Key input
	if (GAME_ENGINE->IsKeyDown(VK_RIGHT))
		m_Direction = 1;
	else if (GAME_ENGINE->IsKeyDown(VK_LEFT))
		m_Direction = -1;

	// Speed: Exceptions
	double speedModifier = 1; // default
	if (m_HeroBehaviourPtr->IsLookingDown())
	{
		speedModifier = 0.3; // ducked
		if (m_HeroBehaviourPtr->GetBehaviourState() == HeroBehaviour::BehaviourState::SHOOT 
			|| m_HeroBehaviourPtr->GetBehaviourState() == HeroBehaviour::BehaviourState::THROW)
			speedModifier = 0; // duck-action
	}

	// Set Velocity
	switch (m_HeroBehaviourPtr->GetMovementState())
	{
	case HeroBehaviour::MovementState::RUN:
	case HeroBehaviour::MovementState::JUMP:
	case HeroBehaviour::MovementState::FALL:
		SetVelocity(m_Direction * RUN_SPEED * speedModifier, m_Velocity.y);
		break;
	}

	// Limit to bounds of screen
	double boundaryBorder = 15;
	if (m_Pos.x < GAME_INFO->GetEnvironment()->GetCameraPos().x + boundaryBorder)
		SetPos(GAME_INFO->GetEnvironment()->GetCameraPos().x + boundaryBorder, m_Pos.y);
	if (m_Pos.x > GAME_INFO->GetEnvironment()->GetCameraPos().x + MetalSlug2::GAMEWIDTH - boundaryBorder)
		SetPos(GAME_INFO->GetEnvironment()->GetCameraPos().x + MetalSlug2::GAMEWIDTH - boundaryBorder, m_Pos.y);
}

// Getters & Setters	
//-------------------------------------------------

int Vehicle::GetDirection()
{
	return m_Direction;
}
