#pragma once
//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//-----------------------------------------------------
// Include Files
//-----------------------------------------------------
//#include "GameEngine.h"

#include "Projectile.h"

// Forward declarations
class Sprite;

//-----------------------------------------------------
// Bullet Class									
//-----------------------------------------------------
class Bullet : public Projectile
{
public:
	Bullet(DOUBLE2 pos, DOUBLE2 dirVector);				// Constructor
	virtual ~Bullet();		// Destructor

	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------
	
	virtual void Tick(double deltaTime);
	virtual void Paint();

protected: 
	//-------------------------------------------------
	// Datamembers								
	//-------------------------------------------------
	
	DOUBLE2 m_DirectionVector;

private: 
	//-------------------------------------------------
	// Datamembers								
	//-------------------------------------------------

	Sprite *m_BulletSpritePtr;

	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	Bullet(const Bullet& yRef);									
	Bullet& operator=(const Bullet& yRef);	
};

 
