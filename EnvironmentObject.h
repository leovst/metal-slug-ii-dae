#pragma once
//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//-----------------------------------------------------
// Include Files
//-----------------------------------------------------
//#include "GameEngine.h"

#include "ObjectEntity.h"

// Defines
#define EO_DESTROYABLEBUILDING 0

//-----------------------------------------------------
// EnvironmentObject Class									
//-----------------------------------------------------
class EnvironmentObject : public ObjectEntity
{
public:
	EnvironmentObject(DOUBLE2 pos, bool canHit, bool canPass);				// Constructor
	virtual ~EnvironmentObject();		// Destructor

	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------

	virtual void Tick(double deltaTime);
	virtual void Paint();

	virtual void Hit(int strength) = 0;

	// Getters
	//-------------------------------------------------

	bool canHit();
	bool canPass();

protected: 
	//-------------------------------------------------
	// Datamembers								
	//-------------------------------------------------

	// Behaviour
	bool m_CanHit, m_CanPass;

	// Animation
	double m_AnimationCount;


private: 

	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	EnvironmentObject(const EnvironmentObject& yRef);									
	EnvironmentObject& operator=(const EnvironmentObject& yRef);	
};

 
