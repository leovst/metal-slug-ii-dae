//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//---------------------------
// Includes
//---------------------------
#include "HeavyMachineUpgrade.h"

#include "GameInfo.h"
#include "Sprite.h"

#include <string>
using namespace std;

//---------------------------
// Defines
//---------------------------
//#define GAME_ENGINE (GameEngine::GetSingleton())
#define GAME_INFO (GameInfo::GetSingleton())

//---------------------------
// Constructor & Destructor
//---------------------------

HeavyMachineUpgrade::HeavyMachineUpgrade(DOUBLE2 pos, int amountOfBullets)
	:Item(pos, amountOfBullets)
{
	// Sprites
	string sIniFile = "Resources/SpriteInfo_GunUpgrades.ini";
	m_UpgradeSpritePtr = new Sprite(sIniFile, "HEAVYGUN");
	m_DisappearSpritePtr = new Sprite(sIniFile, "DISAPPEAR");

	// Bitmap
	m_BmpGunUpgradesPtr = GAME_INFO->GetResourceManager()->GetBitmap(IDB_GUNUPGRADES);

	// Hitregion
	m_HitMainPtr->CreateFromRect(m_UpgradeSpritePtr->GetCenter().x, -m_UpgradeSpritePtr->GetSize().y, -m_UpgradeSpritePtr->GetCenter().x, 0);
	m_HitMainPtr->SetPos(m_Pos);
}
HeavyMachineUpgrade::~HeavyMachineUpgrade()
{
	// Sprites
	delete m_UpgradeSpritePtr;
	m_UpgradeSpritePtr = nullptr;

	delete m_DisappearSpritePtr;
	m_DisappearSpritePtr = nullptr;

	// Bitmap
	GAME_INFO->GetResourceManager()->ReleaseBitmap(m_BmpGunUpgradesPtr);
	m_BmpGunUpgradesPtr = nullptr;
}

//---------------------------
// Methods - Member functions
//---------------------------

void HeavyMachineUpgrade::Tick(double deltaTime)
{
	// Inheritance
	Item::Tick(deltaTime);

	// Mark for deletion
	if (m_IsTriggered && m_AnimationCount > m_DisappearSpritePtr->GetLength())
		SetDeleteStatus(true);
}
void HeavyMachineUpgrade::Paint()
{
	// Inheritance
	Item::Paint();

	// Calculate Matrices
	MATRIX3X2 matCenter, matTranslate, matTotal;
	if (!m_IsTriggered)
		matCenter.SetAsTranslate(m_UpgradeSpritePtr->GetCenter().x, -m_UpgradeSpritePtr->GetSize().y);
	else
		matCenter.SetAsTranslate(m_DisappearSpritePtr->GetCenter().x, -m_DisappearSpritePtr->GetSize().y);
	matTranslate.SetAsTranslate(m_Pos);
	matTotal = matCenter * matTranslate;

	// Paint
	if (!m_IsTriggered)
		m_UpgradeSpritePtr->PaintSpriteFrame(m_BmpGunUpgradesPtr, matTotal, 0);
	else
		m_DisappearSpritePtr->PaintSprite(m_BmpGunUpgradesPtr, matTotal, m_AnimationCount);

}

void HeavyMachineUpgrade::Hit()
{
	// Upgrade Hero
	GAME_INFO->GetHero()->UpgradeHeavyGun(m_Content);

	m_IsTriggered = true;
}