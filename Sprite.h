#pragma once
//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//-----------------------------------------------------
// Include Files
//-----------------------------------------------------
#include "GameEngine.h"


//-----------------------------------------------------
// Sprite Class									
//-----------------------------------------------------
class Sprite
{
public:
	Sprite(std::string sInfoFile, std::string sBeh, std::string sMov, std::string sDir);//, ResourceManager *resourceManagerPtr);				// Constructor
	Sprite(std::string sInfoFile, std::string sSectionName);
	virtual ~Sprite();		// Destructor

	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------
	
	// Paints
	//-------------------------------------------------

	// Paints animated sprite
	void PaintSprite(Bitmap *bmpSpriteSheetPtr, const MATRIX3X2 &matTotalRef, const double &animationCountRef, int direction = 1, bool transparent = false);
	// Paints only 1 frame
	void PaintSpriteFrame(Bitmap *bmpSpriteSheetPtr, const MATRIX3X2 &matTotalRef, int frameNumber);

	// Getters & Setters
	//-------------------------------------------------

	RECT GetClip();
	void SetClip(RECT clip);

	int GetSpeed();
	void SetSpeed(int speed);

	DOUBLE2 GetOffset();
	DOUBLE2 GetCenter();
	DOUBLE2 GetSize();
	int GetNumberOfFrames();
	double GetLength(); // In seconds
	// Has the Sprite been loaded in
	bool IsLoaded();

private: 
	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------

	// File & String parsing
	//-------------------------------------------------

	bool ReadFile(std::string sInfoFile, std::string sBeh, std::string sMov, std::string sDir);
	bool ReadFile(std::string sInfoFil, std::string sSectionName);
	bool ParseString(std::string input);

	//-------------------------------------------------
	// Datamembers								
	//-------------------------------------------------

	// Animation
	bool m_CompletedLoop;

	// Sprite Info
	RECT m_Clip;
	DOUBLE2 m_Offset;
	int m_NumberOfFrames;
	int m_Columns;
	int m_Speed;
	int m_Length;
	bool m_AddReverse;
	bool m_Reversed;
	bool m_StayLast;


	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	Sprite(const Sprite& yRef);									
	Sprite& operator=(const Sprite& yRef);	
};

 
