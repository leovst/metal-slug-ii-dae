#pragma once
//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//-----------------------------------------------------
// Include Files
//-----------------------------------------------------
#include "GameEngine.h"

#include "ObjectEntity.h"

class Vehicle;
class HeroBehaviour;
class Sprite;

//-----------------------------------------------------
// Hero Class									
//-----------------------------------------------------
class Hero : public ObjectEntity
{
public:
	Hero();					// Constructor
	virtual ~Hero();		// Destructor

	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------

	void Tick(double deltaTime);
	void Paint();

	bool Hit();

	// Getters & Setters	
	//-------------------------------------------------

	HeroBehaviour *GetBehaviour();
	HitRegion *GetHitRegion();

	bool HasHeavyGun();
	void UpgradeHeavyGun(int amountOfBullets);
	void DowngradeGun();
	void AddBombs(int amount);

	bool HasDied();

	int GetAmountOfLives();
	int GetAmountOfBullets();
	int GetAmountOfGrenades();
	int GetAmountOfPoints();
	void AddPoints(int amount);
	int GetAmountOfRescues();
	void AddRescue();

	void ToggleInvincibility();
	bool IsInvincible();

private: 
	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------

	// Ticks	
	//-------------------------------------------------

	// Syncs movement data from Vehicle class
	void TickGetVehicleData();
	void TickInvincibleTimer(double deltaTime);
	void TickBehaviour(double deltaTime);
	void TickKeyInput(double deltaTime);
	void TickShoot(double deltaTime, double length);
	void TickDie(double deltaTime);
	void TickRespawn(double deltaTime, double length);

	// Projectile creation
	//-------------------------------------------------

	void CreateBullet(double deltaTime);
	void CreateGrenade(double deltaTime);

	// Paints	
	//-------------------------------------------------

	void PaintBody();
	void PaintDebug();

	// Respawn
	//-------------------------------------------------
	// Set & resets all vars for new life
	void RespawnHero();

	//-------------------------------------------------
	// Datamembers								
	//-------------------------------------------------

	// General Hero Objects
	HeroBehaviour *m_HeroBehaviourPtr;
	Vehicle *m_VehiclePtr;

	// Visual
	Bitmap *m_BmpHeroPtr;
	int m_Direction;
	double m_ShootingAngle;

	// Behaviour
	double m_ActCounter, m_ActTime;
	int m_ShotsFired;

	// Sprites
	static const int
		NR_BEHAVIOUR = 4,
		NR_MOVEMENT = 4,
		NR_DIRECTION = 3;
	Sprite *m_SpritePtrArr[NR_BEHAVIOUR][NR_MOVEMENT][NR_DIRECTION]; // [Behaviour] - [Movement] - [Direction]
	std::string m_SBehaviourArr[NR_BEHAVIOUR];
	std::string m_SMovementArr[NR_MOVEMENT];
	std::string m_SDirectionArr[NR_DIRECTION];

	Sprite* m_EndActionSpritePtr, *m_DieSpritePtr, *m_Die1SpritePtr, *m_Die2SpritePtr, *m_RespawnSpritePtr;

	// Hero Info
	int m_AmountOfBullets, m_AmountOfGrenades, m_AmountOfLives, m_AmountOfPoints, m_AmountOfRescues;
	bool m_HasHeavyGun, m_IsInvincible;
	double m_InvincibleTimer;

	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	Hero(const Hero& yRef);									
	Hero& operator=(const Hero& yRef);	
};


