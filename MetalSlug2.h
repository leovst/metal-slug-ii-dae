//-----------------------------------------------------------------
// Game File
// C++ Source - MetalSlug2.h - version v2_13 jan 2014 
// Copyright Kevin Hoefman - kevin.hoefman@howest.be
// http://www.digitalartsandentertainment.be/
//-----------------------------------------------------------------

//-----------------------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------
// Include Files
//-----------------------------------------------------------------

#include "Resource.h"	
#include "GameEngine.h"
#include "AbstractGame.h"

// Forward declarations
class BitFont;

//-----------------------------------------------------------------
// MetalSlug2 Class																
//-----------------------------------------------------------------
class MetalSlug2 : public AbstractGame, public Callable
{
public:				
	//---------------------------
	// Constructor(s)
	//---------------------------
	MetalSlug2();

	//---------------------------
	// Destructor
	//---------------------------
	virtual ~MetalSlug2();

	//---------------------------
	// General Methods
	//---------------------------

	void GameInitialize(HINSTANCE hInstance);
	void GameStart();				
	void GameEnd();
	void KeyPressed(TCHAR cKey);
	void GameTick(double deltaTime);
	void GamePaint(RECT rect);

	// -------------------------
	// Static variables
	// -------------------------

	static const int GAMEWIDTH = 304;
	static const int GAMEHEIGHT = 224;
	static const int GAMESCALE = 3;

private:
	//---------------------------
	// Methods
	//---------------------------

	void TickReset();
	void ResetGame();

	// -------------------------
	// Datamembers
	// -------------------------

	// Reset & GameOver
	bool m_IsResetting, m_IsGameOver;
	Bitmap* m_BmpMetalSlug2Ptr, *m_BmpGameOverPtr;
	Audio* m_MP3GameOverPtr;
	double m_PlayTimer;


	// Debug
	double m_DeltaTime;
	double m_Speed;

	// -------------------------
	// Disabling default copy constructor and default assignment operator.
	// If you get a linker error from one of these functions, your class is internally trying to use them. This is
	// an error in your class, these declarations are deliberately made without implementation because they should never be used.
	// -------------------------
	MetalSlug2(const MetalSlug2& tRef);
	MetalSlug2& operator=(const MetalSlug2& tRef);
};
