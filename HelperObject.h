#pragma once
//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//-----------------------------------------------------
// Include Files
//-----------------------------------------------------
#include "GameEngine.h"

#include "ObjectEntity.h"

// Forward declarations
class Sprite;

//-----------------------------------------------------
// HelperObject Class									
//-----------------------------------------------------
class HelperObject : public ObjectEntity
{
public:
	HelperObject(DOUBLE2 pos, Bitmap* bmpPtr, Sprite* spritePtr);				// Constructor
	virtual ~HelperObject();		// Destructor

	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------

	void Tick(double deltaTime);
	void Paint();


	// Getters & Setters
	//-------------------------------------------------

	void Show();

private: 
	//-------------------------------------------------
	// Datamembers								
	//-------------------------------------------------

	// Behaviour
	bool m_IsShown;

	// Animation
	double m_AnimationCount;

	// Sprite
	Sprite *m_SpritePtr;

	// Bitmap
	Bitmap* m_BmpPtr;


	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	HelperObject(const HelperObject& yRef);									
	HelperObject& operator=(const HelperObject& yRef);	
};

 
