//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//---------------------------
// Includes
//---------------------------
#include "DestroyableBuilding.h"

#include "GameInfo.h"
#include "Sprite.h"
#include "Mission1.h"

#include <string>
using namespace std;

//---------------------------
// Defines
//---------------------------
#define GAME_ENGINE (GameEngine::GetSingleton())
#define GAME_INFO (GameInfo::GetSingleton())

//---------------------------
// Constructor & Destructor
//---------------------------

DestroyableBuilding::DestroyableBuilding(DOUBLE2 pos)
	:EnvironmentObject(pos, true, false)
	,m_Strength(STRENGTH_COMPLETE)
{
	// Bitmap
	m_BmpBuildingPtr = GAME_INFO->GetResourceManager()->GetBitmap(IDB_DESTROYABLEBUILDING);

	// Sprites
	string sIniFile = "Resources/SpriteInfo_Mission1.ini";
	m_BuildingSpritePtr = new Sprite(sIniFile, "DESTROYABLEBUILDING");
	m_DustSpritePtr = new Sprite(sIniFile, "DUST");

	// HitRegion
	m_HitMainPtr->CreateFromRect(16, 16, m_BuildingSpritePtr->GetSize().x, m_BuildingSpritePtr->GetSize().y);
	m_HitMainPtr->SetPos(m_Pos);

	// General
	m_AnimationCount = 1;

	for (int i = 0; i < DUSTS; ++i)
		m_DustArr[i] = DOUBLE2(rand()%40, rand()%80);
}
DestroyableBuilding::~DestroyableBuilding()
{
	// Bitmap
	GAME_INFO->GetResourceManager()->ReleaseBitmap(m_BmpBuildingPtr);
	m_BmpBuildingPtr = nullptr;

	// Sprite
	delete m_BuildingSpritePtr;
	m_BuildingSpritePtr = nullptr;

	delete m_DustSpritePtr;
	m_DustSpritePtr = nullptr;
}

//---------------------------
// Methods - Member functions
//---------------------------

void DestroyableBuilding::Tick(double deltaTime) 
{
	// Inheritance
	EnvironmentObject::Tick(deltaTime);
}
void DestroyableBuilding::Paint()
{
	// Set state of destroyedness
	int frameNumber = 0;
	if (m_Strength <= 0)
		frameNumber = 3;
	else if (m_Strength < STRENGTH_STATE3)
		frameNumber = 2;
	else if (m_Strength < STRENGTH_STATE2)
		frameNumber = 1;

	// Calculate matrices
	MATRIX3X2 matTranslate, matTotal;
	matTranslate.SetAsTranslate(m_Pos);
	matTotal = matTranslate;

	// Paint Building
	m_BuildingSpritePtr->PaintSpriteFrame(m_BmpBuildingPtr, matTotal, frameNumber);

	// Paint dust when hit
	PaintDusts(0.25);

	// Inheritance
	EnvironmentObject::Paint();
}

void DestroyableBuilding::Hit(int strength)
{
	m_Strength -= strength;
	m_AnimationCount = 0;
	for (int i = 0; i < DUSTS; ++i)
		m_DustArr[i] = DOUBLE2(rand()%40, rand()%80);

	if (m_Strength <= 0)
	{
		m_CanHit = false;
		m_CanPass = true;
		Mission1* missionPtr = (Mission1*) GAME_INFO->GetEnvironment();
		missionPtr->DestroyBuilding();
	}
}


// Paints
//-------------------------------------------------

void  DestroyableBuilding::PaintDusts(double length)
{
	if (m_AnimationCount > 0.25)
		return;

	// Max amount of dusts
	int max = (int)((length - m_AnimationCount)*(1/length)*DUSTS);
	// Paint each dust on it's location
	for (int i = 0; i < max; ++i)
	{
		MATRIX3X2 matTranslate, matOffset, matTotal;
		matTranslate.SetAsTranslate(m_Pos);
		matOffset.SetAsTranslate(m_DustArr[i] - DOUBLE2(0, m_AnimationCount*50));

		matTotal = matTranslate * matOffset;

		m_DustSpritePtr->PaintSpriteFrame(m_BmpBuildingPtr, matTotal, 0);
	}
}