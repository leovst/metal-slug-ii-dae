# README #

This is the final build of an educational recreation of the game Metal Slug II which I created as an exercise. All images and sounds are left out, as well as the game engine I used.
The original game belongs to SNK Playmore Corp and was only used for academic purpose in this project.

Project page can be found at [my portfolio](http://www.leovst.com/metalslug2)