//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//---------------------------
// Includes
//---------------------------
#include "ObjectManager.h"

#include "MetalSlug2.h"
#include "GameInfo.h"
#include "Environment.h"

// Projectiles
#include "Projectile.h"
#include "Bullet.h"
#include "HeavyBullet.h"
#include "Grenade.h"
#include "Explosion.h"
#include "Sword.h"

// Items
#include "Item.h"
#include "HeavyMachineUpgrade.h"
#include "BombsUpgrade.h"
#include "PointsObject.h"

// NPCs
#include "NPC.h"
#include "EnemyArab.h"
#include "POW.h"

// EO
#include "EnvironmentObject.h"
#include "DestroyableBuilding.h"

#include <fstream>
using namespace std;

//---------------------------
// Defines
//---------------------------
#define GAME_ENGINE (GameEngine::GetSingleton())
#define GAME_INFO (GameInfo::GetSingleton())

#define KIND_PLAYER 0
#define KIND_ITEM 1
#define KIND_NPC 2
#define KIND_EO 3

//---------------------------
// Constructor & Destructor
//---------------------------
ObjectManager::ObjectManager()
{
	// Nothing to create
}
ObjectManager::~ObjectManager()
{
	// Projectiles
	for (Projectile* &projectilePtr : m_ProjectilesPtrArr)
		delete projectilePtr;

	// Items
	for (Item* &itemPtr : m_ItemsPtrArr)
		delete itemPtr;

	// NPCs
	for (NPC* &npcPtr : m_NpcPtrArr)
		delete npcPtr;

	// Environment Objects
	for (EnvironmentObject* &eoPtr : m_EoPtrArr)
		delete eoPtr;
}

//---------------------------
// Methods - Member functions
//---------------------------


void ObjectManager::Tick(double deltaTime)
{
	// Projectiles
	TickProjectiles(deltaTime);

	// Items & Upgrades
	TickItems(deltaTime);

	// NPCs
	TickNPCs(deltaTime);

	// Environment Objects
	for (unsigned int i = 0; i < m_EoPtrArr.size(); ++i)
		m_EoPtrArr[i]->Tick(deltaTime);

	// Key Input
	TickKeyInput();

}
void ObjectManager::Paint()
{
	// NPCs
	for (unsigned int i = 0; i < m_NpcPtrArr.size(); ++i)
		m_NpcPtrArr[i]->Paint();

	// Environment Objects
	for (unsigned int i = 0; i < m_EoPtrArr.size(); ++i)
		m_EoPtrArr[i]->Paint();

	// Projectiles
	for (unsigned int i = 0; i < m_ProjectilesPtrArr.size(); ++i)
		m_ProjectilesPtrArr[i]->Paint();

	// Items & Upgrades
	for (unsigned int i = 0; i < m_ItemsPtrArr.size(); ++i)
		m_ItemsPtrArr[i]->Paint();
}


// Ticks
//-------------------------------------------------

void ObjectManager::TickProjectiles(double deltaTime)
{
	for (unsigned int i = 0; i < m_ProjectilesPtrArr.size(); ++i)
	{
		// Pass Tick
		m_ProjectilesPtrArr[i]->Tick(deltaTime);

		// Check if projectiles are still valid
		if (m_ProjectilesPtrArr[i]->GetDeleteStatus())
		{
			// Start Explosion if grenade ends
			if (m_ProjectilesPtrArr[i]->GetType() == PROJ_GRENADE)
				AddProjectile(PROJ_EXPLOSION, m_ProjectilesPtrArr[i]->GetPos());

			// Delete
			delete m_ProjectilesPtrArr[i];
			m_ProjectilesPtrArr.erase(m_ProjectilesPtrArr.begin() + i);
		}
	}
}
void ObjectManager::TickItems(double deltaTime)
{
	for (unsigned int i = 0; i < m_ItemsPtrArr.size(); ++i)
	{
		// Pass Tick
		m_ItemsPtrArr[i]->Tick(deltaTime);

		// Check if Items are still valid
		if (m_ItemsPtrArr[i]->GetDeleteStatus())
		{
			// Delete
			delete m_ItemsPtrArr[i];
			m_ItemsPtrArr.erase(m_ItemsPtrArr.begin() + i);
		}
	}
}
void ObjectManager::TickNPCs(double deltaTime)
{
	for (unsigned int i = 0; i < m_NpcPtrArr.size(); ++i)
	{
		// Pass Tick
		m_NpcPtrArr[i]->Tick(deltaTime);

		// If dead, spawn new instance when necessary & possible
		if (m_NpcPtrArr[i]->HasDied() 
			&& m_NpcPtrArr[i]->GetAmountToSpawn() > 1 
			&& m_NpcPtrArr[i]->WillTrigger())
		{
			// Get trigger locations
			int triggerS, triggerE;
			m_NpcPtrArr[i]->GetTrigger(triggerS, triggerE);

			// Add NPC
			AddNPC(m_NpcPtrArr[i]->GetType(), 
				DOUBLE2(GAME_INFO->GetEnvironment()->GetCameraPos().x + MetalSlug2::GAMEWIDTH +60, m_NpcPtrArr[i]->GetStartPos().y), 
				triggerS, triggerE, m_NpcPtrArr[i]->GetAmountToSpawn()-1, -1, -1);

			// Disable further spawning from current object
			m_NpcPtrArr[i]->SetAmountToSpawn(0);
		}

		// Check if NPC is still valid
		if (m_NpcPtrArr[i]->GetDeleteStatus())
		{
			delete m_NpcPtrArr[i];
			m_NpcPtrArr.erase(m_NpcPtrArr.begin() + i);
		}
	}
}

void ObjectManager::TickKeyInput()
{
	// Kill all Airstrike
	if (ObjectEntity::GetDebug() && GAME_ENGINE->IsKeyDown('G') && m_ProjectilesPtrArr.size() < 10)
	{
		// Drop 10 Grenades
		DOUBLE2 pos = GAME_INFO->GetEnvironment()->GetCameraPos();
		for (int i = 0; i < 10; ++i)
		{
			pos.x += (MetalSlug2::GAMEWIDTH/11); 
			AddProjectile(2, pos, DOUBLE2(0,-100));
		}
	}
}


// Creation
//-------------------------------------------------

Item* ObjectManager::CreateItem(int type, DOUBLE2 pos, int content)
{
	Item* itemPtr = nullptr;
	switch (type)
	{
	case ITEM_HMU:
		itemPtr = new HeavyMachineUpgrade(pos, content);
		break;
	case ITEM_BOMBPACK:
		itemPtr = new BombsUpgrade(pos, content);
		break;
	case ITEM_REDGEM:
	case ITEM_BLUEGEM:
	case ITEM_YELLOWGEM:
	case ITEM_BANANA:
	case ITEM_APPLE:
	case ITEM_MEATBONE:
	case ITEM_CAT:
	case ITEM_MONKEY:
		itemPtr = new PointsObject(pos, content, type);
		break;
	}
	return itemPtr;
}

// Add objects to arrays
//-------------------------------------------------

void ObjectManager::AddProjectile(int type, DOUBLE2 startPos, DOUBLE2 dirVector)
{
	// Creat Corresponding object
	Projectile* projectilePtr = nullptr;
	switch (type)
	{
	case PROJ_BULLET:
		projectilePtr = new Bullet(startPos, dirVector);
		break;
	case PROJ_HBULLET:
		projectilePtr = new HeavyBullet(startPos, dirVector);
		break;
	case PROJ_GRENADE:
		projectilePtr = new Grenade(startPos, dirVector);
		break;
	case PROJ_EXPLOSION:
		projectilePtr = new Explosion(startPos);
		break;
	case PROJ_SWORD:
		projectilePtr = new Sword(startPos, dirVector);
	}

	// Add to list
	m_ProjectilesPtrArr.push_back(projectilePtr);
}
void ObjectManager::AddItem(int type, DOUBLE2 pos, int content)
{
	// Add to list
	m_ItemsPtrArr.push_back(CreateItem(type, pos, content));
}
void ObjectManager::AddItem(Item* itemPtr)
{
	// Add to list
	m_ItemsPtrArr.push_back(itemPtr);
}
void ObjectManager::AddNPC(int type, DOUBLE2 pos, int triggerS, int triggerE, int amount, int drop, int itemNr, int content)
{
	NPC* npcPtr = nullptr;
	Item* itemPtr = nullptr;
	switch (type)
	{
	case NPC_POWSIT:
	case NPC_POWSTICK:
	case NPC_POWHANG:
		// POW
		npcPtr = new POW(pos, type, CreateItem(itemNr, pos, content));
		break;
	case NPC_ARAB:
		npcPtr = new EnemyArab(pos, amount, drop, triggerS, triggerE);
		break;
	}

	// Add to list
	m_NpcPtrArr.push_back(npcPtr);
}
void ObjectManager::AddEnvironmentObject(int type, DOUBLE2 pos)
{
	EnvironmentObject* eoPtr = nullptr;
	switch (type)
	{
	case EO_DESTROYABLEBUILDING:
		eoPtr = new DestroyableBuilding(pos);
		break;
	}

	// Add to list
	m_EoPtrArr.push_back(eoPtr);
}

// File & String parsing
//-------------------------------------------------

// Read in initialization file
bool ObjectManager::ReadIniFile(string sFileName)
{
	// Read file
	ifstream file(sFileName);

	if (file)
	{
		// Special chars
		string sComment = ";";

		string sLine;

		DOUBLE2 pos;
		int triggerS = -1, triggerE = -1, itemNr = -1, content = 0, amount = 0, drop = 0, kind = -1, type = -1;


		// Loop through file content
		while (!file.eof())
		{
			// Get current line
			getline(file, sLine);

			// Check if line is commented
			if (sLine.find(sComment) == string::npos)
			{

				if (sLine.find('[') != string::npos)
				{
					// Create previous section
					// Player
					if (kind == KIND_PLAYER)
						m_HeroStartPos = pos;
					// Item
					else if (kind == KIND_ITEM && type != -1)
						AddItem(type, pos, content);
					// NPC
					else if (kind == KIND_NPC && type != -1)
						AddNPC(type, pos, triggerS, triggerE, amount, drop, itemNr, content);
					else if (kind == KIND_EO && type != -1)
						AddEnvironmentObject(type, pos);

					// Reset variables
					triggerS = -1; triggerE = -1; itemNr = -1; content = 0; amount = 0; drop = 0; kind = -1; type = -1;


					ParseSectionString(sLine, kind, type);
				}

				// Parse variables
				else if (kind != -1)
					ParseVariableString(sLine, pos, triggerS, triggerE, itemNr, content, amount, drop);
			}
		}
		file.close();

	}
	else 
		return false;

	return true;
}
bool ObjectManager::ParseSectionString(string sLine, int &kind, int &type)
{
	// Player
	if (sLine.find("PLAYER") != string::npos)
		kind = KIND_PLAYER;
	// Items
	else if (sLine.find("ITEM") != string::npos)
	{
		kind = KIND_ITEM;

		// Set current item type
		if (sLine.find("HEAVYMACHINEUPGRADE") != string::npos)
			type = ITEM_HMU;
		else if (sLine.find("BOMBPACK") != string::npos)
			type = ITEM_BOMBPACK;
		else if (sLine.find("GEMRED") != string::npos)
			type = ITEM_REDGEM;
		else if (sLine.find("GEMBLUE") != string::npos)
			type = ITEM_BLUEGEM;
		else if (sLine.find("GEMYELLOW") != string::npos)
			type = ITEM_YELLOWGEM;
		else if (sLine.find("BANANA") != string::npos)
			type = ITEM_BANANA;
		else if (sLine.find("APPLE") != string::npos)
			type = ITEM_APPLE;
		else if (sLine.find("MEATBONE") != string::npos)
			type = ITEM_MEATBONE;
		else if (sLine.find("CAT") != string::npos)
			type = ITEM_CAT;
		else if (sLine.find("MONKEY") != string::npos)
			type = ITEM_MONKEY;
	}

	// Enemies
	else if (sLine.find("NPC") != string::npos)
	{
		kind = KIND_NPC;

		// Set current NPC
		if (sLine.find("POWSIT") != string::npos)
			type = NPC_POWSIT;
		else if (sLine.find("POWSTICK") != string::npos)
			type = NPC_POWSTICK;
		else if (sLine.find("POWHANG") != string::npos)
			type = NPC_POWHANG;
		else if (sLine.find("ARAB") != string::npos)
			type = NPC_ARAB;
	}

	// Environment Objects
	else if (sLine.find("ENVIRONMENT") != string::npos)
	{
		kind = KIND_EO;

		// Set type
		if (sLine.find("DESTROYABLEBUILDING") != string::npos)
			type = EO_DESTROYABLEBUILDING;
	}
	return true;
}
bool ObjectManager::ParseVariableString(string sLine, DOUBLE2 &pos, int &triggerS, int &triggerE, int &itemNr, int &content, int &amount, int &drop)
{
	// Special chars
	string sAssign = "=";

	// Remove all whitespaces
	// http://stackoverflow.com/a/83481
	sLine.erase(remove(sLine.begin(), sLine.end(), ' '), sLine.end());
	sLine.erase(remove(sLine.begin(), sLine.end(), '\t'), sLine.end());

	// Get variable name and value as int
	string::size_type assignLoc = sLine.find_first_of(sAssign);
	if (assignLoc != string::npos)
	{
		string sName = sLine.substr(0, assignLoc);
		int value = stoi( sLine.substr(assignLoc+1, sLine.length()-assignLoc+1) );

		if (sName == "spawnx")
			pos.x = value;
		else if (sName == "spawny")
			pos.y = value;
		else if (sName == "triggerS")
			triggerS = value;
		else if (sName == "triggerE")
			triggerE = value;
		else if (sName == "item")
			itemNr = value;
		else if (sName == "content")
			content = value;
		else if (sName == "amount")
			amount = value;
		else if (sName == "drop")
			drop = value;
	}
	return true;
}


// Getters	
//-------------------------------------------------

DOUBLE2 ObjectManager::GetHeroStartPosition()
{
	return m_HeroStartPos;
}
vector<NPC *> ObjectManager::GetNPCs()
{
	return m_NpcPtrArr;
}
vector<EnvironmentObject*> ObjectManager::GetEnvironmentObjects()
{
	return m_EoPtrArr;
}