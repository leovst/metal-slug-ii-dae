//-----------------------------------------------------------------
// Game File
// C++ Source - TestFont.cpp - version v2_13 jan 2014 
// Copyright Kevin Hoefman - kevin.hoefman@howest.be
// http://www.digitalartsandentertainment.be/
//-----------------------------------------------------------------

//-----------------------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------------------

//-----------------------------------------------------------------
// Include Files
//-----------------------------------------------------------------
#include "TestFont.h"	

#include "GameInfo.h"
#include "ResourceManager.h"
#include "BitFont.h"

#include <string>
#include <sstream>
#include <stdlib.h>
using namespace std;

//-----------------------------------------------------------------
// Defines
//-----------------------------------------------------------------
#define GAME_ENGINE (GameEngine::GetSingleton())
#define GAME_INFO (GameInfo::GetSingleton())
#define BITFONT (BitFont::GetSingleton())

//-----------------------------------------------------------------
// TestFont methods																				
//-----------------------------------------------------------------

TestFont::TestFont()
{
	m_sString = "";
}

TestFont::~TestFont()																						
{
	// nothing to destroy
}

void TestFont::GameInitialize(HINSTANCE hInstance)			
{
	// Set the required values
	AbstractGame::GameInitialize(hInstance);
	GAME_ENGINE->SetTitle("TestFont - Vansteenkiste Leo - 1DAE6");					
	GAME_ENGINE->RunGameLoop(true);
	
	// Set the optional values
	GAME_ENGINE->SetWidth(640);
	GAME_ENGINE->SetHeight(480);
	GAME_ENGINE->SetBitmapInterpolationModeNearestNeighbor();
	//GAME_ENGINE->SetKeyList(String("QSDZ") + (TCHAR) VK_SPACE);
	//GAME_ENGINE->ConsoleCreate();
	//GAME_ENGINE->LockFramerateToVerticalSynchronisation(true);
}

void TestFont::GameStart()
{
	GAME_INFO->AttachResourceManager(new ResourceManager());

	m_TxtInputPtr = new TextBox();
	m_TxtInputPtr->AddActionListener(this);
	m_TxtInputPtr->SetBounds(20, 20, 200, 20);
	m_TxtInputPtr->Show();
}

void TestFont::GameEnd()
{
	delete m_TxtInputPtr;
	m_TxtInputPtr = nullptr;
	GAME_INFO->Release();
}

//void TestFont::MouseButtonAction(bool isLeft, bool isDown, int x, int y, WPARAM wParam)
//{	
//	// Insert the code that needs to be executed when the game registers a mouse button action
//
//	/* Example:
//	if (isLeft == true && isDown == true) // is it a left mouse press?
//	{	
//		if ( x > 261 && x < 261 + 117 ) // check if press lies within x coordinates of choice
//		{
//			if ( y > 182 && y < 182 + 33 ) // check if press also lies within y coordinates of choice
//			{
//				GAME_ENGINE->MessageBox("Clicked.");
//			}
//		}
//	}
//	*/
//}
//
//void TestFont::MouseMove(int x, int y, WPARAM wParam)
//{	
//	// Insert the code that needs to be executed when the mouse pointer moves across the game window
//
//	/* Example:
//	if ( x > 261 && x < 261 + 117 ) // check if mouse position is within x coordinates of choice
//	{
//		if ( y > 182 && y < 182 + 33 ) // check if mouse position also is within y coordinates of choice
//		{
//			GAME_ENGINE->MessageBox("Da mouse wuz here.");
//		}
//	}
//	*/
//
//}
//
//void TestFont::CheckKeyboard()
//{	
//	// Here you can check if a key of choice is held down
//	// Is executed once per frame when the gameloop is running
//
//	/* Example:
//	if (GAME_ENGINE->IsKeyDown('K')) xIcon -= xSpeed;
//	if (GAME_ENGINE->IsKeyDown('L')) yIcon += xSpeed;
//	if (GAME_ENGINE->IsKeyDown('M')) xIcon += xSpeed;
//	if (GAME_ENGINE->IsKeyDown('O')) yIcon -= ySpeed;
//	*/
//}
//
//void TestFont::KeyPressed(TCHAR cKey)
//{	
//	// DO NOT FORGET to use SetKeyList() !!
//
//	// Insert the code that needs to be executed when a key of choice is pressed
//	// Is executed as soon as the key is released
//	// You first need to specify the keys that the game engine needs to watch by using the SetKeyList() method
//
//	/* Example:
//	switch (cKey)
//	{
//	case 'K': case VK_LEFT:
//		MoveBlock(DIR_LEFT);
//		break;
//	case 'L': case VK_DOWN:
//		MoveBlock(DIR_DOWN);
//		break;
//	case 'M': case VK_RIGHT:
//		MoveBlock(DIR_RIGHT);
//		break;
//	case 'A': case VK_UP:
//		RotateBlock();
//		break;
//	case VK_ESCAPE:
//	}
//	*/
//}

void TestFont::GameTick(double deltaTime)
{
	// Insert the code that needs to be executed, EXCEPT for paint commands (see next method)
}

void TestFont::GamePaint(RECT rect)
{
	MATRIX3X2 matTranslate, matTotal;
	matTranslate.SetAsTranslate(20, 60);
	matTotal = matTranslate;
	BITFONT->PaintString(m_sString, matTotal, 2);
}

void TestFont::CallAction(Caller* callerPtr)
{
	if (callerPtr == m_TxtInputPtr)
	{
		//stringstream buffer;
		char str[128];
		wstring buffer(m_TxtInputPtr->GetText().ToTChar());
		size_t i;
		wcstombs_s(&i, str, 128, buffer.c_str(), 128);
		m_sString = str;
	}
}





