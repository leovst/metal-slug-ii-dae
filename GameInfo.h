#pragma once
//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//-----------------------------------------------------
// SINGLETON CLASS
//-----------------------------------------------------

//-----------------------------------------------------
// Include Files
//-----------------------------------------------------
#include "GameEngine.h"

#include "Hero.h"
#include "Environment.h"
#include "ObjectManager.h"
#include "ResourceManager.h"

// Forward declaration
class HUD;

//-----------------------------------------------------
// GameInfo Class									
//-----------------------------------------------------

class GameInfo
{
public:
	
	//-------------------------------------------------
	// Singleton Methods						
	//-------------------------------------------------

	static GameInfo* GetSingleton();
	void Release();

	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------

	// Getters & Setters
	//-------------------------------------------------

	ResourceManager* GetResourceManager(); 
	void AttachResourceManager(ResourceManager *resourcePtr);

	Hero* GetHero();
	void AttachHero(Hero *heroPtr);

	Environment* GetEnvironment();
	void AttachEnvironment(Environment *missionPtr);

	ObjectManager* GetObjectManager();
	void AttachObjectManager(ObjectManager *managerPtr);

	HUD* GetHUD();
	void AttachHUD(HUD* hudPtr);

	bool GetGameOver();
	void SetGameOver(bool isGameOver);

	// Debug	
	//-------------------------------------------------

	bool GetShowDebug();
	void ToggleDebug();


private: 
	//-------------------------------------------------
	// Singleton Methods						
	//-------------------------------------------------

	// Private Constructor
	GameInfo();
	virtual ~GameInfo();		// Destructor
	static GameInfo *m_SingletonPtr;

	//-------------------------------------------------
	// Datamembers								
	//-------------------------------------------------

	bool m_IsGameOver;

	// General Objects
	//-------------------------------------------------
	ResourceManager* m_ResourceManagerPtr;
	Hero* m_HeroPtr;
	ObjectManager* m_ObjectManagerPtr;
	HUD* m_HudPtr;

	// Mission Objects
	//-------------------------------------------------
	Environment* m_EnvironmentPtr;
	int m_TimeLeft;

	// Debug
	//-------------------------------------------------
	bool m_ShowDebug;

	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	GameInfo(const GameInfo& yRef);									
	GameInfo& operator=(const GameInfo& yRef);	
};


