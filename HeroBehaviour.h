#pragma once
//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//-----------------------------------------------------
// Include Files
//-----------------------------------------------------
#include "GameEngine.h"


//-----------------------------------------------------
// HeroBehaviour Class									
//-----------------------------------------------------
class HeroBehaviour
{
public:
	HeroBehaviour();				// Constructor
	virtual ~HeroBehaviour();		// Destructor

	//-------------------------------------------------
	// Datamembers								
	//-------------------------------------------------
	
	// State enums
	//-------------------------------------------------

	enum class MovementState
	{
		STILL = 0,
		RUN,
		JUMP,
		FALL
	};
	enum class BehaviourState
	{
		IDLE = 0,
		SHOOT,
		THROW,
		ATTACK,
		ENDACTION,
		DIE,
		RESPAWN
	};

	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------

	void Tick(double deltaTime);

	// Getters & Setters	
	//-------------------------------------------------

	// States

	MovementState GetMovementState();
	void SetMovementState(MovementState movement);

	BehaviourState GetBehaviourState();
	void SetBehaviourState(BehaviourState behaviour);

	// Looking direction

	double GetShootingAngle();
	bool IsLookingDown();
	bool IsLookingUp();

	// Animation counts

	double GetMovementAnimation();
	double GetBehaviourAnimation();

	

private: 
	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------

	// Ticks
	//-------------------------------------------------

	void TickKeyInput(double deltaTime);

	// Resets	
	//-------------------------------------------------

	void ResetMovementAnimation();
	void ResetBehaviourAnimation();

	//-------------------------------------------------
	// Datamembers								
	//-------------------------------------------------

	// States
	MovementState m_MovementState;
	BehaviourState m_BehaviourState;

	// Loking direction
	double m_ShootingAngle;

	// Animation
	double m_MovementAnimation, m_BehaviourAnimation;

	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	HeroBehaviour(const HeroBehaviour& yRef);									
	HeroBehaviour& operator=(const HeroBehaviour& yRef);	
};

 
