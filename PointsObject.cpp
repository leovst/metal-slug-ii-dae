//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//---------------------------
// Includes
//---------------------------
#include "PointsObject.h"

#include "GameInfo.h"
#include "Sprite.h"

#include <string>
using namespace std;

//---------------------------
// Defines
//---------------------------
//#define GAME_ENGINE (GameEngine::GetSingleton())
#define GAME_INFO (GameInfo::GetSingleton())

//---------------------------
// Constructor & Destructor
//---------------------------
PointsObject::PointsObject(DOUBLE2 pos, int amountOfPoints, int type)
	:Item(pos, amountOfPoints)
	,m_Type(type)
	,m_SpecialAni(false)
	,m_SpriteFrame(-1)
{
	// Define type
	string key;
	if (type == ITEM_REDGEM)
		key = "GEMRED";
	else if (type == ITEM_BLUEGEM)
		key = "GEMBLUE";
	else if (type == ITEM_YELLOWGEM)
		key = "GEMYELLOW";
	else if (type == ITEM_BANANA)
		key = "BANANA";
	else if (type == ITEM_APPLE)
		key = "APPLE";
	else if (type == ITEM_MEATBONE)
		key = "MEATBONE";
	else if (type == ITEM_CAT)
		key = "CAT";
	else if (type == ITEM_MONKEY)
		key = "MONKEY";

	// Bitmap
	m_BmpItemsPtr = GAME_INFO->GetResourceManager()->GetBitmap(IDB_ITEMS);

	// Sprite
	m_ItemSpritePtr = new Sprite("Resources/SpriteInfo_Items.ini", key);

	// Hitregion
	m_HitMainPtr->CreateFromRect(m_ItemSpritePtr->GetCenter().x, -m_ItemSpritePtr->GetSize().y, -m_ItemSpritePtr->GetCenter().x, 0);
	m_HitMainPtr->SetPos(m_Pos);
}

PointsObject::~PointsObject()
{
	// Bitmap
	GAME_INFO->GetResourceManager()->ReleaseBitmap(m_BmpItemsPtr);
	m_BmpItemsPtr = nullptr;

	// Sprite
	delete m_ItemSpritePtr;
	m_ItemSpritePtr = nullptr;
}

//---------------------------
// Methods - Member functions
//---------------------------

void PointsObject::Tick(double deltaTime)
{
	// Inheritance
	Item::Tick(deltaTime);

	// Mark for deletion
	if (m_IsTriggered)
		SetDeleteStatus(true);
}
void PointsObject::Paint()
{
	// Inheritance
	Item::Paint();

	// Calculate Matrices
	MATRIX3X2 matCenter, matTranslate, matTotal;
	matCenter.SetAsTranslate(m_ItemSpritePtr->GetCenter().x, -m_ItemSpritePtr->GetSize().y);
	matTranslate.SetAsTranslate(m_Pos);
	matTotal = matCenter * matTranslate;

	// Paint
	if ((m_Type == ITEM_BANANA || m_Type == ITEM_MEATBONE) && !m_PhysicsEnabled)
	{
		// Sets random frame
		if (m_SpriteFrame == -1)
			m_SpriteFrame = rand() % m_ItemSpritePtr->GetNumberOfFrames();

		m_ItemSpritePtr->PaintSpriteFrame(m_BmpItemsPtr, matTotal, m_SpriteFrame);
	}
	else if (m_Type == ITEM_CAT)
	{
		if (!m_SpecialAni && m_AnimationCount >= 1 && rand() % 100 == 0)
		{
			m_SpecialAni = true;
			m_AnimationCount = 0;
		}

		if (m_SpecialAni)
		{
			// Yawning cat
			if (m_AnimationCount >= m_ItemSpritePtr->GetLength())
			{
				m_SpecialAni = false;
				m_AnimationCount = 0;
			}
			m_ItemSpritePtr->PaintSprite(m_BmpItemsPtr, matTotal, m_AnimationCount);
		}
		else
			// Sleeping cat
			m_ItemSpritePtr->PaintSpriteFrame(m_BmpItemsPtr, matTotal, m_ItemSpritePtr->GetNumberOfFrames()-1);
	}
	else
		m_ItemSpritePtr->PaintSprite(m_BmpItemsPtr, matTotal, m_AnimationCount);
}

void PointsObject::Hit()
{
	GAME_INFO->GetHero()->AddPoints(m_Content);
	m_IsTriggered = true;
}


