#pragma once
//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//-----------------------------------------------------
// Include Files
//-----------------------------------------------------
#include "GameEngine.h"

//-----------------------------------------------------
// Defines
//-----------------------------------------------------
#define BF_S_SHINYGOLD 0
#define BF_S_MATTEGOLD 1
#define BF_S_WHITE 2
#define BF_M_TIMER 3
#define BF_B_START 4

//-----------------------------------------------------
// BitFont Class									
//-----------------------------------------------------
class BitFont
{
public:
	//-------------------------------------------------
	// Singleton Methods						
	//-------------------------------------------------

	static BitFont* GetSingleton();
	void Release();

	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------

	void PaintString(std::string sInput, MATRIX3X2 matInput, int fontType, bool leftAlign = true);
	void PaintChar(char cInput, MATRIX3X2 matInput, int fontType);

	void PaintExplode(std::string sInput, DOUBLE2 centerPoint, double explodeSpeed, double animationCounter, double startSecond = 0, double startExplode = 0);

private: 
	//-------------------------------------------------
	// Singleton Methods						
	//-------------------------------------------------

	// Private Constructor
	BitFont();				// Constructor
	virtual ~BitFont();		// Destructor
	static BitFont *m_SingletonPtr;


	//-------------------------------------------------
	// Datamembers								
	//-------------------------------------------------

	Bitmap* m_BmpFontsPtr;

	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	BitFont(const BitFont& yRef);									
	BitFont& operator=(const BitFont& yRef);	
};

 
