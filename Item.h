#pragma once
//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//-----------------------------------------------------
// Include Files
//-----------------------------------------------------
//#include "GameEngine.h"

#include "ObjectEntity.h"

//-----------------------------------------------------
// Defines
//-----------------------------------------------------
#define ITEM_HMU 0
#define ITEM_BOMBPACK 1
#define ITEM_REDGEM 2
#define ITEM_BLUEGEM 3
#define ITEM_YELLOWGEM 4
#define ITEM_BANANA 5
#define ITEM_APPLE 6
#define ITEM_MEATBONE 7
#define ITEM_CAT 8
#define ITEM_MONKEY 9

//-----------------------------------------------------
// Item Class									
//-----------------------------------------------------
class Item : public ObjectEntity
{
public:
	Item(DOUBLE2 pos, int content);				// Constructor
	virtual ~Item();		// Destructor

	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------
	
	virtual void Tick(double deltaTime);
	virtual void Paint();

	virtual void Hit() = 0;


protected: 
	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------

	// Collision
	//-------------------------------------------------

	void TickPlayerCollisionHandling();
	void TickLevelCollisionHandling(double deltaTime);
	bool RectIntersectTest(RECT2 r1, RECT2 r2);


	//-------------------------------------------------
	// Datamembers								
	//-------------------------------------------------
	
	bool m_IsTriggered;
	double m_AnimationCount;

	int m_Content;
	

private: 
	//-------------------------------------------------
	// Datamembers								
	//-------------------------------------------------

	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	Item(const Item& yRef);									
	Item& operator=(const Item& yRef);	
};

 
