//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//---------------------------
// Includes
//---------------------------
#include "BitFont.h"

#include "MetalSlug2.h"
#include "GameInfo.h"

#include <string>
using namespace std;

//---------------------------
// Defines
//---------------------------
#define GAME_ENGINE (GameEngine::GetSingleton())
#define GAME_INFO (GameInfo::GetSingleton())

//---------------------------
// Singleton
//---------------------------
BitFont* BitFont::m_SingletonPtr = nullptr;

BitFont* BitFont::GetSingleton()
{
	if(!m_SingletonPtr)
		m_SingletonPtr = new BitFont();
	return m_SingletonPtr;
}
void BitFont::Release()
{
	delete m_SingletonPtr;
	m_SingletonPtr = nullptr;
}


//---------------------------
// Constructor & Destructor
//---------------------------

BitFont::BitFont()
{
	m_BmpFontsPtr = new Bitmap(IDB_FONTS);
}
BitFont::~BitFont()
{
	delete m_BmpFontsPtr;
	m_BmpFontsPtr = nullptr;
}

//---------------------------
// Methods - Member functions
//---------------------------

void BitFont::PaintChar(char cInput, MATRIX3X2 matInput, int fontType)
{
	// Convert to ASCII numerical value
	int cIndex = (int)cInput;
	// Correct for available characters in bitmap
	if (fontType == BF_M_TIMER)
		// Numbers only
			cIndex -= 48;
	else if (fontType == BF_B_START)
	{
		// Customized mapping
		if (cIndex == 32)
			cIndex = 38;
		else if (cIndex == 33)
			cIndex = 36;
		else if (cIndex == 63)
			cIndex = 37;
		else if (cIndex >= 48 && cIndex <= 57)
			cIndex -= 48;
		else if (cIndex >= 65 && cIndex <= 90)
			cIndex -= (65 - 10);
		else if (cIndex >= 97 && cIndex <= 122)
			cIndex -= (97 - 10);
	}
	else
		cIndex -= 32;

	// Basic dimensions
	DOUBLE2 charDim(8,8);
	if (fontType == BF_M_TIMER)
		charDim *= 2;
	else if (fontType == BF_B_START)
		charDim = DOUBLE2(25,32);

	// Matrices
	MATRIX3X2 matScale;
	matScale.SetAsScale(MetalSlug2::GAMESCALE);

	// Set Clipping rect
	RECT clip = {};
	if (fontType == BF_M_TIMER)
	{
		clip.left = cIndex * (int)charDim.x;
		clip.top = 72;

	}
	else if (fontType == BF_B_START)
	{
		clip.left = (cIndex % 10) * (int)charDim.x;
		clip.top = 88 + (cIndex / 10) * (int)charDim.y;

	}
	else
	{
		clip.left = (cIndex % 32) * (int)charDim.x;
		clip.top = fontType * 3 * (int)charDim.y + (cIndex / 32) * (int)charDim.y;
	}
	clip.right = clip.left + (int)charDim.x;
	clip.bottom = clip.top + (int)charDim.y;

	MATRIX3X2 matTotal = matInput * matScale;

	GAME_ENGINE->SetTransformMatrix(matTotal);
	GAME_ENGINE->DrawBitmap(m_BmpFontsPtr, 0, 0, clip);
}

void BitFont::PaintString(string sInput, MATRIX3X2 matInput, int fontType, bool leftAlign)
{
	// Put string into c-style char array
	const char* charArr = sInput.c_str();

	// Basic dimensions
	DOUBLE2 charDim(8,8);
	if (fontType == BF_M_TIMER)
		charDim *= 2;
	else if (fontType == BF_B_START)
		charDim *= 4;

	// Start matrices
	MATRIX3X2 matAdd, matScale, matCurrent;
	matAdd.SetAsTranslate(charDim.x, 0);
	matScale.SetAsScale(MetalSlug2::GAMESCALE);
	matCurrent = matInput;

	// Exception if right align
	if (!leftAlign)
	{
		MATRIX3X2 matNew;
		matNew.SetAsTranslate(sInput.size() * -charDim.x, 0);
		matCurrent = matCurrent * matNew;
	}

	int column = 0;
	for (unsigned int i = 0; i < min(sInput.size(), 128); ++i)
	{
		int charIndex = (int)charArr[i];
		// SPECIAL CASES:
		// - Tab
		if (charIndex == 9)
		{
			while (4-(column%5) != 0)
			{
				matCurrent = matCurrent * matAdd;
				++column;
			}
		}
		// - New Line
		else if (charIndex == 10)
		{
			matCurrent.SetAsTranslate(matInput.orig.x, matCurrent.orig.y + charDim.y + charDim.y/4);
			column = 0;
		}
		else
		{
			// if char is in bitmap
			if (charIndex >= 32 && charIndex < 128)
			{
				PaintChar(charArr[i], matCurrent, fontType);

				// Set position for next char
				matCurrent = matCurrent * matAdd;
			}
			// Move space
			++column;
		}
	}
}

void BitFont::PaintExplode(string sInput, DOUBLE2 centerPoint, double explodeSpeed, double animationCounter, double startSecond, double startExplode)
{
	// Debug
	if (GAME_INFO->GetShowDebug())
	{
		MATRIX3X2 matScale;
		matScale.SetAsScale(MetalSlug2::GAMESCALE);
		GAME_ENGINE->SetTransformMatrix(matScale);
		GAME_ENGINE->SetColor(COLOR(255,0,0));
		GAME_ENGINE->FillEllipse(centerPoint, 2, 2);
	}

	// Put string into c-style char array
	const char* charArr = sInput.c_str();

	// Basic dimensions
	DOUBLE2 charDim(24,32);

	// Start matrices
	MATRIX3X2 matAdd, matCurrent;
	matAdd.SetAsTranslate(charDim.x, 0);

	// Get size of lines
	int lineLength1 = 0, lineLength2 = 0, currentRow = 1;
	for (unsigned int i = 0; i < min(sInput.size(), 128); ++i)
	{
		int charIndex = (int)charArr[i];

		// Next line
		if (currentRow == 1 && charIndex == 10)
			++currentRow;
		else if (currentRow == 1)
			++lineLength1;
		// Maximum 2 lines
		else if (currentRow == 2 && charIndex == 10)
			break;
		else
			++lineLength2;
	}

	// Set first drawing position
	matCurrent.SetAsTranslate(centerPoint - DOUBLE2(charDim.x * lineLength1/2, charDim.y * currentRow/2));

	double currentAnimation = 0;
	if (animationCounter >= startExplode)
		currentAnimation = animationCounter -startExplode;

	for (unsigned int i = 0; i < min(sInput.size(), 128); ++i)
	{
		int charIndex = (int)charArr[i];

		if (charIndex == 10 && (lineLength2 == 0 || animationCounter < startSecond))
			break;
		else if (charIndex == 10 && lineLength2 > 0)
		{
			// New Line
			matCurrent.SetAsTranslate(centerPoint - DOUBLE2(charDim.x * lineLength2/2, -charDim.y * (currentRow/2 -1)));
		}
		else
		{
			// Paint 
			// if char is in bitmap
			if (charIndex >= 32 && charIndex < 128)
			{
				// Calculate direction vector
				DOUBLE2 dirVector = (matCurrent.orig - centerPoint) + DOUBLE2((centerPoint.x - matCurrent.orig.x)/2, charDim.y/2);
				dirVector = dirVector.Normalized();

				DOUBLE2 currentVelocity = dirVector * explodeSpeed;
				MATRIX3X2 matOffset;

				matOffset.SetAsTranslate(currentVelocity * currentAnimation);

				// Paint
				PaintChar(charArr[i], matCurrent * matOffset, 4);

				// Set position for next char
				matCurrent = matCurrent * matAdd;
			}
		}

	}

}
