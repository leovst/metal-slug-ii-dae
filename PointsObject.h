#pragma once
//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//-----------------------------------------------------
// Include Files
//-----------------------------------------------------
#include "GameEngine.h"

#include "Item.h"

// Forward declaration
class Sprite;

//-----------------------------------------------------
// PointsObject Class									
//-----------------------------------------------------
class PointsObject : public Item
{
public:
	// Color: 0 = red, 1 = blue, 2 = yellow
	PointsObject(DOUBLE2 pos, int amountOfPoints, int type);				// Constructor
	virtual ~PointsObject();		// Destructor

	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------
	
	void Tick(double deltaTime);
	void Paint();

	void Hit();

private: 
	//-------------------------------------------------
	// Datamembers								
	//-------------------------------------------------

	// Type
	int m_Type;

	// Sprites
	Sprite* m_ItemSpritePtr;
	Bitmap* m_BmpItemsPtr;
	bool m_SpecialAni;
	int m_SpriteFrame;

	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	PointsObject(const PointsObject& yRef);									
	PointsObject& operator=(const PointsObject& yRef);	
};

 
