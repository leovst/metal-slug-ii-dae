#pragma once
//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//-----------------------------------------------------
// Include Files
//-----------------------------------------------------
#include "GameEngine.h"

#include "NPC.h"

// Forward Declarations
class Sprite;
class Projectile;

//-----------------------------------------------------
// EnemyArab Class									
//-----------------------------------------------------
class EnemyArab : public NPC
{
public:
	EnemyArab(DOUBLE2 pos, int amountToSpawn, int drop, int triggerStart = -1, int triggerEnd = -1);				// Constructor
	virtual ~EnemyArab();		// Destructor

	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------

	void Tick(double deltaTime);
	void Paint();

	void Hit(double deltaTime);

private: 
	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------

	// Ticks	
	//-------------------------------------------------

	void TickAim(double deltaTime, double waitTime);
	void TickDirection(double waitTime);

	void TickAttack(double deltaTime);
	void TickShuffle(double deltaTime);
	void TickThrow(double deltaTime);

	//-------------------------------------------------
	// Datamembers								
	//-------------------------------------------------

	// Behaviour
	bool m_CanAttack, m_CanThrow;
	int m_AttackSpace, m_ThrowSpace, m_ShuffleBuffer, m_RunBuffer;
	int m_AmountToDrop;

	// Bitmaps
	Bitmap* m_BmpArabPtr;

	// Sprites
	Sprite *m_SpriteIdlePtr, *m_SpriteShufflePtr, *m_SpriteRunPtr, *m_SpriteJumpPtr, *m_SpriteFallPtr,
		*m_SpriteSummersaultPtr, *m_SpritePrepPtr, *m_SpriteAttackPtr, *m_SpriteThrowPtr, *m_SpriteDiePtr;

	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	EnemyArab(const EnemyArab& yRef);									
	EnemyArab& operator=(const EnemyArab& yRef);	
};

 
