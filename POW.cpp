//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//---------------------------
// Includes
//---------------------------
#include "POW.h"

#include "GameInfo.h"
#include "Sprite.h"

#include "Item.h"
#include "HelperObject.h"

#include <string>
using namespace std;

//---------------------------
// Defines
//---------------------------
//#define GAME_ENGINE (GameEngine::GetSingleton())
#define GAME_INFO (GameInfo::GetSingleton())


//---------------------------
// Constructor & Destructor
//---------------------------
POW::POW(DOUBLE2 pos, int kind, Item* itemPtr)
	:NPC(pos, 1)
	,m_ItemPtr(itemPtr)
	,m_Kind(kind)
	,m_PresentSpace(25)
	,m_IsWaving(false)
{
	// General
	m_Type = kind;

	// Movement
	m_RunSpeed = 30;
	m_FleeSpeed = 100;
	m_BehaviourState = BehaviourState::BOUND;

	// Don't fall if hanging
	if (m_Kind == 2)
		m_PhysicsEnabled = false;

	// Bitmaps
	m_BmpPowPtr = GAME_INFO->GetResourceManager()->GetBitmap(IDB_POW);

	// Sprites
	string sIniFile("Resources/SpriteInfo_POW.ini");
	m_BoundSitSpritePtr			= new Sprite(sIniFile, "BOUNDSIT");
	m_FreedSitSpritePtr			= new Sprite(sIniFile, "FREESIT");
	m_BoundStickSpritePtr		= new Sprite(sIniFile, "BOUNDSTICK");
	m_BoundWaveSpritePtr		= new Sprite(sIniFile, "BOUNDWAVE");
	m_FreedStickSpritePtr		= new Sprite(sIniFile, "FREESTICK");
	m_StickDisappearSpritePtr	= new Sprite(sIniFile, "STICKDISAPPEAR");
	m_BoundHangSpritePtr		= new Sprite(sIniFile, "BOUNDHANG");
	m_RopeSpritePtr				= new Sprite(sIniFile, "ROPETOP");
	m_RopeDisappearSpritePtr	= new Sprite(sIniFile, "ROPEDISAPPEAR");
	m_RunSpritePtr				= new Sprite(sIniFile, "RUN");
	m_FleeSpritePtr				= new Sprite(sIniFile, "FLEE");
	m_PresentSpritePtr			= new Sprite(sIniFile, "PRESENT");
	m_SaluteSpritePtr			= new Sprite(sIniFile, "SALUTE");
	m_FallSpritePtr				= new Sprite(sIniFile, "FALL");
	m_LookSpritePtr				= new Sprite(sIniFile, "LOOK");

	// Create helper object
	if (m_Kind == 1)
		m_HelperPtr = new HelperObject(pos, m_BmpPowPtr, m_StickDisappearSpritePtr);
	else if (m_Kind == 2)
		m_HelperPtr = new HelperObject(pos, m_BmpPowPtr, m_RopeDisappearSpritePtr);
	else
		m_HelperPtr = nullptr;

	// Hitregion
	m_HitMainPtr->CreateFromRect(-10, -38, 10, 0);
	m_HitMainPtr->SetPos(m_Pos);
}

POW::~POW()
{
	// Bitmap
	GAME_INFO->GetResourceManager()->ReleaseBitmap(m_BmpPowPtr);
	m_BmpPowPtr = nullptr;

	// Item
	delete m_ItemPtr;
	m_ItemPtr = nullptr;

	// Helper Object
	delete m_HelperPtr;
	m_HelperPtr = nullptr;

	// Sprites
	delete m_BoundSitSpritePtr;
	m_BoundSitSpritePtr			= nullptr;
	delete m_FreedSitSpritePtr;
	m_FreedSitSpritePtr			= nullptr;
	delete m_BoundStickSpritePtr;
	m_BoundStickSpritePtr		= nullptr;
	delete m_BoundWaveSpritePtr;
	m_BoundWaveSpritePtr		= nullptr;
	delete m_FreedStickSpritePtr;
	m_FreedStickSpritePtr		= nullptr;
	delete m_StickDisappearSpritePtr;
	m_StickDisappearSpritePtr	= nullptr;
	delete m_RunSpritePtr;
	m_RunSpritePtr				= nullptr;
	delete m_FleeSpritePtr;
	m_FleeSpritePtr				= nullptr;
	delete m_PresentSpritePtr;
	m_PresentSpritePtr			= nullptr;
	delete m_SaluteSpritePtr;
	m_SaluteSpritePtr			= nullptr;
	delete m_FallSpritePtr;
	m_FallSpritePtr				= nullptr;
	delete m_LookSpritePtr;
	m_LookSpritePtr				= nullptr;
	delete m_BoundHangSpritePtr;
	m_BoundHangSpritePtr		= nullptr;
	delete m_RopeSpritePtr;
	m_RopeSpritePtr				= nullptr;
	delete m_RopeDisappearSpritePtr;
	m_RopeDisappearSpritePtr		= nullptr;
}

//---------------------------
// Methods - Member functions
//---------------------------


void POW::Tick(double deltaTime)
{
	// Set HitRegion
	TickHitregion(deltaTime);

	// Inheritance
	NPC::Tick(deltaTime);

	// Mark for deletion
	if (m_BehaviourState != BehaviourState::BOUND && GetOutBounds() > 40)
		SetDeleteStatus(true);

	// Tick or delete helper
	TickHelper(deltaTime);

	// Delay on actions
	double waitTime = 0.8;

	// Behaviour
	switch (m_BehaviourState)
	{
	case NPC::BehaviourState::IDLE:
		// Aim behaviour
		TickAim(deltaTime, waitTime);
		break;

	case NPC::BehaviourState::RUN:
		// Behaviour
		TickRun(deltaTime);
		// Aim behaviour
		TickAim(deltaTime, waitTime);
		break;

	case NPC::BehaviourState::BOUND:
		// Behaviour
		TickBound(deltaTime);
		break;

	case NPC::BehaviourState::FREED:
		// Behaviour
		TickFreed(deltaTime);
		break;

	case NPC::BehaviourState::PRESENT:
		// Behaviour
		TickPresent(deltaTime);
		break;

	case NPC::BehaviourState::SALUTE:
		if (m_AnimationCount >= m_SaluteSpritePtr->GetLength())
			SetBehaviourState(BehaviourState::FLEE);
		break;
	}

}
void POW::Paint()
{
	if (GetOutBounds() < 40)
	{
		// Pass paint to helper
		if (m_HelperPtr)
			m_HelperPtr->Paint();

		// Set correct Sprite
		Sprite* currentSpritePtr = nullptr;
		switch (m_BehaviourState)
		{
		case NPC::BehaviourState::IDLE:
			currentSpritePtr = m_LookSpritePtr;
			break;
		case NPC::BehaviourState::BOUND:
			if (m_Kind == NPC_POWSIT)
				currentSpritePtr = m_BoundSitSpritePtr;
			else if (m_Kind == NPC_POWSTICK)
			{
				if (m_IsWaving)
					currentSpritePtr = m_BoundWaveSpritePtr;
				else
					currentSpritePtr = m_BoundStickSpritePtr;
			}
			else if (m_Kind == NPC_POWHANG)
				currentSpritePtr = m_BoundHangSpritePtr;
			break;
		case NPC::BehaviourState::RUN:
			currentSpritePtr = m_RunSpritePtr;
			break;
		case NPC::BehaviourState::JUMP:
			currentSpritePtr = m_FallSpritePtr;
			break;
		case NPC::BehaviourState::FALL:
			currentSpritePtr = m_FallSpritePtr;
			break;
		case NPC::BehaviourState::FLEE:
			currentSpritePtr = m_FleeSpritePtr;
			break;
		case NPC::BehaviourState::FREED:
			if (m_Kind == NPC_POWSIT)
				currentSpritePtr = m_FreedSitSpritePtr;
			else if (m_Kind == NPC_POWSTICK)
				currentSpritePtr = m_FreedStickSpritePtr;
			break;
		case NPC::BehaviourState::PRESENT:
			currentSpritePtr = m_PresentSpritePtr;
			break;
		case NPC::BehaviourState::SALUTE:
			currentSpritePtr = m_SaluteSpritePtr;
			break;
		default:
			break;
		}

		// Calculate matrices
		MATRIX3X2 matCenter, matFlip, matTranslate, matTotal;
		matCenter.SetAsTranslate(currentSpritePtr->GetCenter().x, -currentSpritePtr->GetSize().y);
		matFlip.SetAsScale(m_Direction, 1);
		matTranslate.SetAsTranslate(m_Pos);

		matTotal = matCenter * matFlip * matTranslate;

		// Paint
		currentSpritePtr->PaintSprite(m_BmpPowPtr, matTotal, m_AnimationCount, m_Direction);
	}

	// Inheritance
	NPC::Paint();
}

void POW::Hit(double deltaTime)
{
	SetBehaviourState(BehaviourState::FREED);
	GAME_INFO->GetHero()->AddPoints(100);
	GAME_INFO->GetHero()->AddRescue();
	m_CanHit = false;
	Tick(deltaTime);
	m_PhysicsEnabled = true;
}

// Ticks		
//-------------------------------------------------

void POW::TickHitregion(double deltaTime) 
{
	// Get HitRegion boundaries
	RECT2 hit = m_HitMainPtr->GetBounds();
	hit.left -= m_Pos.x;
	hit.top -= m_Pos.y;
	hit.right -= m_Pos.x;
	hit.bottom -= m_Pos.y;

	// Change bounds
	if (m_BehaviourState == BehaviourState::BOUND && m_Kind == 2)
	{
		hit.top = -60;
		hit.bottom = -10;
	}
	else
	{
		hit.top = -38;
		hit.bottom = 0;
	}

	// Set bounds again
	m_HitMainPtr->CreateFromRect(hit.left, hit.top, hit.right, hit.bottom);
	m_HitMainPtr->SetPos(m_Pos);
}
void POW::TickHelper(double deltaTime)
{
	if (m_HelperPtr)
	{
		if (m_HelperPtr->GetDeleteStatus())
		{
			delete m_HelperPtr;
			m_HelperPtr = nullptr;
		}
		else
			m_HelperPtr->Tick(deltaTime);
	}
}
void POW::TickAim(double deltaTime, double waitTime)
{
	// Get distance to hero
	DOUBLE2 currentDistanceBottom = GAME_INFO->GetHero()->DistanceTo(m_Pos);
	DOUBLE2 currentDistanceTop = GAME_INFO->GetHero()->DistanceTo(m_Pos + DOUBLE2(0, -GAME_INFO->GetHero()->GetHitRegion()->GetBounds().top));

	// Present immediately when possible
	if (min(currentDistanceBottom.Length(), currentDistanceTop.Length()) < m_PresentSpace)
	{
		SetBehaviourState(BehaviourState::PRESENT);
		GAME_INFO->GetResourceManager()->PlayAudio(IDR_THANKYOU);
	}
	else if (currentDistanceBottom.y > 60)
	{
		// Height difference is too big
		if (currentDistanceBottom.x > m_PresentSpace)
			SetBehaviourState(BehaviourState::RUN);
		else
			SetBehaviourState(BehaviourState::JUMP);
	}
	// delay before other acts
	else if (m_AnimationCount <= waitTime)
		return;
	else if (m_BehaviourState == BehaviourState::IDLE && rand() %30 == 0)
		SetBehaviourState(BehaviourState::RUN);
}

void POW::TickRun(double deltaTime)
{
	// Set Direction
	if ((int)m_AnimationCount % 4 == 1)
		m_Direction = -1;
	else if ((int)m_AnimationCount % 4 == 3)
		m_Direction = 1;

	// End after a while
	if (m_AnimationCount >= 15)
		SetBehaviourState(BehaviourState::FLEE);
}
void POW::TickBound(double deltaTime)
{
	// Start wave animation
	if (m_Kind == NPC_POWSTICK && m_AnimationCount > 4 && rand() %50 == 0)
	{
		m_IsWaving = true;
		m_AnimationCount = 0;
	}
	// End wave animation
	if (m_IsWaving && m_AnimationCount >= m_BoundWaveSpritePtr->GetLength())
	{
		m_IsWaving = false;
		m_AnimationCount = 0;
	}
}
void POW::TickFreed(double deltaTime)
{
	// End of animation
	if (m_Kind == 0 && m_AnimationCount >= m_FreedSitSpritePtr->GetLength())
		SetBehaviourState(BehaviourState::IDLE);
	else if (m_Kind == NPC_POWSTICK && m_AnimationCount >= m_FreedStickSpritePtr->GetLength())
	{
		SetBehaviourState(BehaviourState::IDLE);
		// Show Helper: stick
		m_HelperPtr->SetPos(m_Pos);
		m_HelperPtr->Show();
	}
	else if (m_Kind == NPC_POWHANG)
	{
		SetBehaviourState(BehaviourState::FALL);
		// Show Helper: rope
		m_HelperPtr->SetPos(m_Pos);
		m_HelperPtr->Show();
	}
}
void POW::TickPresent(double deltaTime)
{
	// Add item to objectmanager
	if (m_AnimationCount >= 1 && m_ItemPtr)
	{
		m_ItemPtr->SetPos(m_Pos + DOUBLE2(-m_Direction * 10, -10));
		GAME_INFO->GetObjectManager()->AddItem(m_ItemPtr);
		m_ItemPtr = nullptr;
	}

	// End of animation
	if (m_AnimationCount >= m_PresentSpritePtr->GetLength())
		SetBehaviourState(BehaviourState::SALUTE);
}