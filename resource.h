
// Microsoft Visual C++ generated include file.
// Used by MetalSlug2.rc


// Icons
#define IDI_BIG                         1000
#define IDI_SMALL                       1001

// Bitmaps
#define IDB_STARTSCREEN                 1100
#define IDB_ITEMS                       1101
#define IDB_MARCOBODY                   1104
#define IDB_MARCOLEGS                   1105
#define IDB_MISSION1		            1106
#define IDB_PROJECTILES                 1107
#define IDB_EXPLOSION                   1108
#define IDB_MARCOBODYHEAVY              1109
#define IDB_GUNUPGRADES                 1110
#define IDB_ENEMYARAB                   1111
#define IDB_FONTS                       1112
#define IDB_HUD                         1113
#define IDB_POW                         1114
#define IDB_DESTROYABLEBUILDING			1115
#define IDB_METALSLUG2					1116
#define IDB_GAMEOVER					1117

// Ini's
#define IDT_ENEMYARAB					1150

// Audio
#define IDR_JUDGEMENTINTRO				1200
#define IDR_JUDGEMENTSONG				1201
#define IDR_PISTOLSHOT	                1202
#define IDR_ARABSCREAM                  1203
#define IDR_ARABUGH                     1204
#define IDR_BOMBPACK                    1205
#define IDR_GRENADEEXPLOSION            1206
#define IDR_HEAVYMACHINEGUNSHOTS		1207
#define IDR_HEAVYMACHINEGUNUPGRADE		1208
#define IDR_KICK						1209
#define IDR_MISSIONONESTART				1210
#define IDR_ITEM						1211
#define IDR_THANKYOU					1212
#define IDR_HERODIE						1213
#define IDR_GAMEOVER					1214
