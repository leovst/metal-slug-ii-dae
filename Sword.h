#pragma once
//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//-----------------------------------------------------
// Include Files
//-----------------------------------------------------
#include "GameEngine.h"

#include "Projectile.h"
#include "Sprite.h"

//-----------------------------------------------------
// Sword Class									
//-----------------------------------------------------
class Sword : public Projectile
{
public:
	Sword(DOUBLE2 startPos, DOUBLE2 dirVector);				// Constructor
	virtual ~Sword();		// Destructor

	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------

	void Tick(double deltaTime);
	void Paint();

private: 

	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------

	// Ticks	
	//-------------------------------------------------

	void TickLevelCollisionHandling(double deltaTime);
	void TickPlayerCollisionHandling();

	//-------------------------------------------------
	// Datamembers								
	//-------------------------------------------------
	
	// Sprites
	Sprite *m_SwordSpritePtr, *m_SwordEndSpritePtr;

	// Animation
	double m_AnimationCount;

	// General Info
	bool m_HasHitGround;

	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	Sword(const Sword& yRef);									
	Sword& operator=(const Sword& yRef);	
};

 
