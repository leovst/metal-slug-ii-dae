//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//---------------------------
// Includes
//---------------------------
#include "HelperObject.h"

#include "Sprite.h"

//---------------------------
// Defines
//---------------------------
#define GAME_ENGINE (GameEngine::GetSingleton())

//---------------------------
// Constructor & Destructor
//---------------------------
HelperObject::HelperObject(DOUBLE2 pos, Bitmap* bmpPtr, Sprite* spritePtr)
	:ObjectEntity(pos, false)
	,m_IsShown(false)
	,m_AnimationCount(0.0)
	,m_SpritePtr(spritePtr)
	,m_BmpPtr(bmpPtr)
{
	// nothing to create
}

HelperObject::~HelperObject()
{
	// nothing to destroy
}

//---------------------------
// Methods - Member functions
//---------------------------

void HelperObject::Tick(double deltaTime)
{
	// Stop if not activated
	if (!m_IsShown)
		return;

	m_AnimationCount += deltaTime;

	// Mark for deletion
	if (m_AnimationCount >= m_SpritePtr->GetLength() || GetOutBounds() > 40)
		SetDeleteStatus(true);
}
void HelperObject::Paint()
{
	// Stop if not activated
	if (!m_IsShown)
		return;

	// Calculate matrices
	MATRIX3X2 matCenter, matTranslate, matTotal;
	matCenter.SetAsTranslate(m_SpritePtr->GetCenter().x, -m_SpritePtr->GetSize().y);
	matTranslate.SetAsTranslate(m_Pos);

	matTotal = matCenter * matTranslate;

	m_SpritePtr->PaintSprite(m_BmpPtr, matTotal, m_AnimationCount);

	// Inheritance
	ObjectEntity::Paint();
}



// Setters
//-------------------------------------------------

void HelperObject::Show()
{
	m_IsShown = true;
}
