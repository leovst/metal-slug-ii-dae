//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//---------------------------
// Includes
//---------------------------
#include "Grenade.h"

#include "GameInfo.h"
#include "Sprite.h"
#include "NPC.h"

//---------------------------
// Defines
//---------------------------
#define GAME_ENGINE (GameEngine::GetSingleton())
#define GAME_INFO (GameInfo::GetSingleton())

//---------------------------
// Constructor & Destructor
//---------------------------

Grenade::Grenade(DOUBLE2 pos, DOUBLE2 dirVector)
	:Projectile(pos, true, 3)
	,m_AnimationCount(0.0)
	,m_hasHitGroundOnce(false)
{
	// Animation
	m_GrenadeSpritePtr = new Sprite(m_sIniFile, "GRENADE");

	// HitRegion
	RECT hit;
	hit.left = (int)m_GrenadeSpritePtr->GetCenter().x;
	hit.right = hit.left + (int)m_GrenadeSpritePtr->GetSize().x;
	hit.top = (int)m_GrenadeSpritePtr->GetCenter().y;
	hit.bottom = hit.top + (int)m_GrenadeSpritePtr->GetSize().y;
	m_HitMainPtr->CreateFromRect(hit.left, hit.top, hit.right, hit.bottom);
	m_HitMainPtr->SetPos(m_Pos);

	// General vars
	m_Velocity = dirVector;
	m_Type = PROJ_GRENADE;
}
Grenade::~Grenade()
{
	delete m_GrenadeSpritePtr;
	m_GrenadeSpritePtr = nullptr;
}

//---------------------------
// Methods - Member functions
//---------------------------

void Grenade::Tick(double deltaTime)
{

	TickLevelCollisionHandling(deltaTime);
		// Inheritance
		Projectile::Tick(deltaTime);

	// Animation
	m_AnimationCount += deltaTime;

}
void Grenade::Paint()
{
	// Calculate matrices
	MATRIX3X2 matCenter, matRotate, matDirection, matTranslate, matTotal;

	matCenter.SetAsTranslate(m_GrenadeSpritePtr->GetCenter());
	int animationSpeed = m_GrenadeSpritePtr->GetSpeed();
	int numberOfFrames = m_GrenadeSpritePtr->GetNumberOfFrames();
	int quadrant = (int)((m_AnimationCount*animationSpeed) / numberOfFrames) % 4;
	matRotate.SetAsRotate((M_PI/2) *quadrant);
	int direction = 1;
	if (m_Velocity.x < 0)
		direction = -1;
	matDirection.SetAsScale(direction, 1);
	matTranslate.SetAsTranslate(m_Pos);

	matTotal = matCenter * matRotate * matDirection * matTranslate;

	m_GrenadeSpritePtr->PaintSprite(m_BmpProjectilesPtr, matTotal, m_AnimationCount);

	// Inheritance
	Projectile::Paint();

	if (m_ShowDebug)
	{
		GAME_ENGINE->SetColor(COLOR(0,0,0));
		GAME_ENGINE->SetTransformMatrix(GetMatView());
		GAME_ENGINE->DrawLine(m_Pos, m_Pos+m_Velocity*0.3);
	}
}

// Ticks	
//-------------------------------------------------

bool Grenade::TickLevelCollisionHandling(double deltaTime)
{
	DOUBLE2 vector = (m_Velocity + m_Gravity * deltaTime) * deltaTime;
	HIT hitArr[1];

	if (GAME_INFO->GetEnvironment()->GetHitRegion()->Raycast(m_Pos, vector, hitArr, 1, -1))
	{
		// Set position to hit point
		SetPos(hitArr[0].point + DOUBLE2(0, -0.1));

		// Remaining time
		deltaTime *= (1-hitArr[0].lambda);
		if (!m_hasHitGroundOnce && m_Velocity.y > 0)
		{
			// Reflection Velocity
			DOUBLE2 projected;
			double length = m_Velocity.DotProduct(hitArr[0].normal);
			projected = hitArr[0].normal * length;
			m_Velocity -= (2*projected); 
			m_Velocity.y /= 1.5;
			m_Velocity.x *= 1.2;

			// Slow down sprite animation
			m_GrenadeSpritePtr->SetSpeed(m_GrenadeSpritePtr->GetSpeed()/2);
			m_hasHitGroundOnce = true;
		}
		else
			m_HasHit = true;

		return true;
	}
	return false;
}
void Grenade::TickNPCCollisionHandling(double deltaTime)
{
	// Basic collision based on position
	for(NPC* npcPtr : GAME_INFO->GetObjectManager()->GetNPCs())
	{
		if (npcPtr->CanHit() && npcPtr->GetHitRegion()->HitTest(m_Pos))
		{
			npcPtr->Hit(deltaTime);
			// Send downwards for explosion on the ground
			m_hasHitGroundOnce = true;
			Translate(0, -10);
			SetVelocity(0, 500);
			TickLevelCollisionHandling(deltaTime);
		}

	}

}
