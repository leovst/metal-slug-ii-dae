//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//---------------------------
// Includes
//---------------------------
#include "Explosion.h"

#include "GameInfo.h"
#include "Sprite.h"

#include <string>
using namespace std;

//---------------------------
// Defines
//---------------------------
//#define GAME_ENGINE (GameEngine::GetSingleton())
#define GAME_INFO (GameInfo::GetSingleton())

//---------------------------
// Constructor & Destructor
//---------------------------

Explosion::Explosion(DOUBLE2 pos)
	:Projectile(pos, false, 0)
	,m_AnimationCount(0.0)
{
	// Bitmap
	m_BmpExplosionPtr = GAME_INFO->GetResourceManager()->GetBitmap(IDB_EXPLOSION);

	// Sprites
	string sIniFile("Resources/SpriteInfo_Projectiles.ini");
	m_ExplosionSpritePtr = new Sprite(sIniFile, "GRENADEEXPLOSION");

	// Audio
	GAME_INFO->GetResourceManager()->PlayAudio(IDR_GRENADEEXPLOSION);

	// HitRegion
	m_HitMainPtr->CreateFromRect(m_Pos.x - 24, m_Pos.y - 48, m_Pos.x + 24, m_Pos.y);
	m_HitMainPtr->SetPos(m_Pos);
	
	// General vars
	m_Velocity = DOUBLE2(0,0);
	m_Type = PROJ_EXPLOSION;
}
Explosion::~Explosion()
{
	// Sprite
	delete m_ExplosionSpritePtr;
	m_ExplosionSpritePtr = nullptr;
}

//---------------------------
// Methods - Member functions
//---------------------------

void Explosion::Tick(double deltaTime)
{
	// Animation
	m_AnimationCount += deltaTime;

	if (m_AnimationCount >= m_ExplosionSpritePtr->GetLength())
		m_HasHit = true;

	// Inheritance
	Projectile::Tick(deltaTime);
}
void Explosion::Paint()
{
	// Calculate matrices
	MATRIX3X2 matCenter, matTranslate, matTotal;

	matCenter.SetAsTranslate(m_ExplosionSpritePtr->GetCenter().x, -m_ExplosionSpritePtr->GetSize().y);
	matTranslate.SetAsTranslate(m_Pos);

	matTotal = matCenter * matTranslate;

	// Paint
	m_ExplosionSpritePtr->PaintSprite(m_BmpExplosionPtr, matTotal, m_AnimationCount);

	// Inheritance
	ObjectEntity::Paint();
}

