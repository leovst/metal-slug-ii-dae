#pragma once
//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//-----------------------------------------------------
// Include Files
//-----------------------------------------------------
//#include "GameEngine.h"

#include "Item.h"

// Forward declarations
class Sprite;

//-----------------------------------------------------
// HeavyMachineUpgrade Class									
//-----------------------------------------------------
class HeavyMachineUpgrade : public Item
{
public:
	HeavyMachineUpgrade(DOUBLE2 pos, int amountOfBullets);				// Constructor
	virtual ~HeavyMachineUpgrade();		// Destructor

	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------

	void Tick(double deltaTime);
	void Paint();

	void Hit();

private: 

	//-------------------------------------------------
	// Datamembers								
	//-------------------------------------------------

	// Sprites
	Sprite *m_UpgradeSpritePtr, *m_DisappearSpritePtr;
	Bitmap *m_BmpGunUpgradesPtr;



	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	HeavyMachineUpgrade(const HeavyMachineUpgrade& yRef);									
	HeavyMachineUpgrade& operator=(const HeavyMachineUpgrade& yRef);	
};

 
