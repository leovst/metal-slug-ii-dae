#pragma once
//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//-----------------------------------------------------
// Include Files
//-----------------------------------------------------
#include "GameEngine.h"

#include "ObjectEntity.h"

// Forward declaration
class Sprite;

//-----------------------------------------------------
// Environment Class									
//-----------------------------------------------------
class Environment : public ObjectEntity
{
public:
	Environment();				// Constructor
	virtual ~Environment();		// Destructor

	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------

	virtual void Tick(double deltaTime);
	void Paint();

	// Paints
	//-------------------------------------------------

	virtual void PaintBack() {}
	virtual void PaintFront() {}

	// Getters & Setters
	//-------------------------------------------------
	
	DOUBLE2 GetCameraPos();
	DOUBLE2 GetNewCameraPos();
	void TranslateCamera(double dx, double dy);
	void TranslateCamera(DOUBLE2 d);
	MATRIX3X2 GetMatView();
	int GetTimeLeft();
	void ResetTime();

	// Overriden and disabled
	void Translate(double dx, double dy) {}

protected:
	
	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------
	
	// Ticks						
	//-------------------------------------------------

	virtual void TickCamera(double deltaTime);
	void TickTimer(double deltaTime);

	// Special Paints
	//-------------------------------------------------

	// Paint with parallax effect depending on cameraposition
	// + Infinite repeated paint
	void PaintParallax(Sprite *spritePtr, double parallaxSpeed, DOUBLE2 offset);


	//-------------------------------------------------
	// Datamembers								
	//-------------------------------------------------
	
	// Bitmaps
	Bitmap *m_BmpEnvironmentPtr;

	// Animation
	double m_AnimationCount;
	double m_AnimationAngle;

	// Camera
	DOUBLE2 m_NewCameraPos, m_CameraPos;
	bool m_IsCameraStopped;

private: 
	//-------------------------------------------------
	// Datamembers								
	//-------------------------------------------------

	int m_TimeLeft;
	double m_Timer;
	static const int MISSION_TIME = 60;


	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	Environment(const Environment& yRef);									
	Environment& operator=(const Environment& yRef);	
};

 
