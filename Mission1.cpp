//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//---------------------------
// Includes
//---------------------------
#include "Mission1.h"

#include "MetalSlug2.h"
#include "GameInfo.h"
#include "BitFont.h"
#include "Sprite.h"

#include <string>
using namespace std;

//---------------------------
// Defines
//---------------------------
#define GAME_ENGINE (GameEngine::GetSingleton())
#define GAME_INFO (GameInfo::GetSingleton())
#define BITFONT (BitFont::GetSingleton())

//---------------------------
// Constructor & Destructor
//---------------------------

Mission1::Mission1()
	:Environment()
	,m_IsBuildingDestroyed(false)
	,m_AudioTimer(0.0)
	,m_StartedLoop(false)
{
	// Bitmaps
	m_BmpEnvironmentPtr = GAME_INFO->GetResourceManager()->GetBitmap(IDB_MISSION1);

	// Sprites
	string sSpriteInfo("Resources/SpriteInfo_Mission1.ini");
	m_EnvironmentMainPtr	 = new Sprite(sSpriteInfo, "MAIN");
	m_EnvironmentFootstepPtr = new Sprite(sSpriteInfo, "FOOTSTEPS");
	m_EnvironmentL3Ptr		 = new Sprite(sSpriteInfo, "LAYER3");
	m_EnvironmentSkyPtr		 = new Sprite(sSpriteInfo, "BGSKY");
	m_EnvironmentWoodPtr	 = new Sprite(sSpriteInfo, "WOODSTRUCTURE");
	m_EnvironmentBuildingBlastPtr = new Sprite(sSpriteInfo, "BLAST");

	// Prepare Footsteps
	m_VisitedMax = m_EnvironmentFootstepPtr->GetSize().x;
	RECT clip = m_EnvironmentFootstepPtr->GetClip();
	clip.left = 104;
	clip.right = clip.left + 16;
	m_EnvironmentFootstepPtr->SetClip(clip);

	// Prepare Layer 2 Animation
	for (int i = 0; i < L2CLIPS; ++i)
		m_EnvironmentL2PtrArr[i] = new Sprite(sSpriteInfo, "LAYER2");

	int clipHeight = (int)(m_EnvironmentL2PtrArr[0]->GetSize().y / L2CLIPS);

	RECT oldClip = m_EnvironmentL2PtrArr[0]->GetClip();
	clip = oldClip;
	for (int i = 0; i < L2CLIPS; ++i)
	{
		clip.top = oldClip.top + i * clipHeight;
		clip.bottom = clip.top + clipHeight + 5;
		m_EnvironmentL2PtrArr[i]->SetClip(clip);
	}

	// Audio
	GAME_INFO->GetResourceManager()->PlayAudio(IDR_MISSIONONESTART, 80);
	GAME_INFO->GetResourceManager()->PlayAudio(IDR_JUDGEMENTINTRO, 100);


	// Load Items
	GAME_INFO->GetObjectManager()->ReadIniFile("Resources/Mission1.ini");

	// Camera start position
	m_CameraStart.x = GAME_INFO->GetObjectManager()->GetHeroStartPosition().x-100;
	m_CameraStart.y = 40;
	m_CameraPos = m_CameraStart;
	m_NewCameraPos = m_CameraStart;

	// Hitregion
	m_HitMainPtr->CreateFromSVG("Resources/EnvironmentHit.svg");
}
Mission1::~Mission1()
{
	// Sprites
	delete m_EnvironmentMainPtr;
	m_EnvironmentMainPtr = nullptr;
	delete m_EnvironmentFootstepPtr;
	m_EnvironmentFootstepPtr = nullptr;
	delete m_EnvironmentL3Ptr;
	m_EnvironmentL3Ptr = nullptr;
	delete m_EnvironmentSkyPtr;
	m_EnvironmentSkyPtr = nullptr;
	delete m_EnvironmentWoodPtr;
	m_EnvironmentWoodPtr = nullptr;
	delete m_EnvironmentBuildingBlastPtr;
	m_EnvironmentBuildingBlastPtr = nullptr;
	for (int i = 0; i < L2CLIPS; ++i)
	{
		delete m_EnvironmentL2PtrArr[i];
		m_EnvironmentL2PtrArr[i] = nullptr;
	}

}


//---------------------------
// Methods - Member functions
//---------------------------

void Mission1::Tick(double deltaTime)
{
	// Inheritance
	Environment::Tick(deltaTime);

	// Audio
	if (m_AudioTimer <= 7.18)
		m_AudioTimer += deltaTime;
	else if (!m_StartedLoop)
	{
		GAME_INFO->GetResourceManager()->PlayAudio(IDR_JUDGEMENTSONG, 100, true);
		m_StartedLoop = true;
	}

	// Show footsteps in desert
	TickFootsteps(deltaTime);
}

// Ticks
//-------------------------------------------------

void Mission1::TickCamera(double deltaTime)
{
	// Change Camera Rail
	double start = 760, stop = 870;
	double height = 16;
	if (m_CameraPos.x > start && m_CameraPos.x < stop)
	{
		// Over a sine function
		double xAngle = ((m_CameraPos.x - start)/(stop - start)) * M_PI;
		m_NewCameraPos.y = height/2 * cos(xAngle) - height/2 + 40; 
	}
	else if (m_CameraPos.x > stop)
		m_NewCameraPos.y = m_CameraStart.y - height;

	double halt = 2342;
	if (!m_IsBuildingDestroyed && m_NewCameraPos.x >= halt && m_NewCameraPos.x < halt + 200)
	{
		m_NewCameraPos.x = halt;
		m_IsCameraStopped = true;
	}
	else
		m_IsCameraStopped = false;

	// Inheritance
	Environment::TickCamera(deltaTime);

}
// Show footsteps where visited
void Mission1::TickFootsteps(double deltaTime)
{
	DOUBLE2 heroPos = GAME_INFO->GetHero()->GetPos();
	RECT clip = m_EnvironmentFootstepPtr->GetClip();
	if (heroPos.x <= clip.left - 8 && heroPos.x >= 8)
		clip.left -= 16;
	if (heroPos.x >= clip.right + 8 && heroPos.x <= m_VisitedMax)
		clip.right += 16;
	m_EnvironmentFootstepPtr->SetClip(clip);
}

// Paints	
//-------------------------------------------------

void Mission1::PaintBack()
{
	//double paralaxSpeed = 1;

	// Paint Layered BG
	PaintSky(); // Backmost layer
	PaintL3();
	PaintL2();
	PaintL1(); // Main Layer

	// Paint footsteps in desert
	PaintFootsteps();

	// Paint Blast when building is destroyed
	if (m_IsBuildingDestroyed)
		PaintBlast();
}
void Mission1::PaintFront()
{
	MATRIX3X2 matWood;
	matWood.SetAsTranslate(613, 141);
	m_EnvironmentWoodPtr->PaintSpriteFrame(m_BmpEnvironmentPtr, matWood, 0);

	// Ineritance
	Environment::Paint();

	// Mission 1 Start
	if (m_AnimationCount < 10)
	{
		BITFONT->PaintExplode("Mission 1\nStart!", DOUBLE2(MetalSlug2::GAMEWIDTH/2, MetalSlug2::GAMEHEIGHT/2), 200, m_AnimationCount, 1.5, 2.8);
	}

}

void Mission1::PaintSky()
{
	if (m_CameraPos.x >= 0 && m_CameraPos.x < 2200)
		PaintParallax(m_EnvironmentSkyPtr, 0.4, DOUBLE2(0,0));
}
void Mission1::PaintL3()
{
	if (m_CameraPos.x >= 0 && m_CameraPos.x < 1340)
		PaintParallax(m_EnvironmentL3Ptr, 0.4, DOUBLE2(0,80));
}
void Mission1::PaintL2()
{
	if (m_CameraPos.x >= 0 && m_CameraPos.x < 1340)
	{
		int clipHeight = (int)(m_EnvironmentL2PtrArr[0]->GetSize().y - 5);

		for (int i = 0; i < L2CLIPS; ++i)
		{
			DOUBLE2 offset(0, 70 + i * clipHeight + sin(m_AnimationAngle*2+i));
			PaintParallax(m_EnvironmentL2PtrArr[i], 0.2, offset);
		}
	}
}
void Mission1::PaintL1()
{
	MATRIX3X2 matId;
	matId.SetAsIdentity();
	m_EnvironmentMainPtr->PaintSpriteFrame(m_BmpEnvironmentPtr, matId, 0);
}
void Mission1::PaintFootsteps()
{
	if (m_CameraPos.x >= 0 && m_CameraPos.x < 1150)
	{
		MATRIX3X2 matFootsteps;
		matFootsteps.SetAsTranslate(m_EnvironmentFootstepPtr->GetClip().left, 40);
		m_EnvironmentFootstepPtr->PaintSpriteFrame(m_BmpEnvironmentPtr, matFootsteps, 0);
	}
}
void Mission1::PaintBlast()
{
	MATRIX3X2 matTranslate, matTotal;
	matTranslate.SetAsTranslate(2512, 8);
	matTotal = matTranslate;
	m_EnvironmentBuildingBlastPtr->PaintSpriteFrame(m_BmpEnvironmentPtr, matTotal, 0);

}

// Setters
//-------------------------------------------------

void Mission1::DestroyBuilding()
{
	m_IsBuildingDestroyed = true;
}