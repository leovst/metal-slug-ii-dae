//-----------------------------------------------------------------
// Game File
// C++ Source - TestHero.cpp - version v2_13 jan 2014 
// Copyright Kevin Hoefman - kevin.hoefman@howest.be
// http://www.digitalartsandentertainment.be/
//-----------------------------------------------------------------

//-----------------------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------------------

//-----------------------------------------------------------------
// Include Files
//-----------------------------------------------------------------
#include "TestHero.h"					

#include "GameInfo.h"
#include "Hero.h"
#include "HeroBehaviour.h"

#include <iostream>
#include <string>
#include <sstream>
using namespace std;

//-----------------------------------------------------------------
// Defines
//-----------------------------------------------------------------
#define GAME_ENGINE (GameEngine::GetSingleton())
#define GAME_INFO (GameInfo::GetSingleton())

//-----------------------------------------------------------------
// TestHero methods																				
//-----------------------------------------------------------------

TestHero::TestHero()
	:m_HeroPtr(nullptr)
	,m_HitPtr(nullptr)
{
	// nothing to create
}

TestHero::~TestHero()																						
{
	// nothing to destroy
}

void TestHero::GameInitialize(HINSTANCE hInstance)			
{
	// Set the required values
	AbstractGame::GameInitialize(hInstance);
	GAME_ENGINE->SetTitle("TestHero - Vansteenkiste Leo - 1DAE6");					
	GAME_ENGINE->RunGameLoop(true);
	GAME_ENGINE->SetBitmapInterpolationModeNearestNeighbor();

	// Set the optional values
	GAME_ENGINE->SetWidth(640);
	GAME_ENGINE->SetHeight(480);
	//GAME_ENGINE->SetKeyList(String("QSDZ") + (TCHAR) VK_SPACE);
	GAME_ENGINE->ConsoleCreate();
	//GAME_ENGINE->LockFramerateToVerticalSynchronisation(true);
}

void TestHero::GameStart()
{
	RECT hit = {-100, 150, 500, 200};

}

void TestHero::GameEnd()
{
}


void TestHero::GameTick(double deltaTime)
{
	m_HeroPtr->Tick(deltaTime);
}

void TestHero::GamePaint(RECT rect)
{
	m_HeroPtr->Paint();
	//MATRIX3X2 matScale;
	//matScale.SetAsScale(m_HeroPtr->GetScale());
	//GAME_ENGINE->SetTransformMatrix(matScale);
	//GAME_ENGINE->SetColor(COLOR(255,0,0,127));
	//GAME_ENGINE->FillHitRegion(m_GameInfoPtr->GetEnvironment()->GetHitRegion());

	// Info to screen
	string movement = "Movement: ", behaviour = "Behaviour: ", direction = "Direction: ";
	stringstream pos, vel;
	pos << "X: " << m_HeroPtr->GetPos().x << "; Y: " << m_HeroPtr->GetPos().y << endl << "Velocity: " << m_HeroPtr->GetVelocity().y;
	m_HeroPtr->GetPos();
	switch (GAME_INFO->GetHero()->GetBehaviour()->GetMovementState())
	{
	case HeroBehaviour::MovementState::STILL:
		movement += "Still";
		break;
	case HeroBehaviour::MovementState::RUN:
		movement += "Run";
		break;
	case HeroBehaviour::MovementState::JUMP:
		movement += "Jump";
		break;
	case HeroBehaviour::MovementState::FALL:
		movement += "Fall";
		break;
	default:
		break;
	}
	switch (m_GameInfoPtr->GetHero()->GetBehaviour()->GetBehaviourState())
	{
	case HeroBehaviour::BehaviourState::IDLE:
		behaviour += "Idle";
		break;
	case HeroBehaviour::BehaviourState::ENDACTION:
		behaviour += "End Action";
		break;
	case HeroBehaviour::BehaviourState::SHOOT:
		behaviour += "Shoot";
		break;
	case HeroBehaviour::BehaviourState::THROW:
		behaviour += "Throw";
		break;
	case HeroBehaviour::BehaviourState::DIE:
		behaviour += "Die";
		break;
	default:
		break;
	}

	if (m_GameInfoPtr->GetHero()->GetBehaviour()->IsLookingDown())
		direction += "Down";
	else if (m_GameInfoPtr->GetHero()->GetBehaviour()->IsLookingUp())
		direction += "Up";
	else
		direction += "Side";

	stringstream behAni, movAni;
	behAni << m_GameInfoPtr->GetHero()->GetBehaviour()->GetBehaviourAnimation();
	movAni << m_GameInfoPtr->GetHero()->GetBehaviour()->GetMovementAnimation();

	// paint
	GAME_ENGINE->SetColor(COLOR(0,0,0));
	MATRIX3X2 mat;
	GAME_ENGINE->SetTransformMatrix(mat);
	int x = 20, y = 20;
	GAME_ENGINE->DrawString(movement + ": " + movAni.str(), x, y);
	y += 20;
	GAME_ENGINE->DrawString(behaviour + ": " + behAni.str(), x, y);
	y += 20;
	GAME_ENGINE->DrawString(direction, x, y);
	y += 20;
	GAME_ENGINE->DrawString(pos.str(), x, y);
}





