#pragma once
//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//-----------------------------------------------------
// Include Files
//-----------------------------------------------------
#include "GameEngine.h"

#include "Bullet.h"

//-----------------------------------------------------
// HeavyBullet Class									
//-----------------------------------------------------
class HeavyBullet : public Bullet
{
public:
	HeavyBullet(DOUBLE2 startPos, DOUBLE2 dirVector);				// Constructor
	virtual ~HeavyBullet();		// Destructor

	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------
	
	void Tick(double deltaTime);
	void Paint();


private: 
	//-------------------------------------------------
	// Datamembers								
	//-------------------------------------------------
	
	Sprite *m_HeavyBulletSpritePtr;
	int m_FrameIndex;

	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	HeavyBullet(const HeavyBullet& yRef);									
	HeavyBullet& operator=(const HeavyBullet& yRef);	
};

 
