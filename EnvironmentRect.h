#pragma once
//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//-----------------------------------------------------
// Include Files
//-----------------------------------------------------
#include "GameEngine.h"
#include "Environment.h"

//-----------------------------------------------------
// EnvironmentRect Class									
//-----------------------------------------------------
class EnvironmentRect : public Environment
{
public:
	EnvironmentRect(RECT hit);				// Constructor
	virtual ~EnvironmentRect();		// Destructor

	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------


private: 
	//-------------------------------------------------
	// Datamembers								
	//-------------------------------------------------


	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	EnvironmentRect(const EnvironmentRect& yRef);									
	EnvironmentRect& operator=(const EnvironmentRect& yRef);	
};

 
