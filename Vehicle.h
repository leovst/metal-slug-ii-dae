#pragma once
//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//-----------------------------------------------------
// Include Files
//-----------------------------------------------------
#include "GameEngine.h"

#include "ObjectEntity.h"

// Forward declaration
class HeroBehaviour;
class Sprite;

//-----------------------------------------------------
// Vehicle Class									
//-----------------------------------------------------
class Vehicle : public ObjectEntity
{
public:
	Vehicle(DOUBLE2 pos, HeroBehaviour *heroBehaviourPtr);				// Constructor
	virtual ~Vehicle();													// Destructor

	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------

	void Tick(double deltaTime);
	void Paint();
	
	// Getters & Setters	
	//-------------------------------------------------

	int GetDirection();

private: 

	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------

	// Ticks	
	//-------------------------------------------------

	void TickStill(double deltaTime);
	void TickRun(double deltaTime);
	void TickJump(double deltaTime);
	void TickFall(double deltaTime);
	
	void TickHitregion(double deltaTime);
	void TickLevelCollisionHandling(double deltaTime);

	// Paints	
	//-------------------------------------------------

	void PaintDebug(MATRIX3X2 matTotal);

	// Movement
	//-------------------------------------------------

	void MoveHero(double deltaTime);


	//-------------------------------------------------
	// Datamembers								
	//-------------------------------------------------

	// Link to hero class
	HeroBehaviour *m_HeroBehaviourPtr;

	// Bitmap
	Bitmap *m_BmpVehiclePtr;

	// Animation
	int m_Direction;
	static const int 
		RUN_SPEED = 100,
		JUMP_SPEED = 180;

	// Sprites
	static const int SPRITE_AMOUNT = 4;
	Sprite *m_SpritePtrArr[SPRITE_AMOUNT];

	// Collision
	bool m_FeetOnGround;


	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	Vehicle(const Vehicle& yRef);									
	Vehicle& operator=(const Vehicle& yRef);	
};


