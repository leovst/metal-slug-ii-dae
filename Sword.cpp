//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//---------------------------
// Includes
//---------------------------
#include "Sword.h"

#include "GameInfo.h"
#include "Sprite.h"

//---------------------------
// Defines
//---------------------------
#define GAME_ENGINE (GameEngine::GetSingleton())
#define GAME_INFO (GameInfo::GetSingleton())

//---------------------------
// Constructor & Destructor
//---------------------------

Sword::Sword(DOUBLE2 pos, DOUBLE2 dirVector)
	:Projectile(pos, true, 1)
	,m_AnimationCount(0.0)
	,m_HasHitGround(false)
{
	// Animation
	m_SwordSpritePtr = new Sprite(m_sIniFile, "SWORD");
	m_SwordEndSpritePtr = new Sprite(m_sIniFile, "SWORDEND");

	// HitRegion
	RECT hit;
	hit.left = (int)m_SwordSpritePtr->GetCenter().x +5;
	hit.right = hit.left + (int)m_SwordSpritePtr->GetSize().x -10;
	hit.top = (int)m_SwordSpritePtr->GetCenter().y - 2;
	hit.bottom = (int)m_SwordSpritePtr->GetCenter().y + 2;

	m_HitMainPtr->CreateFromRect(hit.left, hit.top, hit.right, hit.bottom);
	m_HitMainPtr->SetPos(m_Pos);

	// General vars
	m_Velocity = dirVector;
	m_Type = PROJ_SWORD;
}
Sword::~Sword()
{
	delete m_SwordSpritePtr;
	m_SwordSpritePtr = nullptr;

	delete m_SwordEndSpritePtr;
	m_SwordEndSpritePtr = nullptr;
}

//---------------------------
// Methods - Member functions
//---------------------------

void Sword::Tick(double deltaTime)
{
	// Inheritance
	Projectile::Tick(deltaTime);

	if (!m_HasHitGround)
	{
		// Rotate HitRegion
		MATRIX3X2 matCenter, matRotate, matTranslate, matOffset, matTotal;
		matCenter.SetAsTranslate(0, -m_SwordSpritePtr->GetCenter().y);
		int rotationIndex = (int)(m_AnimationCount*m_SwordSpritePtr->GetSpeed()) % m_SwordSpritePtr->GetNumberOfFrames();
		matRotate.SetAsRotate(-rotationIndex * 2 * M_PI / (m_SwordSpritePtr->GetNumberOfFrames()-1));
		matTranslate.SetAsTranslate(m_Pos);
		matOffset.SetAsTranslate(0, m_SwordSpritePtr->GetCenter().y);

		matTotal = matCenter * matRotate * matOffset * matTranslate;

		m_HitMainPtr->SetTransformMatrix(matTotal);

		TickLevelCollisionHandling(deltaTime);
		TickPlayerCollisionHandling();
	}
	else if (m_AnimationCount > 3)
		m_HasHit = true; // Object can be deleted

	// Animation
	m_AnimationCount += deltaTime;

}
void Sword::Paint()
{
	// Calculate matrices
	MATRIX3X2 matCenter, matDirection, matTranslate, matTotal;
	matCenter.SetAsTranslate(m_SwordSpritePtr->GetCenter().x, -m_SwordEndSpritePtr->GetSize().y);
	int direction = 1;
	if (m_Velocity.x < 0)
		direction = -1;
	matDirection.SetAsScale(direction, 1);
	matTranslate.SetAsTranslate(m_Pos);

	matTotal = matCenter * matDirection * matTranslate;
	if (!m_HasHitGround)
		m_SwordSpritePtr->PaintSprite(m_BmpProjectilesPtr, matTotal, m_AnimationCount);
	else if ((int)(m_AnimationCount * 10) % 5 < 2)
		m_SwordEndSpritePtr->PaintSpriteFrame(m_BmpProjectilesPtr, matTotal, 0);

	// Inheritance
	Projectile::Paint();
}

// Ticks
//-------------------------------------------------

void Sword::TickLevelCollisionHandling(double deltaTime)
{
	DOUBLE2 vector = m_Velocity * deltaTime *1.01;
	HIT hitArr[1];

	if (GAME_INFO->GetEnvironment()->GetHitRegion()->Raycast(m_Pos, vector, hitArr, 1, -1))
	{
		// Set position to hit point
		SetPos(hitArr[0].point);

		// Set to hit ground
		m_HasHitGround = true;
		m_PhysicsEnabled = false;
		SetVelocity(0,0);
		m_AnimationCount = 0;

	}

}
void Sword::TickPlayerCollisionHandling()
{
	if (GAME_INFO->GetHero()->GetHitRegion()->HitTest(m_HitMainPtr) && !GAME_INFO->GetHero()->HasDied())
	{
		if(GAME_INFO->GetHero()->Hit())
			m_HasHit = true;
	}
}

