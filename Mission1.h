#pragma once
//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//-----------------------------------------------------
// Include Files
//-----------------------------------------------------
#include "GameEngine.h"

#include "Environment.h"

//-----------------------------------------------------
// Mission1 Class									
//-----------------------------------------------------
class Mission1 : public Environment
{
public:
	Mission1();				// Constructor
	virtual ~Mission1();		// Destructor

	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------

	void Tick(double deltaTime);

	// Paints	
	//-------------------------------------------------

	void PaintBack();
	void PaintFront();

	// Setters
	//-------------------------------------------------

	void DestroyBuilding();

private: 
	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------

	// Ticks	
	//-------------------------------------------------

	// Mission 1 Camera behaviour
	void TickCamera(double deltaTime);
	void TickFootsteps(double deltaTime);
	
	// Paints	
	//-------------------------------------------------

	void PaintSky();
	void PaintL3();
	void PaintL2();
	void PaintL1();
	void PaintFootsteps();
	void PaintBlast();

	//-------------------------------------------------
	// Datamembers								
	//-------------------------------------------------

	// Behaviour
	double m_VisitedMax;
	bool m_IsBuildingDestroyed;

	// Camera
	DOUBLE2 m_CameraStart;

	// Animation
	Sprite 
		*m_EnvironmentMainPtr, 
		*m_EnvironmentFootstepPtr, 
		*m_EnvironmentL3Ptr, 
		*m_EnvironmentSkyPtr, 
		*m_EnvironmentWoodPtr,
		*m_EnvironmentBuildingBlastPtr;
	static const int L2CLIPS = 10;
	Sprite *m_EnvironmentL2PtrArr[L2CLIPS];

	// Audio
	double m_AudioTimer;
	bool m_StartedLoop;


	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	Mission1(const Mission1& yRef);									
	Mission1& operator=(const Mission1& yRef);	
};

 
