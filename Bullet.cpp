//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//---------------------------
// Includes
//---------------------------
#include "Bullet.h"

#include "GameInfo.h"
#include "Sprite.h"

//---------------------------
// Defines
//---------------------------
//#define GAME_ENGINE (GameEngine::GetSingleton())
#define GAME_INFO (GameInfo::GetSingleton())

//---------------------------
// Constructor & Destructor
//---------------------------

Bullet::Bullet(DOUBLE2 pos, DOUBLE2 dirVector)
	:Projectile(pos, false, 1)
	,m_DirectionVector(dirVector)
{
	// Animation
	m_BulletSpritePtr = new Sprite(m_sIniFile, "REGULARBULLET");

	// General vars
	m_Velocity = dirVector * 500; // Set speed of bullets
	m_Type = PROJ_BULLET;

}
Bullet::~Bullet()
{
	// Sprite
	delete m_BulletSpritePtr;
	m_BulletSpritePtr = nullptr;
}

//---------------------------
// Methods - Member functions
//---------------------------

void Bullet::Tick(double deltaTime)
{
	// Inheritance
	Projectile::Tick(deltaTime);
}
void Bullet::Paint()
{
	// Calculate matrices
	MATRIX3X2 matCenter, matRotate, matTranslate, matTotal;

	DOUBLE2 size = m_BulletSpritePtr->GetSize();
	matCenter.SetAsTranslate(-size.x/2, -size.y/2);
	if (abs(m_DirectionVector.y) == 1)
		matRotate.SetAsRotate(M_PI/2);

	matTranslate.SetAsTranslate(m_Pos);

	matTotal = matCenter * matRotate * matTranslate;

	// Paint
	m_BulletSpritePtr->PaintSpriteFrame(m_BmpProjectilesPtr, matTotal, 0);

	// Inheritanc
	Projectile::Paint();
}
