//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//---------------------------
// Includes
//---------------------------
#include "EnvironmentObject.h"

//---------------------------
// Defines
//---------------------------
//#define GAME_ENGINE (GameEngine::GetSingleton())
//#define GAME_INFO (GameInfo::GetSingleton())

//---------------------------
// Constructor & Destructor
//---------------------------

EnvironmentObject::EnvironmentObject(DOUBLE2 pos, bool canHit, bool canPass)
	:ObjectEntity(pos, false)
	,m_CanHit(canHit)
	,m_CanPass(canPass)
	,m_AnimationCount(0.0)
{
	// nothing to create
}
EnvironmentObject::~EnvironmentObject()
{
	// nothing to destroy
}

//---------------------------
// Methods - Member functions
//---------------------------


void EnvironmentObject::Tick(double deltaTime)
{
	m_AnimationCount += deltaTime;

	// Inheritance
	ObjectEntity::Tick(deltaTime);
}
void EnvironmentObject::Paint()
{
	// Inheritance
	ObjectEntity::Paint();
}



// Getters
//-------------------------------------------------

bool EnvironmentObject::canHit()
{
	return m_CanHit;
}
bool EnvironmentObject::canPass()
{
	return m_CanPass;
}