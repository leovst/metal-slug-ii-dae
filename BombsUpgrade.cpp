//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//---------------------------
// Includes
//---------------------------
#include "BombsUpgrade.h"

#include "GameInfo.h"
#include "Sprite.h"

#include <string>
using namespace std;

//---------------------------
// Defines
//---------------------------
//#define GAME_ENGINE (GameEngine::GetSingleton())
#define GAME_INFO (GameInfo::GetSingleton())

//---------------------------
// Constructor & Destructor
//---------------------------
BombsUpgrade::BombsUpgrade(DOUBLE2 pos, int amountOfBombs)
	:Item(pos, amountOfBombs)
{
	
	// Sprites
	string sIniFile = "Resources/SpriteInfo_GunUpgrades.ini";
	m_UpgradeSpritePtr = new Sprite(sIniFile, "BOMBPACK");
	m_DisappearSpritePtr = new Sprite(sIniFile, "DISAPPEAR");
	m_GhostSpritePtr = new Sprite(sIniFile, "BOMBSGHOST");

	// Bitmap
	m_BmpUpgradesPtr = GAME_INFO->GetResourceManager()->GetBitmap(IDB_GUNUPGRADES);

	// Hitregion
	m_HitMainPtr->CreateFromRect(m_UpgradeSpritePtr->GetCenter().x, -m_UpgradeSpritePtr->GetSize().y, -m_UpgradeSpritePtr->GetCenter().x, 0);
	m_HitMainPtr->SetPos(m_Pos);
}

BombsUpgrade::~BombsUpgrade()
{
	// Sprites
	delete m_UpgradeSpritePtr;
	m_UpgradeSpritePtr = nullptr;

	delete m_DisappearSpritePtr;
	m_DisappearSpritePtr = nullptr;

	delete m_GhostSpritePtr;
	m_GhostSpritePtr = nullptr;

	// Bitmaps
	GAME_INFO->GetResourceManager()->ReleaseBitmap(m_BmpUpgradesPtr);
	m_BmpUpgradesPtr = nullptr;
}

//---------------------------
// Methods - Member functions
//---------------------------

void BombsUpgrade::Tick(double deltaTime)
{
	// Inheritance
	Item::Tick(deltaTime);

	// Mark for deletion
	if (m_IsTriggered && m_AnimationCount > m_DisappearSpritePtr->GetLength())
		SetDeleteStatus(true);
}
void BombsUpgrade::Paint()
{
	// Inheritance
	Item::Paint();

	// Calculate Matrices
	MATRIX3X2 matCenter, matTranslate, matTotal;
	if (!m_IsTriggered)
		matCenter.SetAsTranslate(m_UpgradeSpritePtr->GetCenter().x, -m_UpgradeSpritePtr->GetSize().y);
	else
		matCenter.SetAsTranslate(m_DisappearSpritePtr->GetCenter().x, -m_DisappearSpritePtr->GetSize().y);
	matTranslate.SetAsTranslate(m_Pos);
	matTotal = matCenter * matTranslate;

	// Paint
	if (!m_IsTriggered)
		m_UpgradeSpritePtr->PaintSpriteFrame(m_BmpUpgradesPtr, matTotal, 0);
	else
	{
		// Disappear (poof)
		m_DisappearSpritePtr->PaintSprite(m_BmpUpgradesPtr, matTotal, m_AnimationCount);

		// Ghost
		if ((int)(m_AnimationCount * 5) %2 == 0)
			m_GhostSpritePtr->PaintSpriteFrame(m_BmpUpgradesPtr,matTotal, 0);
	}

}
void BombsUpgrade::Hit()
{
	// Upgrade Hero
	GAME_INFO->GetHero()->AddBombs(m_Content);

	m_IsTriggered = true;
}

