#pragma once
//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//-----------------------------------------------------
// Include Files
//-----------------------------------------------------
#include "GameEngine.h"
//-----------------------------------------------------
// ObjectEntity Class									
//-----------------------------------------------------
class ObjectEntity
{
public:
	ObjectEntity(DOUBLE2 pos, bool physicsEnabled);					// Constructor
	virtual ~ObjectEntity() = 0;									// Destructor

	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------
	
	virtual void Tick(double deltaTime);
	virtual void Paint();


	// Getters & Setters	
	//-------------------------------------------------

	// Position

	DOUBLE2 GetPos();
	void SetPos(double x, double y);
	void SetPos(DOUBLE2 pos);
	void Translate(double dx, double dy);
	void Translate(DOUBLE2 d);

	// Distance from object to point
	DOUBLE2 DistanceTo(DOUBLE2 other);

	// Movement

	DOUBLE2 GetVelocity();
	void SetVelocity(double vx, double vy);

	// Is object visible
	int GetOutBounds();

	// Get HitRegion
	HitRegion *GetHitRegion();

	// Deletion
	bool GetDeleteStatus();
	void SetDeleteStatus(bool deleteStatus);


	// Debug	
	//-------------------------------------------------

	static void SetDebug(bool showDebug);
	static bool GetDebug();


protected: 
	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------

	// Returns the general view matrix with scaling included
	MATRIX3X2 GetMatView();

	//-------------------------------------------------
	// Datamembers								
	//-------------------------------------------------

	// Basic Properties
	DOUBLE2 m_Pos, m_Velocity, m_Gravity;
	bool m_PhysicsEnabled;
	//bool m_IsOutBounds;

	// HitRegion
	HitRegion *m_HitMainPtr;

	// Deletion
	bool m_CanDelete;

	// Debug
	static bool m_ShowDebug;

private: 
	 
	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------

	// Ticks	
	//-------------------------------------------------

	// Check if object is within camera view (+30 px border)
	int TickCheckBounds();

	//-------------------------------------------------
	// Datamembers								
	//-------------------------------------------------


	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	ObjectEntity(const ObjectEntity& yRef);									
	ObjectEntity& operator=(const ObjectEntity& yRef);	
};

 
