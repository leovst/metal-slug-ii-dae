#pragma once
//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//-----------------------------------------------------
// Include Files
//-----------------------------------------------------
#include "GameEngine.h"

#include "ObjectEntity.h"

//-----------------------------------------------------
// Defines
//-----------------------------------------------------
#define PROJ_BULLET 0
#define PROJ_HBULLET 1
#define PROJ_GRENADE 2
#define PROJ_EXPLOSION 3
#define PROJ_SWORD 4

//-----------------------------------------------------
// Projectile Class									
//-----------------------------------------------------
class Projectile : public ObjectEntity
{
public:
	Projectile(DOUBLE2 pos, bool physicsEnabled, int strength);				// Constructor
	virtual ~Projectile();		// Destructor

	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------
	
	virtual void Tick(double deltaTime);
	virtual void Paint();

	// Getters	
	//-------------------------------------------------

	bool GetHasHit();
	// 0 = Bullet, 1 = HeavyBullet, 2 = Grenade, 3 = Explosion, 4 = Sword
	int GetType();

protected: 
	//-------------------------------------------------
	// Datamembers								
	//-------------------------------------------------

	// Bitmap
	Bitmap *m_BmpProjectilesPtr;

	// Initialization file
	std::string m_sIniFile;

	// General info
	int m_Type;
	bool m_HasHit;
	int m_Strength;

private: 

	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------

	// Ticks
	//-------------------------------------------------

	void TickNPCCollisionHandling(double deltaTime);
	void TickEnvironmentCollisionHandling(double deltaTime);


	//-------------------------------------------------
	// Datamembers								
	//-------------------------------------------------



	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	Projectile(const Projectile& yRef);									
	Projectile& operator=(const Projectile& yRef);	
};

 
