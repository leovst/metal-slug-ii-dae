//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//---------------------------
// Includes
//---------------------------
#include "HUD.h"

#include "MetalSlug2.h"
#include "GameInfo.h"
#include "Sprite.h"
#include "BitFont.h"

#include "HeroBehaviour.h"

#include <string>
#include <sstream>
using namespace std;

//---------------------------
// Defines
//---------------------------
#define GAME_ENGINE (GameEngine::GetSingleton())
#define GAME_INFO (GameInfo::GetSingleton())
#define BITFONT (BitFont::GetSingleton())

//---------------------------
// Constructor & Destructor
//---------------------------
HUD::HUD()
	:m_AmountOfBullets(0)
	,m_AmountOfGrenades(0)
	,m_AmountOfLives(0)
	,m_AmountOfPoints(0)
	,m_AmountOfRescues(0)
	,m_TimeLeft(0)
{
	// Bitmap
	m_BmpHudPtr = GAME_INFO->GetResourceManager()->GetBitmap(IDB_HUD);

	// Sprites
	string sIniFile("Resources/SpriteInfo_HUD.ini");
	m_WeaponsFrameSpritePtr = new Sprite(sIniFile, "WEAPONFRAME");
	m_RescuedSpritePtr = new Sprite(sIniFile, "RESCUED");
	m_InfiniteSpritePtr = new Sprite(sIniFile, "INFINITE");
	m_GoRightSpritePtr = new Sprite(sIniFile, "GORIGHT");

	// Hero
	m_HeroPtr = GAME_INFO->GetHero();
}

HUD::~HUD()
{
	// Bitmap
	GAME_INFO->GetResourceManager()->ReleaseBitmap(m_BmpHudPtr);
	m_BmpHudPtr = nullptr;

	// Sprites
	delete m_WeaponsFrameSpritePtr;
	m_WeaponsFrameSpritePtr = nullptr;

	delete m_RescuedSpritePtr;
	m_RescuedSpritePtr = nullptr;

	delete m_InfiniteSpritePtr;
	m_InfiniteSpritePtr = nullptr;

	delete m_GoRightSpritePtr;
	m_GoRightSpritePtr = nullptr;

	// Hero
	m_HeroPtr = nullptr;

	BITFONT->Release();
}

//---------------------------
// Methods - Member functions
//---------------------------

void HUD::Tick(double deltaTime)
{
	// Get data to show
	m_AmountOfBullets = m_HeroPtr->GetAmountOfBullets();
	m_AmountOfGrenades = m_HeroPtr->GetAmountOfGrenades();
	m_AmountOfLives = m_HeroPtr->GetAmountOfLives();
	m_AmountOfPoints = m_HeroPtr->GetAmountOfPoints();
	m_AmountOfRescues = m_HeroPtr->GetAmountOfRescues();
	m_TimeLeft = GAME_INFO->GetEnvironment()->GetTimeLeft();
}
void HUD::Paint()
{
	MATRIX3X2 matFrame;

	// Weapon Frame
	PaintWeaponFrame(matFrame);

	// Hero Data
	PaintHeroData(matFrame);

	// Time Left
	PaintTime(matFrame);

	// Debug
	if (GAME_INFO->GetShowDebug())
		PaintDebug();
}

// Paints
//-------------------------------------------------

void HUD::PaintWeaponFrame(MATRIX3X2 &matFrame)
{
	// Genral var
	stringstream buffer;

	// General matrices
	MATRIX3X2 matCam, matTotal;
	matCam.SetAsTranslate(GAME_INFO->GetEnvironment()->GetCameraPos());

	// FRAME
	matFrame.SetAsTranslate(70, 5);
	matTotal = matCam * matFrame;
	m_WeaponsFrameSpritePtr->PaintSpriteFrame(m_BmpHudPtr, matTotal, 0);

	// BULLETS
	MATRIX3X2 matBullets;
	if (m_AmountOfBullets == -1)
	{
		// Infinite
		matBullets.SetAsTranslate(11, 8);
		matTotal = matCam * matBullets * matFrame;
		m_InfiniteSpritePtr->PaintSpriteFrame(m_BmpHudPtr, matTotal, 0);
	}
	else
	{
		// Show amount
		matBullets.SetAsTranslate(31, 8);
		matTotal = matBullets * matFrame;
		buffer << m_AmountOfBullets;
		BITFONT->PaintString(buffer.str(), matTotal, 1, false);
	}

	// GRENADES
	MATRIX3X2 matGrenades;
	matGrenades.SetAsTranslate(33,8);
	matTotal = matGrenades * matFrame;
	buffer.clear();
	buffer.str("");
	buffer << m_AmountOfGrenades;
	BITFONT->PaintString(buffer.str(), matTotal, 1);
}
void HUD::PaintHeroData(const MATRIX3X2 &matFrame)
{
	// Genral var
	stringstream buffer;

	// General matrices
	MATRIX3X2 matCam, matTotal;
	matCam.SetAsTranslate(GAME_INFO->GetEnvironment()->GetCameraPos());

	// POINTS
	MATRIX3X2 matPoints;
	matPoints.SetAsTranslate(-2, 0);
	matTotal = matPoints * matFrame;
	buffer.clear();
	buffer.str("");
	buffer << m_AmountOfPoints;
	BITFONT->PaintString(buffer.str(), matTotal, 2, false);

	// LIVES
	MATRIX3X2 matLives;
	matLives.SetAsTranslate(0, 9);
	matTotal = matPoints * matLives * matFrame;
	buffer.clear();
	buffer.str("");
	buffer << "1UP=" << m_AmountOfLives;
	BITFONT->PaintString(buffer.str(), matTotal, 0, false);

	// RESCUES
	int distance = (int)m_RescuedSpritePtr->GetSize().y + 4;
	MATRIX3X2 matAdd, matRescues;
	matRescues.SetAsTranslate(10, MetalSlug2::GAMEHEIGHT-18);
	matAdd.SetAsTranslate(distance, 0);
	matTotal = matCam * matRescues;

	for (int i = 0; i < m_AmountOfRescues; ++i)
	{
		m_RescuedSpritePtr->PaintSpriteFrame(m_BmpHudPtr, matTotal, 0);
		matTotal = matTotal * matAdd;
	}
}
void HUD::PaintTime(const MATRIX3X2 &matFrame)
{
	// Genral var
	stringstream buffer;

	// General matrices
	MATRIX3X2 matCam, matTotal;
	matCam.SetAsTranslate(GAME_INFO->GetEnvironment()->GetCameraPos());

	// TIME LEFT
	MATRIX3X2 matTime;
	matTime.SetAsTranslate(96, 1);
	matTotal = matTime * matFrame;
	buffer.clear();
	buffer.str("");
	string leadingZero("");
	if (m_TimeLeft < 10)
		leadingZero = "0";
	buffer << leadingZero << m_TimeLeft;
	BITFONT->PaintString(buffer.str(), matTotal, 3, false);
}

void HUD::PaintDebug()
{
	// Info
	MATRIX3X2 matTranslate, matAdd, matTotal;
	matTranslate.SetAsTranslate(MetalSlug2::GAMEWIDTH - 250, MetalSlug2::GAMEHEIGHT - 35);
	matAdd.SetAsTranslate(0, 7);
	matTotal = matTranslate;

	stringstream buffer;
	buffer.precision(0);
	buffer.setf( ios::fixed, ios::floatfield );
	buffer << "Hero -\tPx: " << GAME_INFO->GetHero()->GetPos().x << ";\tPy: " << GAME_INFO->GetHero()->GetPos().y << endl;
	buffer << "Cam - \tPx: " << GAME_INFO->GetEnvironment()->GetCameraPos().x << ";\tPy: " << GAME_INFO->GetEnvironment()->GetCameraPos().y << endl;
	buffer << "Beh - \tB: " << (int)GAME_INFO->GetHero()->GetBehaviour()->GetBehaviourState() << "; \tM: " << (int)GAME_INFO->GetHero()->GetBehaviour()->GetMovementState();
	BITFONT->PaintString(buffer.str(), matTotal, 2);
}