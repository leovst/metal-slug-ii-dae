//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//---------------------------
// Includes
//---------------------------
#include "Sprite.h"

#include "GameInfo.h"
#include "ResourceManager.h"

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>

using namespace std;

//---------------------------
// Defines
//---------------------------
#define GAME_ENGINE (GameEngine::GetSingleton())
#define GAME_INFO (GameInfo::GetSingleton())

//---------------------------
// Constructor & Destructor
//---------------------------
Sprite::Sprite(string sInfoFile, string sBeh, string sMov, string sDir)
	:m_CompletedLoop(false)
	,m_NumberOfFrames(0)
	,m_Columns(0)
	,m_Speed(0)
	,m_Length(0)
	,m_AddReverse(false)
	,m_Reversed(false)
	,m_StayLast(false)
{
	m_Clip.left = 0; m_Clip.top = 0; m_Clip.right = 0; m_Clip.bottom = 0;

	// Read File and initializes vars
	ReadFile(sInfoFile, sBeh, sMov, sDir);
}
Sprite::Sprite(string sInfoFile, string sSectionName)
	:m_CompletedLoop(false)
	,m_NumberOfFrames(0)
	,m_Columns(0)
	,m_Speed(0)
	,m_Length(0)
	,m_AddReverse(false)
	,m_Reversed(false)
	,m_StayLast(false)
{
	// Read File and initializes vars
	ReadFile(sInfoFile, sSectionName);
}
Sprite::~Sprite()
{
	// Nothing to destroy
}

//---------------------------
// Methods - Member functions
//---------------------------

// Paints
//-------------------------------------------------

void Sprite::PaintSprite(Bitmap *bmpSpriteSheetPtr, const MATRIX3X2 &matTotalRef, const double &animationCountRef, int direction, bool transparent)
{
	// Checks if the sprite contains any frames
	if (IsLoaded())
	{
		if (m_StayLast && animationCountRef < 0.02)
			m_CompletedLoop = false;
		int spriteIndex = (int)(animationCountRef*m_Speed) % m_NumberOfFrames;
		RECT clip = m_Clip;

		// Add reversed loop
		if (m_AddReverse)
		{
			spriteIndex = (int)(animationCountRef*m_Speed) % (m_NumberOfFrames*2);
			if (spriteIndex >= m_NumberOfFrames)
				spriteIndex = m_NumberOfFrames*2 -spriteIndex-1;
		}
		// Stay at last frame
		if (m_StayLast)
		{
			if (spriteIndex == m_NumberOfFrames-1)
				m_CompletedLoop = true;
			if (m_CompletedLoop)
				spriteIndex = m_NumberOfFrames-1;
		}
		else
			m_CompletedLoop = false;

		// Reverse complete loop
		if (m_Reversed)
			spriteIndex = m_NumberOfFrames -spriteIndex-1;

		int clipWidth = m_Clip.right - m_Clip.left;
		int clipHeight = m_Clip.bottom - m_Clip.top;
		clip.left += clipWidth * (spriteIndex % m_Columns);
		clip.top += clipHeight * (spriteIndex / m_Columns);
		clip.right = clip.left + clipWidth;
		clip.bottom = clip.top + clipHeight;

		// Paint
		MATRIX3X2 matOffset, matFinal;
		matOffset.SetAsTranslate(direction * m_Offset.x, m_Offset.y);
		matFinal = matTotalRef * matOffset * GAME_INFO->GetEnvironment()->GetMatView();

		GAME_ENGINE->SetTransformMatrix(matFinal);

		double opacity = 1;
		if (transparent)
		{
			int angle = (int)(animationCountRef * 100) % (int)(2 * M_PI * 100);
			opacity = 0.25*sin(100*angle) + 0.75;
		}

		bmpSpriteSheetPtr->SetOpacity(opacity);
		GAME_ENGINE->DrawBitmap(bmpSpriteSheetPtr, 0, 0, clip);
	}
}
void Sprite::PaintSpriteFrame(Bitmap *bmpSpriteSheetPtr, const MATRIX3X2 &matTotalRef, int frameNumber)
{
	// Checks if given frame exists
	if (IsLoaded() && frameNumber < m_NumberOfFrames)
	{
		RECT clip = m_Clip;
		int clipWidth = m_Clip.right - m_Clip.left;
		int clipHeight = m_Clip.bottom - m_Clip.top;

		clip.left += clipWidth * (frameNumber % m_Columns);
		clip.top += clipHeight * (frameNumber / m_Columns);
		clip.right = clip.left + clipWidth;
		clip.bottom = clip.top + clipHeight;

		// Paint
		MATRIX3X2 matOffset, matFinal;
		matOffset.SetAsTranslate(m_Offset);
		matFinal = matTotalRef * matOffset * GAME_INFO->GetEnvironment()->GetMatView();

		GAME_ENGINE->SetTransformMatrix(matFinal);
		GAME_ENGINE->DrawBitmap(bmpSpriteSheetPtr, 0, 0, clip);
	}
}

// File & String parsing
//-------------------------------------------------

bool Sprite::ReadFile(string sInfoFile, string sBeh, string sMov, string sDir)
{
	// Read file
	ifstream file(sInfoFile);

	// Check if file exists
	if (file)
	{
		// Special chars
		string sComment = ";";

		string sLine;
		bool isCorrectSection = false;

		while (!file.eof())
		{
			getline(file, sLine);

			// Check if line is commented
			if (sLine.find(sComment) == string::npos)
			{
				// Check if line is new section
				if (sLine.find("[") != string::npos && isCorrectSection)
				{
					isCorrectSection = false;
					break;
				}

				// Check if line is correct section
				if (sLine.find("[") != string::npos &&
					sLine.find(sBeh) != string::npos &&
					sLine.find(sMov) != string::npos &&
					sLine.find(sDir) != string::npos)
				{
					isCorrectSection = true;
					cout << sBeh << "_" << sMov << "_" << sDir << endl;
				}

				if (isCorrectSection)
					if (!ParseString(sLine))
						return false;
			}
		}
		file.close();

	}
	else 
		return false;

	return true;
}
bool Sprite::ReadFile(string sInfoFile, string sSectionName)
{
	// Read file
	ifstream file(sInfoFile);

	// Check if file exists
	if (file)
	{
		// Special chars
		string sComment = ";";

		string sLine;
		bool isCorrectSection = false;

		while (!file.eof())
		{
			getline(file, sLine);

			// Check if line is commented
			if (sLine.find(sComment) == string::npos)
			{
				if (sLine.find("[") != string::npos && isCorrectSection)
				{
					isCorrectSection = false;
					break;
				}

				if (sLine.find("[" + sSectionName + "]") != string::npos)
				{
					isCorrectSection = true;
					//cout << sSectionName << endl;
				}

				if (isCorrectSection)
					if (!ParseString(sLine))
						return false;
			}
		}
		file.close();

	}
	else 
		return false;

	return true;
}

// Puts all required data into correct vars
bool Sprite::ParseString(string input)
{
	// Special chars
	string sAssign = "=";

	// Remove all whitespaces
	// http://stackoverflow.com/a/83481
	input.erase(remove(input.begin(), input.end(), ' '), input.end());
	input.erase(remove(input.begin(), input.end(), '\t'), input.end());

	// Get variable name and value as int
	string::size_type assignLoc = input.find_first_of(sAssign);
	if (assignLoc != string::npos)
	{
		string sName = input.substr(0, assignLoc);
		int value = stoi( input.substr(assignLoc+1, input.length()-assignLoc+1) );

		// Assign values to correct vars
		if (sName == "left")
			m_Clip.left = value;
		if (sName == "top")
			m_Clip.top = value;
		if (sName == "width")
			m_Clip.right = m_Clip.left + value;
		if (sName == "height")
			m_Clip.bottom = m_Clip.top + value;
		if (sName == "offsetx")
			m_Offset.x = value;
		if (sName == "offsety")
			m_Offset.y = value;
		if (sName == "clips")
		{
			m_NumberOfFrames = value;
			if (m_Columns <= 0)
				m_Columns = value;
		}
		if (sName == "cols")
			m_Columns = value;
		if (sName == "speed")
			m_Speed = value;
		if (sName == "length")
			m_Length = value;
		if (sName == "addreverse")
			m_AddReverse = ((value == 0) ? false : true);
		if (sName == "reversed")
			m_Reversed = ((value == 0) ? false : true);
		if (sName == "staylast")
			m_StayLast = ((value == 0) ? false : true);
	}
	return true;
}


// Setters & Getters
//-------------------------------------------------

RECT Sprite::GetClip()
{
	return m_Clip;
}
void Sprite::SetClip(RECT clip)
{
	m_Clip = clip;
}

int Sprite::GetSpeed()
{
	return m_Speed;
}
void Sprite::SetSpeed(int speed)
{
	if (speed > 0)
		m_Speed = speed;
}

DOUBLE2 Sprite::GetOffset()
{
	return m_Offset;
}
// Returns Center point
DOUBLE2 Sprite::GetCenter()
{
	if (m_Clip.right > 0)
	{
		int clipWidth = m_Clip.right - m_Clip.left;
		int clipHeight = m_Clip.bottom - m_Clip.top;

		return DOUBLE2(-clipWidth/2, -clipHeight/2);
	}
	else 
		return DOUBLE2(0,0);
}
DOUBLE2 Sprite::GetSize()
{
	return DOUBLE2 (m_Clip.right - m_Clip.left, m_Clip.bottom - m_Clip.top);
}
int Sprite::GetNumberOfFrames()
{
	return m_NumberOfFrames;
}
// In seconds
double Sprite::GetLength()
{
	if (m_Length != -1)
		return ((double) m_Length)/1000;
	else return -1;
}

bool Sprite::IsLoaded()
{
	if (m_NumberOfFrames > 0)
		return true;
	else
		return false;
}


