//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//---------------------------
// Includes
//---------------------------
#include "Hero.h"

#include "MetalSlug2.h"
#include "GameInfo.h"
#include "ObjectEntity.h"
#include "Sprite.h"
#include "Projectile.h"

#include "HeroBehaviour.h"
#include "Vehicle.h"

#include "NPC.h"

#include <string>
using namespace std;

//---------------------------
// Defines
//---------------------------
#define GAME_ENGINE (GameEngine::GetSingleton())
#define GAME_INFO (GameInfo::GetSingleton())

//---------------------------
// Constructor & Destructor
//---------------------------

Hero::Hero()
	:ObjectEntity(GAME_INFO->GetObjectManager()->GetHeroStartPosition(), true)
	,m_Direction(1)
	,m_ShootingAngle(0.0)
	,m_ActCounter(0.0)
	,m_ActTime(0.35)
	,m_ShotsFired(0)
	,m_AmountOfBullets(-1)
	,m_AmountOfGrenades(10)
	,m_AmountOfLives(2)
	,m_AmountOfPoints(0)
	,m_AmountOfRescues(0)
	,m_HasHeavyGun(false)
	,m_IsInvincible(false)
	,m_InvincibleTimer(0.0)
{
	// ASSISTING OBJECTS
	//-----------------------------------------------------
	m_HeroBehaviourPtr = new HeroBehaviour();
	m_VehiclePtr = new Vehicle(m_Pos, m_HeroBehaviourPtr);

	// DISABLE HITREGION
	//-----------------------------------------------------
	if (m_HitMainPtr != nullptr)
	{
		delete m_HitMainPtr;
		m_HitMainPtr = nullptr;
	}

	// SPRITES
	//-----------------------------------------------------

	// Bitmap
	m_BmpHeroPtr = GAME_INFO->GetResourceManager()->GetBitmap(IDB_MARCOBODY);

	// Sprite info
	m_SBehaviourArr[0] = "IDLE";
	m_SBehaviourArr[1] = "SHOOT";
	m_SBehaviourArr[2] = "THROW";
	m_SBehaviourArr[3] = "ATTACK";
	m_SMovementArr[0] = "STILL";
	m_SMovementArr[1] = "RUN";
	m_SMovementArr[2] = "JUMP";
	m_SMovementArr[3] = "FALL";
	m_SMovementArr[4] = "RUNJUMP";
	m_SDirectionArr[0] = "SIDE";
	m_SDirectionArr[1] = "UP";
	m_SDirectionArr[2] = "DOWN";

	string sIniFile = "Resources/SpriteInfo_MarcoBody.ini";

	// Normal Sprites
	for (int beh = 0; beh < NR_BEHAVIOUR; ++beh)
		for (int mov = 0; mov < NR_MOVEMENT; ++mov)
			for (int dir = 0; dir < NR_DIRECTION; ++dir)
				m_SpritePtrArr[beh][mov][dir] = new Sprite(sIniFile, m_SBehaviourArr[beh], m_SMovementArr[mov], m_SDirectionArr[dir]);
	// Special Sprites
	m_EndActionSpritePtr = new Sprite(sIniFile, "ENDACTION");
	m_DieSpritePtr = nullptr; 
	m_Die1SpritePtr = new Sprite(sIniFile, "DIE1");
	m_Die2SpritePtr = new Sprite(sIniFile, "DIE2");
	m_RespawnSpritePtr = new Sprite(sIniFile, "RESPAWN");
}
Hero::~Hero()
{
	// ASSISTING OBJECTS
	//-----------------------------------------------------
	delete m_HeroBehaviourPtr;
	m_HeroBehaviourPtr = nullptr;

	delete m_VehiclePtr;
	m_VehiclePtr = nullptr;

	// SPRITES
	//-----------------------------------------------------

	for (int beh = 0; beh < NR_BEHAVIOUR; ++beh)
	{
		for (int mov = 0; mov < NR_MOVEMENT; ++mov)
			for (int dir = 0; dir < NR_DIRECTION; ++dir)
			{
				delete m_SpritePtrArr[beh][mov][dir];
				m_SpritePtrArr[beh][mov][dir] = nullptr;
			}
	}
	delete m_EndActionSpritePtr;
	m_EndActionSpritePtr = nullptr;

	delete m_Die1SpritePtr;
	m_Die1SpritePtr = nullptr;

	delete m_Die2SpritePtr;
	m_Die2SpritePtr = nullptr;

	delete m_RespawnSpritePtr;
	m_RespawnSpritePtr = nullptr;

	// Bitmap
	GAME_INFO->GetResourceManager()->ReleaseBitmap(m_BmpHeroPtr);
	m_BmpHeroPtr = nullptr;

}

//---------------------------
// Methods - Member functions
//---------------------------

void Hero::Tick(double deltaTime)
{
	// Invincible timer
	TickInvincibleTimer(deltaTime);

	// Behaviour tick
	m_HeroBehaviourPtr->Tick(deltaTime);

	// Movement - Going through the vehicle class
	m_VehiclePtr->Tick(deltaTime);
	TickGetVehicleData();

	// Behaviour
	TickBehaviour(deltaTime);
}
void Hero::Paint()
{
	// PAINT VEHICLE
	m_VehiclePtr->Paint();

	// PAINT BODY
	PaintBody();

	// Debug
	PaintDebug();
}

bool Hero::Hit()
{
	// Do nothing if dead or invincible
	if (m_IsInvincible || HasDied())
		return false;

	// Pass to assisting objects
	m_HeroBehaviourPtr->SetBehaviourState(HeroBehaviour::BehaviourState::DIE);
	m_VehiclePtr->SetVelocity(m_VehiclePtr->GetVelocity().x, m_VehiclePtr->GetVelocity().y/2);

	// Play Sound
	GAME_INFO->GetResourceManager()->PlayAudio(IDR_HERODIE);

	return true;
}

// Ticks	
//-------------------------------------------------

// Syncs movement data from Vehicle class
void Hero::TickGetVehicleData()
{
	m_Pos = m_VehiclePtr->GetPos();
	m_Velocity = m_VehiclePtr->GetVelocity();
	m_Direction = m_VehiclePtr->GetDirection();
}
void Hero::TickInvincibleTimer(double deltaTime)
{
	// Decrease invincible time
	if (m_IsInvincible && m_InvincibleTimer > 0)
	{
		m_InvincibleTimer -= deltaTime;
		if (m_InvincibleTimer <= 0)
			m_IsInvincible = false;
	}

}
void Hero::TickBehaviour(double deltaTime)
{
	// Add to act counter
	m_ActCounter += deltaTime;

	// Get states
	HeroBehaviour::BehaviourState behaviourState = m_HeroBehaviourPtr->GetBehaviourState();
	HeroBehaviour::MovementState movementState = m_HeroBehaviourPtr->GetMovementState();

	int behaviourIndex = (int)behaviourState;
	int movementIndex = (int)movementState;
	int lookIndex = 0;
	if (m_HeroBehaviourPtr->IsLookingDown())
		lookIndex = 1;
	else if (m_HeroBehaviourPtr->IsLookingUp())
		lookIndex = 2;

	// Get animation data
	double behAnimation = m_HeroBehaviourPtr->GetBehaviourAnimation();

	// Default length
	double length = 1000; // default will show incorrect animation if wrong

	if (behaviourIndex < NR_BEHAVIOUR)
		length = m_SpritePtrArr[behaviourIndex][movementIndex][lookIndex]->GetLength();

	// Specefic lengths
	else if (behaviourState == HeroBehaviour::BehaviourState::ENDACTION)
		length = m_EndActionSpritePtr->GetLength();
	else if (behaviourState == HeroBehaviour::BehaviourState::DIE && m_DieSpritePtr)
		length = m_DieSpritePtr->GetLength();
	else if (behaviourState == HeroBehaviour::BehaviourState::RESPAWN)
		length = m_RespawnSpritePtr->GetLength();

	// Act on states
	// Order: BehaviourState -> MovementState -> LookDirection
	switch (behaviourState)
	{
	case HeroBehaviour::BehaviourState::IDLE:
		// Key input
		TickKeyInput(deltaTime);
		break;

	case HeroBehaviour::BehaviourState::SHOOT:
		// Behaviour
		TickShoot(deltaTime, length);

		// Key input
		TickKeyInput(deltaTime);
		break;

	case HeroBehaviour::BehaviourState::THROW:
		// End of animation
		if (behAnimation > length)
			m_HeroBehaviourPtr->SetBehaviourState(HeroBehaviour::BehaviourState::IDLE);

		// Create Grenade
		if (m_ActCounter <= deltaTime)
			CreateGrenade(deltaTime);

		// Key input
		TickKeyInput(deltaTime);
		break;

	case HeroBehaviour::BehaviourState::ATTACK:
		// Begin of animation
		if (behAnimation <= deltaTime)
			GAME_INFO->GetResourceManager()->PlayAudio(IDR_KICK);

		// End of animation
		if (behAnimation > length)
			m_HeroBehaviourPtr->SetBehaviourState(HeroBehaviour::BehaviourState::IDLE);

		// Key input
		TickKeyInput(deltaTime);
		break;

	case HeroBehaviour::BehaviourState::ENDACTION:
		// End of animation
		if (behAnimation >= length)
			m_HeroBehaviourPtr->SetBehaviourState(HeroBehaviour::BehaviourState::IDLE);

		// Key input
		TickKeyInput(deltaTime);
		break;

	case HeroBehaviour::BehaviourState::DIE:
		// Behaviour
		TickDie(deltaTime);
		break;

	case HeroBehaviour::BehaviourState::RESPAWN:
		// Behaviour
		TickRespawn(deltaTime, length);
		break;
	}
}
void Hero::TickKeyInput(double deltaTime)
{
	// Shoot or attack
	if (GAME_ENGINE->IsKeyDown('Z') && m_ActCounter >= m_ActTime)
	{
		bool willAttack = false;
		// Check if any NPC is close enough 
		for (NPC* npcPtr : GAME_INFO->GetObjectManager()->GetNPCs())
		{
			if (npcPtr->DistanceTo(m_Pos).x < 40 && npcPtr->DistanceTo(m_Pos).y < 40 && npcPtr->CanHit())
			{
				willAttack = true;
				npcPtr->Hit(deltaTime);
				break;
			}
		}

		// Set to correct state
		if (willAttack)
		{
			m_HeroBehaviourPtr->SetBehaviourState(HeroBehaviour::BehaviourState::ATTACK);

			// Audio
			GAME_INFO->GetResourceManager()->PlayAudio(IDR_KICK);
		}
		else
		{
			m_HeroBehaviourPtr->SetBehaviourState(HeroBehaviour::BehaviourState::SHOOT);
			m_ShotsFired = 0;
		}
		m_ActCounter = 0;
	}


	// Throw bomb
	if (GAME_ENGINE->IsKeyDown('X') && m_ActCounter >= m_ActTime)
	{
		m_HeroBehaviourPtr->SetBehaviourState(HeroBehaviour::BehaviourState::THROW);
		m_ActCounter = 0;
	}
}
void Hero::TickShoot(double deltaTime, double length)
{
	// Create Bullet
	double shotDistance = 0.1;
	int allowedShots = 1;
	if (m_HasHeavyGun)
		allowedShots = 4;

	if (m_ActCounter <= shotDistance * allowedShots && m_ActCounter > m_ShotsFired * shotDistance)
		CreateBullet(deltaTime);

	// At given length become idle again
	if (m_HeroBehaviourPtr->GetBehaviourAnimation() > length)
		m_HeroBehaviourPtr->SetBehaviourState(HeroBehaviour::BehaviourState::IDLE);
}
void Hero::TickDie(double deltaTime)
{
	// Set Sprite to display
	if (!m_DieSpritePtr)
	{
		if (rand()%2 == 0)
			m_DieSpritePtr = m_Die1SpritePtr;
		else
			m_DieSpritePtr = m_Die2SpritePtr;
	}
	// End of animation
	if (m_HeroBehaviourPtr->GetBehaviourAnimation() > 3 && m_AmountOfLives > 0)
		m_HeroBehaviourPtr->SetBehaviourState(HeroBehaviour::BehaviourState::RESPAWN);
	else if (m_HeroBehaviourPtr->GetBehaviourAnimation() > 2 && m_AmountOfLives == 0)
		GAME_INFO->SetGameOver(true);
}
void Hero::TickRespawn(double deltaTime, double length)
{
	// Begin of Animation
	if (m_HeroBehaviourPtr->GetBehaviourAnimation() <= deltaTime)
	{
		RespawnHero();
		// Reset data
		GAME_INFO->GetEnvironment()->ResetTime();
		m_AmountOfRescues = 0;
	}
	// End of animation
	if (m_HeroBehaviourPtr->GetBehaviourAnimation() > length)
		m_HeroBehaviourPtr->SetBehaviourState(HeroBehaviour::BehaviourState::ENDACTION);
}

// Paints
//-------------------------------------------------

void Hero::PaintBody()
{
	// Get Behaviour/Movement States
	HeroBehaviour::BehaviourState behaviourState = m_HeroBehaviourPtr->GetBehaviourState();
	HeroBehaviour::MovementState movementState = m_HeroBehaviourPtr->GetMovementState();

	int behaviourIndex = (int)behaviourState;
	int movementIndex = (int)movementState;
	int lookIndex = 0;
	if (m_HeroBehaviourPtr->IsLookingDown())
		lookIndex = 2;
	else if (m_HeroBehaviourPtr->IsLookingUp())
		lookIndex = 1;

	// Set up sprite to draw
	Sprite* currentSprite = nullptr;
	if (behaviourIndex < NR_BEHAVIOUR)
		currentSprite = m_SpritePtrArr[behaviourIndex][movementIndex][lookIndex];
	else if (behaviourState == HeroBehaviour::BehaviourState::ENDACTION)
		currentSprite = m_EndActionSpritePtr;
	else if (behaviourState == HeroBehaviour::BehaviourState::DIE)
		currentSprite = m_DieSpritePtr;
	else if (behaviourState == HeroBehaviour::BehaviourState::RESPAWN)
		currentSprite = m_RespawnSpritePtr;

	// Get animation data
	double animationCount = m_HeroBehaviourPtr->GetBehaviourAnimation();

	// Special cases: Uses movement animationCount
	if ((movementState == HeroBehaviour::MovementState::JUMP || movementState == HeroBehaviour::MovementState::FALL) &&
		behaviourState == HeroBehaviour::BehaviourState::IDLE)
		animationCount = m_HeroBehaviourPtr->GetMovementAnimation();

	// Calculate matrices
	MATRIX3X2 matCenter, matFlip, matTranslate, matOffset, matTotal, matPlayer;
	matCenter.SetAsTranslate(
		currentSprite->GetCenter().x,
		-currentSprite->GetSize().y);
	matFlip.SetAsScale(m_Direction, 1);
	matTranslate.SetAsTranslate(m_Pos);

	matTotal = matCenter * matFlip * matTranslate * matOffset;
	matPlayer = matTotal;

	// PAINT
	// Death
	if (behaviourState == HeroBehaviour::BehaviourState::DIE && animationCount > currentSprite->GetLength())
	{
		// Blinking
		if ((int)(animationCount *10) %2 == 0)
			currentSprite->PaintSpriteFrame(m_BmpHeroPtr, matPlayer, currentSprite->GetNumberOfFrames()-1);
	}
	// Default
	else
		currentSprite->PaintSprite(m_BmpHeroPtr, matPlayer, animationCount, m_Direction, m_IsInvincible);
}
void Hero::PaintDebug()
{
	if (m_ShowDebug)
	{
		GAME_ENGINE->SetColor(COLOR(0,0,0));
		GAME_ENGINE->SetTransformMatrix(GetMatView());
		DOUBLE2 vector(m_Direction * cos(m_HeroBehaviourPtr->GetShootingAngle())*20, sin(m_HeroBehaviourPtr->GetShootingAngle())*20);
		DOUBLE2 end(m_Pos + vector);
		GAME_ENGINE->DrawLine(m_Pos, end);
	}
}


// Projectile creation
//-------------------------------------------------

void Hero::CreateBullet(double deltaTime)
{
	HeroBehaviour::MovementState movementState = m_HeroBehaviourPtr->GetMovementState();
	m_ShotsFired++;
	DOUBLE2 startPos, dirVector;
	startPos = m_Pos;

	// Regular Gun
	if (!m_HasHeavyGun)
	{
		// Set up startpos and direction vector
		if (m_HeroBehaviourPtr->IsLookingDown() && (movementState == HeroBehaviour::MovementState::JUMP || movementState == HeroBehaviour::MovementState::FALL))
		{
			startPos += DOUBLE2(m_Direction*2, -10);
			dirVector = DOUBLE2(0,1);
		}
		else if (m_HeroBehaviourPtr->IsLookingDown())
		{
			startPos += DOUBLE2(m_Direction*30, -16);
			dirVector = DOUBLE2(m_Direction, 0);
		}
		else if (m_HeroBehaviourPtr->IsLookingUp())
		{
			startPos += DOUBLE2(m_Direction*-1, -60);
			dirVector = DOUBLE2(0,-1);
		}
		else if (! (m_HeroBehaviourPtr->IsLookingDown() && m_HeroBehaviourPtr->IsLookingUp()))
		{
			startPos += DOUBLE2(m_Direction*30, -23);
			dirVector = DOUBLE2(m_Direction,0);
		}

		// Create Regular Bullet
		GAME_INFO->GetObjectManager()->AddProjectile(PROJ_BULLET, startPos, dirVector);

		// Start Audio
		GAME_INFO->GetResourceManager()->PlayAudio(IDR_PISTOLSHOT, 90);
	}
	else
	{
		// Get Shooting angle
		double angle = m_HeroBehaviourPtr->GetShootingAngle();
		// Add random in arc of 20 degrees.
		angle += sin((double)(rand()%35)/100) -0.17;

		dirVector.x = m_Direction * cos(angle);
		dirVector.y = sin(angle);

		// Adjust when ducking
		if (m_HeroBehaviourPtr->IsLookingDown() && !(movementState == HeroBehaviour::MovementState::JUMP || movementState == HeroBehaviour::MovementState::FALL))
		{
			dirVector.x = m_Direction;
			dirVector.y = 0;
		}

		// Offset: move higher
		startPos.y -= 14;
		// Offset: in circle
		startPos += dirVector * 50;

		// Create Heavy Machinegun Bullet
		GAME_INFO->GetObjectManager()->AddProjectile(PROJ_HBULLET, startPos, dirVector);

		// Audio
		if (m_ActCounter <= deltaTime)
		{
			GAME_INFO->GetResourceManager()->PlayAudio(IDR_HEAVYMACHINEGUNSHOTS, 90);
		}

	}
	// Subtract a bullet
	if (!(m_IsInvincible &&  m_InvincibleTimer < 0.1))
	{
		// Decrease bullet count
		if (m_AmountOfBullets > 0)
			--m_AmountOfBullets;
		if (m_AmountOfBullets == 0)
			DowngradeGun();
	}
}
void Hero::CreateGrenade(double deltaTime)
{
	// Check if there are enough bombs
	if (m_AmountOfGrenades <= 0)
		return;

	// Calculate starting position
	DOUBLE2 startPos;
	startPos = m_Pos + DOUBLE2(m_Direction*14, -30);
	GAME_INFO->GetObjectManager()->AddProjectile(PROJ_GRENADE, startPos, DOUBLE2(m_Direction * 120, -100));

	// Subtract a grenade
	if (!(m_IsInvincible &&  m_InvincibleTimer < 0.1))
		--m_AmountOfGrenades;
}

// Respawn
//-------------------------------------------------

void Hero::RespawnHero()
{
	// Invincible for 8 seconds
	m_IsInvincible = true;
	m_InvincibleTimer = 4;

	// New spawn location
	SetPos(m_Pos.x, 50);
	SetVelocity(0, 200);

	// Lives
	--m_AmountOfLives;

	// Weapons
	DowngradeGun();
	m_AmountOfGrenades = 10;

	// Reset dying animation
	m_DieSpritePtr = nullptr;
}

// Getters & Setters
//-------------------------------------------------

HeroBehaviour *Hero::GetBehaviour()
{
	return m_HeroBehaviourPtr;
}
HitRegion *Hero::GetHitRegion()
{
	return m_VehiclePtr->GetHitRegion();
}

bool Hero::HasHeavyGun()
{
	return m_HasHeavyGun;
}
void Hero::UpgradeHeavyGun(int amountOfBullets)
{
	// Upgrade from other gun
	if (!m_HasHeavyGun)
	{
		GAME_INFO->GetResourceManager()->ReleaseBitmap(m_BmpHeroPtr);
		m_BmpHeroPtr = GAME_INFO->GetResourceManager()->GetBitmap(IDB_MARCOBODYHEAVY);
		m_HasHeavyGun = true;
		m_AmountOfBullets = 0;
	}

	// Add Bullets
	m_AmountOfBullets += amountOfBullets;

	// Audio
	GAME_INFO->GetResourceManager()->StopAudio(IDR_ITEM);
	GAME_INFO->GetResourceManager()->PlayAudio(IDR_HEAVYMACHINEGUNUPGRADE);
}
void Hero::DowngradeGun()
{
	if (m_HasHeavyGun)
	{
		m_AmountOfBullets = -1;
		GAME_INFO->GetResourceManager()->ReleaseBitmap(m_BmpHeroPtr);
		m_BmpHeroPtr = GAME_INFO->GetResourceManager()->GetBitmap(IDB_MARCOBODY);
		m_HasHeavyGun = false;
	}
}
void Hero::AddBombs(int amount)
{
	m_AmountOfGrenades += amount;

	// Audio
	GAME_INFO->GetResourceManager()->StopAudio(IDR_ITEM);
	GAME_INFO->GetResourceManager()->PlayAudio(IDR_BOMBPACK);
}

bool Hero::HasDied()
{
	if (m_HeroBehaviourPtr->GetBehaviourState() == HeroBehaviour::BehaviourState::DIE 
		|| m_HeroBehaviourPtr->GetBehaviourState() == HeroBehaviour::BehaviourState::RESPAWN)
		return true;
	else
		return false;
}

int Hero::GetAmountOfLives()
{
	return m_AmountOfLives;
}
int Hero::GetAmountOfBullets()
{
	return m_AmountOfBullets;
}
int Hero::GetAmountOfGrenades()
{
	return m_AmountOfGrenades;
}
int Hero::GetAmountOfPoints()
{
	return m_AmountOfPoints;
}
void Hero::AddPoints(int amount)
{
	m_AmountOfPoints += amount;
}

int Hero::GetAmountOfRescues()
{
	return m_AmountOfRescues;
}
void Hero::AddRescue()
{
	++m_AmountOfRescues;
}

void Hero::ToggleInvincibility()
{
	m_IsInvincible = !m_IsInvincible;
	m_InvincibleTimer = 0;
}
bool Hero::IsInvincible()
{
	return m_IsInvincible;
}
