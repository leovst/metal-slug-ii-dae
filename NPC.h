#pragma once
//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//-----------------------------------------------------
// Include Files
//-----------------------------------------------------
#include "GameEngine.h"

#include "ObjectEntity.h"

//-----------------------------------------------------
// Defines
//-----------------------------------------------------
#define NPC_POWSIT 0
#define NPC_POWSTICK 1
#define NPC_POWHANG 2
#define NPC_ARAB 3

//-----------------------------------------------------
// NPC Class									
//-----------------------------------------------------
class NPC : public ObjectEntity
{
public:
	NPC(DOUBLE2 pos, int amountToSpawn, int triggerStart = -1, int triggerEnd = -1);				// Constructor
	virtual ~NPC();		// Destructor

	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------

	virtual void Tick(double deltaTime);
	virtual void Paint();

	virtual void Hit(double deltaTime) = 0;

	// Getters & Setters
	//-------------------------------------------------

	// -1 = none, 0 = POW - sit, 1 = POW - stick, 2 = POW - hang, 3 = Arab
	int GetType();
	DOUBLE2 GetStartPos();

	int GetAmountToSpawn();
	void SetAmountToSpawn(int amount);

	void GetTrigger(int &triggerStart, int &triggerEnd);
	bool WillTrigger();

	bool CanHit();
	bool HasDied();

protected: 
	//-------------------------------------------------
	// Datamembers								
	//-------------------------------------------------

	// Spawn
	int m_Type;
	DOUBLE2 m_StartPos;
	int m_AmountToSpawn;
	int m_TriggerStart, m_TriggerEnd;
	bool m_IsTriggered;

	// Movement
	int m_Direction, m_RunSpeed, m_ShuffleSpeed, m_FleeSpeed;
	bool m_FeetOnGround;

	// Animation
	double m_AnimationCount;

	// Behaviour
	bool m_CanHit, m_IsFleeing, m_DisableCollision;
	double m_JumpStartY;

	enum class BehaviourState
	{
		// General
		IDLE = 0,
		RUN,
		JUMP,
		FALL,
		ATTACK,
		FLEE,
		DIE,

		// Arab
		SHUFFLE,
		THROW,

		// POW
		BOUND,
		FREED,
		PRESENT,
		SALUTE

	} m_BehaviourState;

	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------

	void SetBehaviourState(BehaviourState behaviour);

private: 

	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------

	// Ticks
	//-------------------------------------------------

	void TickLevelCollisionHandling(double deltaTime);

	// Paints
	//-------------------------------------------------

	void PaintDebug();

	//-------------------------------------------------
	// Datamembers								
	//-------------------------------------------------


	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	NPC(const NPC& yRef);									
	NPC& operator=(const NPC& yRef);	
};

 
