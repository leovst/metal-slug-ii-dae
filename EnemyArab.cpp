//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//---------------------------
// Includes
//---------------------------
#include "EnemyArab.h"

#include "MetalSlug2.h"
#include "GameInfo.h"
#include "Sprite.h"
#include "Projectile.h"

#include <string>
#include <atlconv.h>
#include <atlstr.h>
using namespace std;

//---------------------------
// Defines
//---------------------------
//#define GAME_ENGINE (GameEngine::GetSingleton())
#define GAME_INFO (GameInfo::GetSingleton())

//---------------------------
// Constructor & Destructor
//---------------------------

EnemyArab::EnemyArab(DOUBLE2 pos, int amountToSpawn, int drop, int triggerStart, int triggerEnd)
	:NPC(pos, amountToSpawn, triggerStart, triggerEnd)
	,m_CanAttack(false)
	,m_CanThrow(false)
	,m_AmountToDrop(drop)
{
	// General
	m_Type = 3;

	// Movement
	m_RunSpeed = 60;
	m_ShuffleSpeed = 20;
	m_BehaviourState = BehaviourState::FALL;

	// Set distances
	m_AttackSpace = 50;
	m_ThrowSpace = 120;
	m_ShuffleBuffer = 2;
	m_RunBuffer = 30;

	// Bitmap
	m_BmpArabPtr = GAME_INFO->GetResourceManager()->GetBitmap(IDB_ENEMYARAB);

	// Sprites
	string sIniFile			= "Resources/SpriteInfo_EnemyArab.ini";
	m_SpriteIdlePtr			= new Sprite(sIniFile, "IDLE");
	m_SpriteShufflePtr		= new Sprite(sIniFile, "SHUFFLE");
	m_SpriteRunPtr			= new Sprite(sIniFile, "RUN");
	m_SpriteJumpPtr			= new Sprite(sIniFile, "JUMP");
	m_SpriteFallPtr			= new Sprite(sIniFile, "FALL");
	m_SpriteSummersaultPtr	= new Sprite(sIniFile, "SUMMERSAULT");
	m_SpritePrepPtr			= new Sprite(sIniFile, "PREP");
	m_SpriteAttackPtr		= new Sprite(sIniFile, "ATTACK");
	m_SpriteThrowPtr		= new Sprite(sIniFile, "THROW");
	if (rand() % 2 == 0)
		m_SpriteDiePtr		= new Sprite(sIniFile, "DIE1");
	else
		m_SpriteDiePtr		= new Sprite(sIniFile, "DIE2");

	// Hitregion
	m_HitMainPtr->CreateFromRect(-10, -38, 10, 0);
	m_HitMainPtr->SetPos(m_Pos);

}
EnemyArab::~EnemyArab()
{
	// Bitmap
	GAME_INFO->GetResourceManager()->ReleaseBitmap(m_BmpArabPtr);

	// Sprites
	delete m_SpriteIdlePtr;
	m_SpriteIdlePtr			= nullptr;
	delete m_SpriteShufflePtr;
	m_SpriteShufflePtr		= nullptr;
	delete m_SpriteRunPtr;
	m_SpriteRunPtr			= nullptr;
	delete m_SpriteJumpPtr;
	m_SpriteJumpPtr			= nullptr;
	delete m_SpriteFallPtr;
	m_SpriteFallPtr			= nullptr;
	delete m_SpriteSummersaultPtr;
	m_SpriteSummersaultPtr	= nullptr;
	delete m_SpritePrepPtr;
	m_SpritePrepPtr			= nullptr;
	delete m_SpriteAttackPtr;
	m_SpriteAttackPtr		= nullptr;
	delete m_SpriteThrowPtr;
	m_SpriteThrowPtr		= nullptr;
	delete m_SpriteDiePtr;
	m_SpriteDiePtr			= nullptr;
}

//---------------------------
// Methods - Member functions
//---------------------------

void EnemyArab::Tick(double deltaTime)
{
	// Delay on actions
	double waitTime = 0.4;

	// Set looking direction
	TickDirection(waitTime);

	// Inheritance
	NPC::Tick(deltaTime);

	// Get distance to Hero
	double currentDistance = GAME_INFO->GetHero()->DistanceTo(m_Pos).x;

	// Arab Behaviour
	switch (m_BehaviourState)
	{
	case NPC::BehaviourState::IDLE:
		// Aim behaviour
		TickAim(deltaTime, waitTime);
		break;

	case NPC::BehaviourState::RUN:
		// Aim behaviour
		TickAim(deltaTime, waitTime);
		break;

	case NPC::BehaviourState::ATTACK:
		// Behaviour
		TickAttack(deltaTime);
		break;

	case NPC::BehaviourState::DIE:
		if (m_AnimationCount <= deltaTime)
		{
			if (m_AmountToDrop > 0)
			{
				DOUBLE2 pos, cameraPos(GAME_INFO->GetEnvironment()->GetCameraPos());
				int bound = 20;
				for (int i = 0; i < m_AmountToDrop; ++i)
				{
					pos.x = rand() % (MetalSlug2::GAMEWIDTH - 2*bound) + cameraPos.x + MetalSlug2::GAMEWIDTH+ bound;
					pos.y = rand() % (MetalSlug2::GAMEHEIGHT - 2*bound) + cameraPos.y;
					GAME_INFO->GetObjectManager()->AddNPC(NPC_ARAB, pos, -1, -1, 1, 0, -1, -1);
				}
			}
		}
		// Mark for deletion after 4 seconds
		if (m_AnimationCount > 4)
			SetDeleteStatus(true);
		break;

	case NPC::BehaviourState::SHUFFLE:
		// Behaviour
		TickShuffle(deltaTime);
		// Aim behaviour
		TickAim(deltaTime, waitTime);
		break;

	case NPC::BehaviourState::THROW:
		// Behaviour
		TickThrow(deltaTime);
		break;

	default:
		break;
	}

}
void EnemyArab::Paint()
{
	int bounds = GetOutBounds();
	if (bounds < 40)
	{
		Sprite* currentSpritePtr = nullptr;

		switch (m_BehaviourState)
		{
		case NPC::BehaviourState::IDLE:
			currentSpritePtr = m_SpriteIdlePtr;
			break;
		case NPC::BehaviourState::RUN:
			currentSpritePtr = m_SpriteRunPtr;
			break;
		case NPC::BehaviourState::JUMP:
			currentSpritePtr = m_SpriteJumpPtr;
			break;
		case NPC::BehaviourState::FALL:
			currentSpritePtr = m_SpriteFallPtr;
			break;
		case NPC::BehaviourState::ATTACK:
			currentSpritePtr = m_SpriteAttackPtr;
			break;
		case NPC::BehaviourState::FLEE:
			break;
		case NPC::BehaviourState::DIE:
			currentSpritePtr = m_SpriteDiePtr;
			break;
		case NPC::BehaviourState::SHUFFLE:
			currentSpritePtr = m_SpriteShufflePtr;
			break;
		case NPC::BehaviourState::THROW:
			currentSpritePtr = m_SpriteThrowPtr;
			break;
		}

		// Calculate matrices
		MATRIX3X2 matCenter, matFlip, matTranslate, matTotal;
		matCenter.SetAsTranslate(currentSpritePtr->GetCenter().x, -currentSpritePtr->GetSize().y);
		matFlip.SetAsScale(m_Direction, 1);
		matTranslate.SetAsTranslate(m_Pos);

		matTotal = matCenter * matFlip * matTranslate;

		// Paint
		// End of dying animation
		if (m_BehaviourState == BehaviourState::DIE && m_AnimationCount > m_SpriteDiePtr->GetLength())
		{	
			if ((int)(m_AnimationCount * 10) % 5 < 2)
				currentSpritePtr->PaintSpriteFrame(m_BmpArabPtr, matTotal, currentSpritePtr->GetNumberOfFrames()-1);
		}
		// Default
		else
			currentSpritePtr->PaintSprite(m_BmpArabPtr, matTotal, m_AnimationCount, m_Direction);
	}
	// Inheritance
	NPC::Paint();
}

void EnemyArab::Hit(double deltaTime)
{
	m_CanHit = false;
	GAME_INFO->GetHero()->AddPoints(100);
	SetBehaviourState(BehaviourState::DIE);
	Tick(deltaTime);

	// Audio
	if (rand() %2 == 0)
		GAME_INFO->GetResourceManager()->PlayAudio(IDR_ARABSCREAM);
	else
		GAME_INFO->GetResourceManager()->PlayAudio(IDR_ARABUGH);
}

// Ticks	
//-------------------------------------------------

void EnemyArab::TickAim(double deltaTime, double waitTime)
{
	// Continue only if char is triggered, 
	// Hero has not died or Hero is not invincible
	if (!m_IsTriggered || GAME_INFO->GetHero()->HasDied() || GAME_INFO->GetHero()->IsInvincible())
	{
		SetBehaviourState(BehaviourState::IDLE);
		return;
	}

	// Wait a bit after behaviour change
	if (m_AnimationCount <= waitTime)
		return;

	// Defaults
	m_CanAttack = false;
	m_CanThrow = false;

	// Get distance to hero
	DOUBLE2 currentDistance = GAME_INFO->GetHero()->DistanceTo(m_Pos);

	if (currentDistance.y > 60 && m_Pos.y < GAME_INFO->GetHero()->GetPos().y)
	{
		// Height difference is too big
		if (currentDistance.x > m_AttackSpace)
			SetBehaviourState(BehaviourState::RUN);
		else
			SetBehaviourState(BehaviourState::JUMP);
	}

	else if (currentDistance.x > m_ThrowSpace + m_RunBuffer)
		SetBehaviourState(BehaviourState::RUN);

	else if ((currentDistance.x > m_AttackSpace + m_ShuffleBuffer && currentDistance.x < m_ThrowSpace - m_ShuffleBuffer) 
		|| currentDistance.x > m_ThrowSpace + m_ShuffleBuffer)
	{
		if (m_BehaviourState != BehaviourState::RUN)
			SetBehaviourState(BehaviourState::SHUFFLE);
	}

	else if (currentDistance.x > m_ThrowSpace - m_ShuffleBuffer && !GAME_INFO->GetHero()->HasDied())
	{
		if (rand()%20 == 0)
		{
			m_CanThrow = true;
			SetBehaviourState(BehaviourState::THROW);
		}
		else if (rand()%20 == 0)
			SetBehaviourState(BehaviourState::SHUFFLE);
		else
			SetBehaviourState(BehaviourState::IDLE);

	}

	else if (currentDistance.Length() < m_AttackSpace + m_ShuffleBuffer && !GAME_INFO->GetHero()->HasDied())
	{
		if (rand()%5 == 0)
		{
			m_CanAttack = true;
			SetBehaviourState(BehaviourState::ATTACK);
		}
		else
			SetBehaviourState(BehaviourState::SHUFFLE);
	}

	else
		SetBehaviourState(BehaviourState::IDLE);


}
void EnemyArab::TickDirection(double waitTime)
{
	// Wait a bit after behaviour change
	if (m_AnimationCount <= waitTime)
		return;

	switch (m_BehaviourState)
	{
	case NPC::BehaviourState::IDLE:
	case NPC::BehaviourState::RUN:
	case NPC::BehaviourState::JUMP:
	case NPC::BehaviourState::FALL:
	case NPC::BehaviourState::SHUFFLE:
		// Default: look at hero
		if (m_Pos.x < GAME_INFO->GetHero()->GetPos().x)
			m_Direction = -1;
		else
			m_Direction = 1;
		break;
	default:
		break;
	}
}

void EnemyArab::TickAttack(double deltaTime)
{
	// Hit the hero
	if (abs(m_AnimationCount - 0.2) < 0.1 && GAME_INFO->GetHero()->DistanceTo(m_Pos).Length() < m_AttackSpace)
		GAME_INFO->GetHero()->Hit();

	// End of animation
	if (m_AnimationCount > m_SpriteAttackPtr->GetLength())
		SetBehaviourState(BehaviourState::IDLE);
}
void EnemyArab::TickShuffle(double deltaTime)
{
	// Set movement direction
	if (m_AnimationCount <= deltaTime)
	{
		// Back away
		if (GAME_INFO->GetHero()->DistanceTo(m_Pos).x - m_ThrowSpace <= m_ShuffleBuffer || rand()%3 < 2)
			SetVelocity(m_Direction * m_ShuffleSpeed, m_Velocity.y);
	}
}
void EnemyArab::TickThrow(double deltaTime)
{
	// Throw Sword
	if (m_CanThrow && m_AnimationCount > 0.5 && m_AnimationCount < 0.6)
	{
		m_CanThrow = false;
		DOUBLE2 startPos(m_Pos + DOUBLE2(m_Direction*-10, -15));
		GAME_INFO->GetObjectManager()->AddProjectile(PROJ_SWORD, startPos, DOUBLE2(m_Direction * -80, -200));
	}

	// End action
	if (m_AnimationCount > m_SpriteThrowPtr->GetLength())
		SetBehaviourState(BehaviourState::IDLE);
}
