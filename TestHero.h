//-----------------------------------------------------------------
// Game File
// C++ Source - TestHero.h - version v2_13 jan 2014 
// Copyright Kevin Hoefman - kevin.hoefman@howest.be
// http://www.digitalartsandentertainment.be/
//-----------------------------------------------------------------

//-----------------------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------
// Include Files
//-----------------------------------------------------------------

#include "Resource.h"	
#include "GameEngine.h"
#include "AbstractGame.h"

// Forward declarations
class GameInfo;
class Hero;

//-----------------------------------------------------------------
// TestHero Class																
//-----------------------------------------------------------------
class TestHero : public AbstractGame, public Callable
{
public:				
	//---------------------------
	// Constructor(s)
	//---------------------------
	TestHero();

	//---------------------------
	// Destructor
	//---------------------------
	virtual ~TestHero();

	//---------------------------
	// General Methods
	//---------------------------

	void GameInitialize(HINSTANCE hInstance);
	void GameStart();				
	void GameEnd();
	//void MouseButtonAction(bool isLeft, bool isDown, int x, int y, WPARAM wParam);
	//void MouseMove(int x, int y, WPARAM wParam);
	//void CheckKeyboard();
	//void KeyPressed(TCHAR cKey);
	void GameTick(double deltaTime);
	void GamePaint(RECT rect);
	//void CallAction(Caller* callerPtr);

	// -------------------------
	// Member functions
	// -------------------------

private:
	// -------------------------
	// Member functions
	// -------------------------

	// -------------------------
	// Datamembers
	// -------------------------

	GameInfo *m_GameInfoPtr;
	Hero *m_HeroPtr;
	HitRegion *m_HitPtr;

	// -------------------------
	// Disabling default copy constructor and default assignment operator.
	// If you get a linker error from one of these functions, your class is internally trying to use them. This is
	// an error in your class, these declarations are deliberately made without implementation because they should never be used.
	// -------------------------
	TestHero(const TestHero& tRef);
	TestHero& operator=(const TestHero& tRef);
};
