//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//---------------------------
// Includes
//---------------------------
#include "NPC.h"

#include "GameInfo.h"

#include <string>
#include <sstream>
using namespace std;

//---------------------------
// Defines
//---------------------------
#define GAME_ENGINE (GameEngine::GetSingleton())
#define GAME_INFO (GameInfo::GetSingleton())

//---------------------------
// Constructor & Destructor
//---------------------------

NPC::NPC(DOUBLE2 pos, int amountToSpawn, int triggerStart, int triggerEnd)
	:ObjectEntity(pos, true)
	,m_Type(-1)
	,m_StartPos(pos)
	,m_AmountToSpawn(amountToSpawn)
	,m_TriggerStart(triggerStart)
	,m_TriggerEnd(triggerEnd)
	,m_IsTriggered(false)
	,m_Direction(1)
	,m_RunSpeed(10)
	,m_FleeSpeed(40)
	,m_FeetOnGround(false)
	,m_AnimationCount(0.0)
	,m_CanHit(true)
	,m_IsFleeing(false)
	,m_DisableCollision(false)
	,m_JumpStartY(0)
	,m_BehaviourState(BehaviourState::IDLE)
{
	// nothing to create
}
NPC::~NPC()
{
	// nothing to destroy
}

//---------------------------
// Methods - Member functions
//---------------------------

void NPC::Tick(double deltaTime)
{
	// Animation
	m_AnimationCount += deltaTime;

	// Default velocity: 0
	if (m_BehaviourState != BehaviourState::RUN && m_BehaviourState != BehaviourState::SHUFFLE)
		SetVelocity(0, m_Velocity.y);

	// Default collision
	if (m_DisableCollision)
		m_FeetOnGround = false;

	// Trigger
	if (WillTrigger())
		m_IsTriggered = true;

	switch (m_BehaviourState)
	{
	case NPC::BehaviourState::IDLE:
		// Fall
		if (m_Velocity.y > 1 && !m_FeetOnGround)
			SetBehaviourState(BehaviourState::FALL);
		break;

	case NPC::BehaviourState::RUN:
		// Move
		SetVelocity(-m_Direction * m_RunSpeed, m_Velocity.y);
		break;

	case NPC::BehaviourState::JUMP:
		// Jumpstart
		if (m_AnimationCount <= deltaTime)
		{
			m_DisableCollision = true;
			SetVelocity(m_Velocity.x, -100);
			m_JumpStartY = m_Pos.y;
		}
		// Top of jump
		if (m_Velocity.y >= 0)
			SetBehaviourState(BehaviourState::FALL);
		break;

	case NPC::BehaviourState::FALL:
		// End of fall
		if (m_FeetOnGround)
		{
			if (m_IsFleeing)
				SetBehaviourState(BehaviourState::FLEE);
			else
				SetBehaviourState(BehaviourState::IDLE);
		}

		// Reenable collsion, when past current platform
		if (m_Pos.y > m_JumpStartY + (m_Pos.y - m_HitMainPtr->GetBounds().top))
			m_DisableCollision = false;
		break;

	case NPC::BehaviourState::SHUFFLE:
		// Move
		SetVelocity(-m_Direction * m_FleeSpeed, m_Velocity.y);
		break;

	case NPC::BehaviourState::FLEE:
		// Start of animation
		if (m_AnimationCount <= deltaTime)
			m_IsFleeing = true;
		// Move
		SetVelocity(-m_Direction * m_FleeSpeed, m_Velocity.y);
		break;

	case NPC::BehaviourState::DIE:
		m_DisableCollision = false;
		break;
	}

	// Inheritance
	ObjectEntity::Tick(deltaTime);

	// Level Collision
	if (!m_DisableCollision)
		TickLevelCollisionHandling(deltaTime);

	// Fall
	if (!m_FeetOnGround && m_Velocity.y > m_Gravity.y/5 && m_BehaviourState != BehaviourState::DIE)
		SetBehaviourState(BehaviourState::FALL);

}
void NPC::Paint()
{
	// Inheritance
	ObjectEntity::Paint();

	// Debug
	if (GAME_INFO->GetShowDebug())
		PaintDebug();
}

// Ticks
//-------------------------------------------------

void NPC::TickLevelCollisionHandling(double deltaTime)
{
	m_FeetOnGround = false;

	// Check down hit (Raycast)
	DOUBLE2 startPoint, vector;
	startPoint = m_Pos;
	vector.y = (m_HitMainPtr->GetBounds().top - m_Pos.y) * deltaTime * m_Velocity.y;

	HIT hitArr[1] = {};
	int numHits = GAME_INFO->GetEnvironment()->GetHitRegion()->Raycast(startPoint, vector, hitArr, 1);

	if (numHits > 0 && m_Velocity.y >= 0 && m_Pos.y - hitArr[0].point.y < 10*m_Velocity.y*deltaTime)
	{
		double depth = (hitArr[0].lambda) * vector.Length(); 
		Translate(0, -depth);
		SetVelocity(m_Velocity.x, 0);
		m_FeetOnGround = true;
	}
}

// Paints
//-------------------------------------------------

void NPC::PaintDebug()
{
	// Calculate Matrices
	MATRIX3X2 matTranslate, matOffset, matScale, matTotal;
	matScale.SetAsScale(0.3);
	matTranslate.SetAsTranslate(m_Pos);
	if (m_HitMainPtr->Exists())
		matOffset.SetAsTranslate(-10, m_HitMainPtr->GetBounds().top - m_Pos.y - 20);
	else
		matOffset.SetAsTranslate(-10, -40 -15);

	matTotal = matScale * matTranslate * matOffset * GetMatView();

	GAME_ENGINE->SetColor(COLOR(0,0,0));
	GAME_ENGINE->SetTransformMatrix(matTotal);

	// Create info
	stringstream sInfo;
	sInfo << "Pos: " << m_Pos.x << "; " << m_Pos.y << endl;
	sInfo << "Beh: " << (int)m_BehaviourState << "; " << m_AnimationCount << endl;

	// Draw
	GAME_ENGINE->DrawString(sInfo.str(), 0, 0);

	// Trigger area
	if (m_TriggerStart > 0 && m_TriggerEnd > 0 && !(m_IsTriggered && m_AmountToSpawn <= 1))
	{
		GAME_ENGINE->SetColor(COLOR(0,0,255,50));
		GAME_ENGINE->SetTransformMatrix(GetMatView());
		GAME_ENGINE->FillRect(m_TriggerStart,0, m_TriggerEnd, 1000);

	}
}

// Getters & Setters
//-------------------------------------------------

int NPC::GetType()
{
	return m_Type;
}
DOUBLE2 NPC::GetStartPos()
{
	return m_StartPos;
}

int NPC::GetAmountToSpawn()
{
	return m_AmountToSpawn;
}
void NPC::SetAmountToSpawn(int amount)
{
	m_AmountToSpawn = amount;
}

void NPC::GetTrigger(int &triggerStart, int &triggerEnd)
{
	triggerStart = m_TriggerStart;
	triggerEnd = m_TriggerEnd;
}
bool NPC::WillTrigger()
{
	// Don't trigger whit invincible Hero
	if (GAME_INFO->GetHero()->IsInvincible())
		return false;

	// If Hero within trigger boundaries
	if (GAME_INFO->GetHero()->GetPos().x > m_TriggerStart && GAME_INFO->GetHero()->GetPos().x < m_TriggerEnd)
		return true;

	// If no trigger area specified
	if (m_TriggerStart < 0 || m_TriggerEnd < 0)
		return true;

	return false;
}

bool NPC::CanHit()
{
	return m_CanHit;
}
bool NPC::HasDied()
{
	if (m_BehaviourState == BehaviourState::DIE)
		return true;

	return false;
}

void NPC::SetBehaviourState(BehaviourState behaviour)
{
	// Reset Animation Count
	if (!(m_BehaviourState == BehaviourState::JUMP && behaviour == BehaviourState::FALL) && !(m_BehaviourState == behaviour))
		m_AnimationCount = 0;

	// Set Behaviour state
	m_BehaviourState = behaviour;
}
