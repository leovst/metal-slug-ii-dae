//-----------------------------------------------------------------
// Game File
// C++ Source - TestResource.cpp - version v2_13 jan 2014 
// Copyright Kevin Hoefman - kevin.hoefman@howest.be
// http://www.digitalartsandentertainment.be/
//-----------------------------------------------------------------

//-----------------------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------------------

//-----------------------------------------------------------------
// Include Files
//-----------------------------------------------------------------
#include "TestResource.h"																				

//-----------------------------------------------------------------
// Defines
//-----------------------------------------------------------------
#define GAME_ENGINE (GameEngine::GetSingleton())

//-----------------------------------------------------------------
// TestResource methods																				
//-----------------------------------------------------------------

TestResource::TestResource()
	:m_ResourcePtr(nullptr)
	,m_BtnAddBananaPtr(nullptr)
	,m_BtnAddBombPtr(nullptr)
	,m_BtnAddCatPtr(nullptr)
	,m_BtnReleaseBananaPtr(nullptr)
	,m_BtnReleaseBombPtr(nullptr)
	,m_BtnReleaseCatPtr(nullptr)
{
	// nothing to create
}

TestResource::~TestResource()																						
{
	// nothing to destroy
}

void TestResource::GameInitialize(HINSTANCE hInstance)			
{
	// Set the required values
	AbstractGame::GameInitialize(hInstance);
	GAME_ENGINE->SetTitle("TestResource - Vansteenkiste Leo - 1DAE6");					
	GAME_ENGINE->RunGameLoop(false);
	
	// Set the optional values
	GAME_ENGINE->SetWidth(640);
	GAME_ENGINE->SetHeight(480);
	//GAME_ENGINE->SetKeyList(String("QSDZ") + (TCHAR) VK_SPACE);
	//GAME_ENGINE->ConsoleCreate();
	//GAME_ENGINE->LockFramerateToVerticalSynchronisation(true);
}

void TestResource::GameStart()
{
	m_ResourcePtr = new ResourceManager();

	m_BtnAddBananaPtr = CreateButton(20, 20, 200, "Add Item");
	m_BtnAddBombPtr = CreateButton(20, 45, 200, "Add Explosion");
	m_BtnAddCatPtr = CreateButton(20, 70, 200, "Add HUD");

	m_BtnReleaseBananaPtr = CreateButton(220, 20, 200, "Release Items");
	m_BtnReleaseBombPtr = CreateButton(220, 45, 200, "Release Explosion");
	m_BtnReleaseCatPtr = CreateButton(220, 70, 200, "Release HUD");
}

void TestResource::GameEnd()
{
	delete m_ResourcePtr;

	delete m_BtnAddBananaPtr;
	delete m_BtnAddBombPtr;
	delete m_BtnAddCatPtr;

	delete m_BtnReleaseBananaPtr;
	delete m_BtnReleaseBombPtr;
	delete m_BtnReleaseCatPtr;
}

//void TestResource::MouseButtonAction(bool isLeft, bool isDown, int x, int y, WPARAM wParam)
//{	
//	// Insert the code that needs to be executed when the game registers a mouse button action
//
//	/* Example:
//	if (isLeft == true && isDown == true) // is it a left mouse press?
//	{	
//		if ( x > 261 && x < 261 + 117 ) // check if press lies within x coordinates of choice
//		{
//			if ( y > 182 && y < 182 + 33 ) // check if press also lies within y coordinates of choice
//			{
//				GAME_ENGINE->MessageBox("Clicked.");
//			}
//		}
//	}
//	*/
//}
//
//void TestResource::MouseMove(int x, int y, WPARAM wParam)
//{	
//	// Insert the code that needs to be executed when the mouse pointer moves across the game window
//
//	/* Example:
//	if ( x > 261 && x < 261 + 117 ) // check if mouse position is within x coordinates of choice
//	{
//		if ( y > 182 && y < 182 + 33 ) // check if mouse position also is within y coordinates of choice
//		{
//			GAME_ENGINE->MessageBox("Da mouse wuz here.");
//		}
//	}
//	*/
//
//}
//
//void TestResource::CheckKeyboard()
//{	
//	// Here you can check if a key of choice is held down
//	// Is executed once per frame when the gameloop is running
//
//	/* Example:
//	if (GAME_ENGINE->IsKeyDown('K')) xIcon -= xSpeed;
//	if (GAME_ENGINE->IsKeyDown('L')) yIcon += xSpeed;
//	if (GAME_ENGINE->IsKeyDown('M')) xIcon += xSpeed;
//	if (GAME_ENGINE->IsKeyDown('O')) yIcon -= ySpeed;
//	*/
//}
//
//void TestResource::KeyPressed(TCHAR cKey)
//{	
//	// DO NOT FORGET to use SetKeyList() !!
//
//	// Insert the code that needs to be executed when a key of choice is pressed
//	// Is executed as soon as the key is released
//	// You first need to specify the keys that the game engine needs to watch by using the SetKeyList() method
//
//	/* Example:
//	switch (cKey)
//	{
//	case 'K': case VK_LEFT:
//		MoveBlock(DIR_LEFT);
//		break;
//	case 'L': case VK_DOWN:
//		MoveBlock(DIR_DOWN);
//		break;
//	case 'M': case VK_RIGHT:
//		MoveBlock(DIR_RIGHT);
//		break;
//	case 'A': case VK_UP:
//		RotateBlock();
//		break;
//	case VK_ESCAPE:
//	}
//	*/
//}
//
//void TestResource::GameTick(double deltaTime)
//{
//	// Insert the code that needs to be executed, EXCEPT for paint commands (see next method)
//}
//
void TestResource::GamePaint(RECT rect)
{
	GAME_ENGINE->DrawString(String("Check output debug"), 20, 100);

}

void TestResource::CallAction(Caller* callerPtr)
{
	if (callerPtr == m_BtnAddBananaPtr)
		m_ResourcePtr->GetBitmap(IDB_ITEMS);
	if (callerPtr == m_BtnReleaseBananaPtr)
		m_ResourcePtr->ReleaseBitmap(IDB_ITEMS);

	if (callerPtr == m_BtnAddBombPtr)
		m_ResourcePtr->GetBitmap(IDB_EXPLOSION);
	if (callerPtr == m_BtnReleaseBombPtr)
		m_ResourcePtr->ReleaseBitmap(IDB_EXPLOSION);

	if (callerPtr == m_BtnAddCatPtr)
		m_ResourcePtr->GetBitmap(IDB_HUD);
	if (callerPtr == m_BtnReleaseCatPtr)
		m_ResourcePtr->ReleaseBitmap(IDB_HUD);
}




Button* TestResource::CreateButton(int xPos, int yPos, int width, String text)
{
	Button *btnReturnPtr = new Button(text);
	btnReturnPtr->AddActionListener(this);
	btnReturnPtr->SetBounds(xPos, yPos, width, 25);
	btnReturnPtr->Show();

	return btnReturnPtr;
}
