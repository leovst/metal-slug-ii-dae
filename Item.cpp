//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//---------------------------
// Includes
//---------------------------
#include "Item.h"

#include "GameInfo.h"

//---------------------------
// Defines
//---------------------------
//#define GAME_ENGINE (GameEngine::GetSingleton())
#define GAME_INFO (GameInfo::GetSingleton())

//---------------------------
// Constructor & Destructor
//---------------------------

Item::Item(DOUBLE2 pos, int content)
	:ObjectEntity(pos, true)
	,m_IsTriggered(false)
	,m_AnimationCount(0.0)
	,m_Content(content)
{
	// Nothing to create
}
Item::~Item()
{
	// Nothing to delete
}

//---------------------------
// Methods - Member functions
//---------------------------

void Item::Tick(double deltaTime)
{
	// Level Collision
	if (m_PhysicsEnabled)
		TickLevelCollisionHandling(deltaTime);

	// Animation
	m_AnimationCount += deltaTime;

	if (!m_IsTriggered)
		TickPlayerCollisionHandling();

	// Audio


	// Inheritance
	ObjectEntity::Tick(deltaTime);
}
void Item::Paint()
{
	// Inheritance
	ObjectEntity::Paint();
}


// Collision
//-------------------------------------------------

void Item::TickPlayerCollisionHandling()
{
	// Check horizontal hit (AABB)
	RECT2 hitHero, hitItem;
	hitHero = GAME_INFO->GetHero()->GetHitRegion()->GetBounds();
	hitItem = m_HitMainPtr->GetBounds();

	if (RectIntersectTest(hitHero, hitItem))
	{
		// Start Audio
		GAME_INFO->GetResourceManager()->PlayAudio(IDR_ITEM);

		Hit();
		m_AnimationCount = 0.0;

	}
}
void Item::TickLevelCollisionHandling(double deltaTime)
{
	DOUBLE2 vector = (m_Velocity + m_Gravity * deltaTime) * deltaTime;
	HIT hitArr[1];

	if (GAME_INFO->GetEnvironment()->GetHitRegion()->Raycast(m_Pos, vector, hitArr, 1, 0))
	{
		// Set position to hit point
		SetPos(hitArr[0].point);

		// Set to hit ground
		m_PhysicsEnabled = false;
		m_Velocity.y = 0;

	}

}
bool Item::RectIntersectTest(RECT2 r1, RECT2 r2)
{
	return !(r1.right < r2.left || r1.left > r2.right || 
		r1.top > r2.bottom || r1.bottom < r2.top);
}
