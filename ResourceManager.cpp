//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//---------------------------
// Includes
//---------------------------
#include "ResourceManager.h"

using namespace std;

//---------------------------
// Defines
//---------------------------
//#define GAME_ENGINE (GameEngine::GetSingleton())

//---------------------------
// Constructor & Destructor
//---------------------------

ResourceManager::ResourceManager()
	:m_AudioIsMuted(false)
{
	// Nothing to create
}
ResourceManager::~ResourceManager()
{
	// Delete Bitmaps
	for (const pair<int, Bitmap*> &bmpRef : m_BitmapArr)
		delete bmpRef.second;

	// Delete Audio objects
	for (const pair<int, Audio*> &audioRef : m_AudioArr)
		delete audioRef.second;
}


//---------------------------
// Methods - Member functions
//---------------------------

void ResourceManager::Tick()
{
	// Play Audio objects
	for (const pair<int, Audio*> &audioRef : m_AudioArr)
		if (audioRef.second->IsPlaying())
			audioRef.second->Tick();
}


// Bitmaps
//-------------------------------------------------

// Get bitmap pointer
Bitmap* ResourceManager::GetBitmap(int resourceID)
{
	// If entry does not exist
	if (m_BitmapArr.find(resourceID) == m_BitmapArr.end())
	{
		// Create Bitmap
		m_BitmapArr[resourceID] = new Bitmap(resourceID);
		m_BitmapFlipArr[m_BitmapArr[resourceID]] = resourceID;
		// Create reference count
		m_BitmapReferenceArr[resourceID] = 0;
	}
	// Add reference Count
	++m_BitmapReferenceArr[resourceID];

	// Return Bitmap
	return m_BitmapArr[resourceID];
}

// Decrease reference count and delete if was last reference
void ResourceManager::ReleaseBitmap(int resourceID)
{
	// If entry does not exist
	if (m_BitmapArr.find(resourceID) == m_BitmapArr.end())
		return;

	// Decrease reference
	--m_BitmapReferenceArr[resourceID];

	// Check if it was last reference
	if (m_BitmapReferenceArr[resourceID] == 0)
	{
		m_BitmapFlipArr.erase(m_BitmapArr[resourceID]);
		delete m_BitmapArr[resourceID];
		m_BitmapArr.erase(resourceID);
		m_BitmapReferenceArr.erase(resourceID);
	}
}

// Decrease reference count and delete if was last reference
// Uses flipped stl-map
void ResourceManager::ReleaseBitmap(Bitmap *bmpPtr)
{
	// If entry exists
	if (m_BitmapFlipArr.find(bmpPtr) != m_BitmapFlipArr.end())
		ReleaseBitmap(m_BitmapFlipArr[bmpPtr]);
}

// Audio
//-------------------------------------------------

//Audio* ResourceManager::GetAudio(int resourceID)
//{
//	// If entry does not exist
//	if (m_AudioArr.find(resourceID) == m_AudioArr.end())
//	{
//		// Create Audio object
//		m_AudioArr[resourceID] = new Audio(resourceID, "MP3");
//		m_AudioArr[resourceID]->SetVolume(40);
//		m_AudioFlipArr[m_AudioArr[resourceID]] = resourceID;
//		// Create reference count
//		m_AudioReferencArr[resourceID] = 0;
//	}
//	// Add reference Count
//	++m_AudioReferencArr[resourceID];
//
//	// Return Audio Object
//	return m_AudioArr[resourceID];
//}
//void ResourceManager::ReleaseAudio(int resourceID)
//{
//	// If entry does not exist
//	if (m_AudioArr.find(resourceID) == m_AudioArr.end())
//		return;
//
//	// Decrease reference
//	--m_AudioReferencArr[resourceID];
//
//	// Check if it was last reference
//	if (m_AudioReferencArr[resourceID] == 0)
//	{
//		m_AudioFlipArr.erase(m_AudioArr[resourceID]);
//		delete m_AudioArr[resourceID];
//		m_AudioArr.erase(resourceID);
//		m_BitmapReferenceArr.erase(resourceID);
//	}
//}
//void ResourceManager::ReleaseAudio(Audio* audioPtr)
//{
//	// If entry exists
//	if (m_AudioFlipArr.find(audioPtr) != m_AudioFlipArr.end())
//		ReleaseAudio(m_AudioFlipArr[audioPtr]);
//}

void ResourceManager::PlayAudio(int resourceID, int volume, bool repeat)
{
	// If entry does not exist
	if (m_AudioArr.find(resourceID) == m_AudioArr.end())
	{
		// Create Audio object
		m_AudioArr[resourceID] = new Audio(resourceID, "MP3");
		if (m_AudioIsMuted)
			m_AudioArr[resourceID]->SetVolume(0);
		else
			m_AudioArr[resourceID]->SetVolume(volume);

		m_VolumeArr[resourceID] = volume;
		if (repeat)
			m_AudioArr[resourceID]->SetRepeat(repeat);
	}

	// Stop if it was already playing
	m_AudioArr[resourceID]->Stop();

	m_AudioArr[resourceID]->Play();
}
void ResourceManager::StopAudio(int resourceID)
{
	m_AudioArr[resourceID]->Play();
	m_AudioArr[resourceID]->Stop();
}
void ResourceManager::MuteAudio()
{
	for (const pair<int, Audio*> &audioRef : m_AudioArr)
		audioRef.second->SetVolume(0);

	m_AudioIsMuted = true;
}
void ResourceManager::UnMuteAudio()
{
	for (const pair<int, Audio*> &audioRef : m_AudioArr)
		audioRef.second->SetVolume(m_VolumeArr[audioRef.first]);

	m_AudioIsMuted = false;
}
void ResourceManager::ToggleMuteAudio()
{
	if (m_AudioIsMuted)
		UnMuteAudio();
	else
		MuteAudio();
}
