//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//---------------------------
// Includes
//---------------------------
#include "Projectile.h"
#include "GameInfo.h"
#include "NPC.h"
#include "EnvironmentObject.h"

//---------------------------
// Defines
//---------------------------
#define GAME_ENGINE (GameEngine::GetSingleton())
#define GAME_INFO (GameInfo::GetSingleton())

//---------------------------
// Constructor & Destructor
//---------------------------
Projectile::Projectile(DOUBLE2 pos, bool physicsEnabled, int strength)
	:ObjectEntity(pos, physicsEnabled)
	,m_sIniFile("Resources/SpriteInfo_Projectiles.ini")
	,m_HasHit(false)
	,m_Type(-1)
	,m_Strength(strength)
{
	// Bitmap
	m_BmpProjectilesPtr = GAME_INFO->GetResourceManager()->GetBitmap(IDB_PROJECTILES);
}

Projectile::~Projectile()
{
	// Bitmap
	GAME_INFO->GetResourceManager()->ReleaseBitmap(m_BmpProjectilesPtr);
	m_BmpProjectilesPtr = nullptr;
}

//---------------------------
// Methods - Member functions
//---------------------------

void Projectile::Tick(double deltaTime)
{
	// Mark for deletion
	int boundLimit = 5;
	if (m_Type == PROJ_GRENADE || m_Pos.y < GAME_INFO->GetEnvironment()->GetCameraPos().y)
		boundLimit = 40;
	if (GetOutBounds() > boundLimit || GetHasHit())
		SetDeleteStatus(true);

	if (m_CanDelete && m_Type != PROJ_GRENADE)
		return;

	// Collision
	if (m_Type == PROJ_BULLET || m_Type == PROJ_HBULLET || m_Type == PROJ_GRENADE)
	{
		TickNPCCollisionHandling(deltaTime);
		TickEnvironmentCollisionHandling(deltaTime);
	}

	ObjectEntity::Tick(deltaTime);
}
void Projectile::Paint()
{
	// Inheritance
	ObjectEntity::Paint();
}

// Ticks
//-------------------------------------------------

void Projectile::TickNPCCollisionHandling(double deltaTime)
{
	// Basic collision based on position
	for(NPC* npcPtr : GAME_INFO->GetObjectManager()->GetNPCs())
	{
		if (npcPtr->CanHit() && npcPtr->GetHitRegion()->HitTest(m_Pos))
		{
			npcPtr->Hit(deltaTime);
			m_HasHit = true;
			if (m_Type != PROJ_GRENADE)
				return;
		}
	}
}
void Projectile::TickEnvironmentCollisionHandling(double deltaTime)
{
	// Environment Objects
	for (EnvironmentObject* eoPtr : GAME_INFO->GetObjectManager()->GetEnvironmentObjects())
	{
		if (eoPtr->GetOutBounds() < 100 && eoPtr->canHit() && eoPtr->GetHitRegion()->HitTest(m_Pos))
		{
			eoPtr->Hit(m_Strength);
			m_HasHit = true;
		}
	}
}

// Getters
//-------------------------------------------------

bool Projectile::GetHasHit()
{
	return m_HasHit;
}
int Projectile::GetType()
{
	return m_Type;
}

