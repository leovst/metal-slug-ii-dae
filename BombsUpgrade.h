#pragma once
//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//-----------------------------------------------------
// Include Files
//-----------------------------------------------------
#include "GameEngine.h"

#include "Item.h"

// Forward declaration
class Sprite;

//-----------------------------------------------------
// BombsUpgrade Class									
//-----------------------------------------------------
class BombsUpgrade : public Item
{
public:
	BombsUpgrade(DOUBLE2 pos, int amountOfBombs);				// Constructor
	virtual ~BombsUpgrade();		// Destructor

	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------

	void Tick(double deltaTime);
	void Paint();

	void Hit();


private: 
	//-------------------------------------------------
	// Datamembers								
	//-------------------------------------------------
	
	// Sprites
	Sprite *m_UpgradeSpritePtr, *m_DisappearSpritePtr, *m_GhostSpritePtr;
	Bitmap *m_BmpUpgradesPtr;

	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	BombsUpgrade(const BombsUpgrade& yRef);									
	BombsUpgrade& operator=(const BombsUpgrade& yRef);	
};

 
