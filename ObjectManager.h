#pragma once
//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//-----------------------------------------------------
// Include Files
//-----------------------------------------------------
#include "GameEngine.h"

#include <vector>
#include <list>

// Forward Declaration
class NPC;
class Item;
class Projectile;
class EnvironmentObject;

//-----------------------------------------------------
// ObjectManager Class									
//-----------------------------------------------------
class ObjectManager
{
public:
	ObjectManager();				// Constructor
	virtual ~ObjectManager();		// Destructor

	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------

	void Tick(double deltaTime);
	void Paint();

	// Read in initialization file
	bool ReadIniFile(std::string sFileName);

	// Add Projectiles
	void AddProjectile(int type, DOUBLE2 startPos, DOUBLE2 dirVector = DOUBLE2(0,0));

	// Add Items
	Item* CreateItem(int type, DOUBLE2 pos, int content);
	void AddItem(int type, DOUBLE2 pos, int content);
	void AddItem(Item* itemPtr);

	// Add NPC
	void AddNPC(int type, DOUBLE2 pos, int triggerS, int triggerE, int amount, int drop, int itemNr=-1, int content=-1);

	// Add Environment Object
	void AddEnvironmentObject(int type, DOUBLE2 pos);

	// Getters	
	//-------------------------------------------------

	DOUBLE2 GetHeroStartPosition();
	std::vector<NPC *> GetNPCs();
	std::vector<EnvironmentObject*> GetEnvironmentObjects();

private: 
	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------

	// Ticks
	//-------------------------------------------------

	void TickProjectiles(double deltaTime);
	void TickItems(double deltaTime);
	void TickNPCs(double deltaTime);

	void TickKeyInput();

	// File & String parsing
	//-------------------------------------------------

	bool ParseSectionString(std::string sLine, int &kind, int &type);
	bool ParseVariableString(std::string sLine, DOUBLE2 &pos, int &triggerS, int &triggerE, int &itemNr, int &content, int &amount, int &drop);

	//-------------------------------------------------
	// Datamembers								
	//-------------------------------------------------

	// Hero starting pos
	DOUBLE2 m_HeroStartPos;

	// Projectile Objects
	std::vector<Projectile*> m_ProjectilesPtrArr;

	// Upgrades & Items
	std::vector<Item*> m_ItemsPtrArr;

	// NPC's
	std::vector<NPC*> m_NpcPtrArr;

	// Environment Objects
	std::vector<EnvironmentObject*> m_EoPtrArr;

	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	ObjectManager(const ObjectManager& yRef);									
	ObjectManager& operator=(const ObjectManager& yRef);	
};


