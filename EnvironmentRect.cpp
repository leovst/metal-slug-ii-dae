//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//---------------------------
// Includes
//---------------------------
#include "EnvironmentRect.h"

//---------------------------
// Defines
//---------------------------
//#define GAME_ENGINE (GameEngine::GetSingleton())
//#define GAME_INFO (GameInfo::GetSingleton())

//---------------------------
// Constructor & Destructor
//---------------------------
EnvironmentRect::EnvironmentRect(RECT hit)
	:Environment()
{
	m_HitMainPtr->CreateFromRect(hit.left, hit.top, hit.right, hit.bottom);
	MATRIX3X2 matRotate;
	matRotate.SetAsRotate(0.3);
	m_HitMainPtr->SetTransformMatrix(matRotate);
}

EnvironmentRect::~EnvironmentRect()
{
	// nothing to destroy
}

//---------------------------
// Methods - Member functions
//---------------------------

// Add here the methods - Member functions


