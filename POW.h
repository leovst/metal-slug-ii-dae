#pragma once
//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//-----------------------------------------------------
// Include Files
//-----------------------------------------------------
#include "GameEngine.h"

#include "NPC.h"

// Forward Declarations
class Sprite;
class Item;
class HelperObject;

//-----------------------------------------------------
// POW Class									
//-----------------------------------------------------
class POW : public NPC
{
public:
	POW(DOUBLE2 pos, int kind, Item* itemPtr);				// Constructor
	virtual ~POW();		// Destructor

	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------
	
	void Tick(double deltaTime);
	void Paint();

	void Hit(double deltaTime);

private: 

	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------
	
	// Ticks		
	//-------------------------------------------------

	void TickHitregion(double deltaTime);
	void TickHelper(double deltaTime);
	void TickAim(double deltaTime, double waitTime);

	void TickRun(double deltaTime);
	void TickBound(double deltaTime);
	void TickFreed(double deltaTime);
	void TickPresent(double deltaTime);

	//-------------------------------------------------
	// Datamembers								
	//-------------------------------------------------

	// Behaviour
	int m_Kind;
	HelperObject* m_HelperPtr;
	int m_PresentSpace;
	bool m_IsWaving;

	// Content
	Item* m_ItemPtr;

	// Bitmap
	Bitmap* m_BmpPowPtr;

	// Sprites
	Sprite 
		*m_BoundSitSpritePtr, *m_FreedSitSpritePtr, 
		*m_BoundStickSpritePtr, *m_BoundWaveSpritePtr, *m_FreedStickSpritePtr, *m_StickDisappearSpritePtr,
		*m_BoundHangSpritePtr, *m_RopeSpritePtr, *m_RopeDisappearSpritePtr,
		*m_RunSpritePtr, *m_FleeSpritePtr,
		*m_PresentSpritePtr, *m_SaluteSpritePtr,
		*m_FallSpritePtr, *m_LookSpritePtr;



	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	POW(const POW& yRef);									
	POW& operator=(const POW& yRef);	
};

 
