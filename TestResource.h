//-----------------------------------------------------------------
// Game File
// C++ Source - TestResource.h - version v2_13 jan 2014 
// Copyright Kevin Hoefman - kevin.hoefman@howest.be
// http://www.digitalartsandentertainment.be/
//-----------------------------------------------------------------

//-----------------------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------
// Include Files
//-----------------------------------------------------------------

#include "Resource.h"	
#include "GameEngine.h"
#include "AbstractGame.h"

#include "ResourceManager.h"

//-----------------------------------------------------------------
// TestResource Class																
//-----------------------------------------------------------------
class TestResource : public AbstractGame, public Callable
{
public:				
	//---------------------------
	// Constructor(s)
	//---------------------------
	TestResource();

	//---------------------------
	// Destructor
	//---------------------------
	virtual ~TestResource();

	//---------------------------
	// General Methods
	//---------------------------

	void GameInitialize(HINSTANCE hInstance);
	void GameStart();				
	void GameEnd();
	//void MouseButtonAction(bool isLeft, bool isDown, int x, int y, WPARAM wParam);
	//void MouseMove(int x, int y, WPARAM wParam);
	//void CheckKeyboard();
	//void KeyPressed(TCHAR cKey);
	//void GameTick(double deltaTime);
	void GamePaint(RECT rect);
	void CallAction(Caller* callerPtr);

	// -------------------------
	// Member functions
	// -------------------------

private:
	// -------------------------
	// Member functions
	// -------------------------
	
	Button* CreateButton(int xPos, int yPos, int width, String text);

	// -------------------------
	// Datamembers
	// -------------------------

	ResourceManager *m_ResourcePtr;
	Button *m_BtnAddBananaPtr, *m_BtnAddCatPtr, *m_BtnAddBombPtr;
	Button *m_BtnReleaseBananaPtr, *m_BtnReleaseCatPtr, *m_BtnReleaseBombPtr;


	// -------------------------
	// Disabling default copy constructor and default assignment operator.
	// If you get a linker error from one of these functions, your class is internally trying to use them. This is
	// an error in your class, these declarations are deliberately made without implementation because they should never be used.
	// -------------------------
	TestResource(const TestResource& tRef);
	TestResource& operator=(const TestResource& tRef);
};
