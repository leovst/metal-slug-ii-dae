//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//---------------------------
// Includes
//---------------------------
#include "GameInfo.h"
#include "ObjectEntity.h"

#include "Mission1.h"
#include "EnvironmentRect.h"
#include "HUD.h"

//---------------------------
// Defines
//---------------------------
#define GAME_ENGINE (GameEngine::GetSingleton())

//---------------------------
// Singleton
//---------------------------
GameInfo* GameInfo::m_SingletonPtr = nullptr;

GameInfo* GameInfo::GetSingleton()
{
	if(!m_SingletonPtr)
		m_SingletonPtr = new GameInfo();
	return m_SingletonPtr;
}
void GameInfo::Release()
{
	delete m_SingletonPtr;
	m_SingletonPtr = nullptr;
}

//---------------------------
// Constructor & Destructor
//---------------------------

GameInfo::GameInfo()
	:m_ShowDebug(false)
	,m_IsGameOver(false)
{
	// General
	m_ResourceManagerPtr = nullptr;
	m_ObjectManagerPtr = nullptr;
	m_EnvironmentPtr = nullptr;
	m_HeroPtr = nullptr;
	m_HudPtr = nullptr;
}

GameInfo::~GameInfo()
{

	delete m_HudPtr;
	m_HudPtr = nullptr;

	delete m_HeroPtr;
	m_HeroPtr = nullptr;

	delete m_EnvironmentPtr;
	m_EnvironmentPtr = nullptr;

	delete m_ObjectManagerPtr;
	m_ObjectManagerPtr = nullptr;

	delete m_ResourceManagerPtr;
	m_ResourceManagerPtr = nullptr;
}

//---------------------------
// Methods - Member functions
//---------------------------

// Getters & Setters
//-------------------------------------------------

ResourceManager* GameInfo::GetResourceManager()
{
	return m_ResourceManagerPtr;
}
void GameInfo::AttachResourceManager(ResourceManager *resourcePtr)
{
	m_ResourceManagerPtr = resourcePtr;
}

Hero* GameInfo::GetHero()
{
	return m_HeroPtr;
}
void GameInfo::AttachHero(Hero *heroPtr)
{
	m_HeroPtr = heroPtr;
}

Environment* GameInfo::GetEnvironment()
{
	return m_EnvironmentPtr;
}
void GameInfo::AttachEnvironment(Environment *missionPtr)
{
	m_EnvironmentPtr = missionPtr;
}

ObjectManager* GameInfo::GetObjectManager()
{
	return m_ObjectManagerPtr;
}
void GameInfo::AttachObjectManager(ObjectManager *managerPtr)
{
	m_ObjectManagerPtr = managerPtr;
}

HUD* GameInfo::GetHUD()
{
	return m_HudPtr;
}
void GameInfo::AttachHUD(HUD* hudPtr)
{
	m_HudPtr = hudPtr;
}

bool GameInfo::GetGameOver()
{
	return m_IsGameOver;
}
void GameInfo::SetGameOver(bool isGameOver)
{
	m_IsGameOver = isGameOver;
}

// Debug
//---------------------------

bool GameInfo::GetShowDebug()
{
	return m_ShowDebug;
}
void GameInfo::ToggleDebug()
{
	m_ShowDebug = !m_ShowDebug;

	// Set for all objects
	ObjectEntity::SetDebug(m_ShowDebug);
}
