#pragma once
//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//-----------------------------------------------------
// Include Files
//-----------------------------------------------------
#include "GameEngine.h"

// Forward declarations
class Sprite;
class Hero;

//-----------------------------------------------------
// HUD Class									
//-----------------------------------------------------
class HUD
{
public:
	HUD();				// Constructor
	virtual ~HUD();		// Destructor

	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------

	void Tick(double deltaTime);
	void Paint();

private: 
	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------

	//Paints	
	//-------------------------------------------------

	void PaintWeaponFrame(MATRIX3X2 &matFrame);
	void PaintHeroData(const MATRIX3X2 &matFrame);
	void PaintTime(const MATRIX3X2 &matFrame);

	void PaintDebug();

	//-------------------------------------------------
	// Datamembers								
	//-------------------------------------------------

	// Bitmap
	Bitmap* m_BmpHudPtr;

	// Sprites
	Sprite* m_WeaponsFrameSpritePtr, *m_RescuedSpritePtr, *m_InfiniteSpritePtr, *m_GoRightSpritePtr;


	// Hero (for easy access)
	Hero* m_HeroPtr;

	// Display info
	int m_AmountOfBullets, m_AmountOfGrenades, m_AmountOfLives, m_AmountOfPoints, m_AmountOfRescues;
	int m_TimeLeft;

	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	HUD(const HUD& yRef);									
	HUD& operator=(const HUD& yRef);	
};

 
