#pragma once
//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//-----------------------------------------------------
// Include Files
//-----------------------------------------------------
#include "GameEngine.h"

#include <map>

//-----------------------------------------------------
// ResourceManager Class									
//-----------------------------------------------------
class ResourceManager
{
public:
	ResourceManager();				// Constructor
	virtual ~ResourceManager();		// Destructor

	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------
	
	void Tick();

	// Bitmaps
	//-------------------------------------------------

	Bitmap* GetBitmap(int resourceID);
	void ReleaseBitmap(int resourceID);
	void ReleaseBitmap(Bitmap* bmpPtr);
	
	// Audio
	//-------------------------------------------------

	//Audio* GetAudio(int resourceID);
	//void ReleaseAudio(int resourceID);
	//void ReleaseAudio(Audio* audioPtr);

	void PlayAudio(int resourceID, int volume = 40, bool repeat = false);
	void StopAudio(int resourceID);
	void MuteAudio();
	void UnMuteAudio();
	void ToggleMuteAudio();

private: 
	//-------------------------------------------------
	// Datamembers								
	//-------------------------------------------------
	
	
	// Bitmaps
	//-------------------------------------------------
	std::map<int, Bitmap*> m_BitmapArr;
	// Flipped map for searching Bitmap pointers
	std::map<Bitmap*, int> m_BitmapFlipArr;
	std::map<int, int> m_BitmapReferenceArr;
	
	// Audio
	//-------------------------------------------------
	std::map<int, Audio*> m_AudioArr;
	// Flipped map for searching Audio pointers
	//std::map<Audio*, int> m_AudioFlipArr;
	//std::map<int, int> m_AudioReferencArr;
	std::map<int, int> m_VolumeArr;

	bool m_AudioIsMuted;

	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	ResourceManager(const ResourceManager& yRef);									
	ResourceManager& operator=(const ResourceManager& yRef);	
};

 
