//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//---------------------------
// Includes
//---------------------------
#include "HeavyBullet.h"

#include "Sprite.h"

//---------------------------
// Defines
//---------------------------
#define GAME_ENGINE (GameEngine::GetSingleton())
//#define GAME_INFO (GameInfo::GetSingleton())

//---------------------------
// Constructor & Destructor
//---------------------------

HeavyBullet::HeavyBullet(DOUBLE2 startPos, DOUBLE2 dirVector)
	:Bullet(startPos, dirVector)
{
	m_HeavyBulletSpritePtr = new Sprite(m_sIniFile, "HEAVYBULLET");

	// Get frame index
	double angle = acos(abs(dirVector.x));
	m_FrameIndex = (int)((angle/M_PI)*32)%16;

	// General vars
	m_Type = PROJ_HBULLET;
}
HeavyBullet::~HeavyBullet()
{
	delete m_HeavyBulletSpritePtr;
	m_HeavyBulletSpritePtr = nullptr;
}

//---------------------------
// Methods - Member functions
//---------------------------

void HeavyBullet::Tick(double deltaTime)
{
	// Inheritance
	Bullet::Tick(deltaTime);
}
void HeavyBullet::Paint()
{
	// Calculate matrices
	MATRIX3X2 matCenter, matHFlip, matVFlip, matRotate, matTranslate, matTotal;

	matCenter.SetAsTranslate(-2, -2);
	if (m_DirectionVector.x > 0)
		matHFlip.SetAsScale(-1, 1);
	if (m_DirectionVector.y > 0)
		matVFlip.SetAsScale(1, -1);

	if (abs(m_DirectionVector.y) == 1)
		if (m_DirectionVector.x > 0)
			matRotate.SetAsRotate(m_DirectionVector.y * M_PI/2);
		else
			matRotate.SetAsRotate(-m_DirectionVector.y * M_PI/2);

	matTranslate.SetAsTranslate(m_Pos);

	matTotal = matHFlip * matVFlip * matRotate * matTranslate;

	// Paint
	m_HeavyBulletSpritePtr->PaintSpriteFrame(m_BmpProjectilesPtr, matCenter * matTotal, m_FrameIndex);
}



