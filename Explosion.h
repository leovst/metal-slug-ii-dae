#pragma once
//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//-----------------------------------------------------
// Include Files
//-----------------------------------------------------
#include "GameEngine.h"

#include "Projectile.h"

// Forward declarations
class Sprite;

//-----------------------------------------------------
// Explosion Class									
//-----------------------------------------------------
class Explosion : public Projectile
{
public:
	Explosion(DOUBLE2 pos);				// Constructor
	virtual ~Explosion();		// Destructor

	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------

	void Tick(double deltaTime);
	void Paint();

private: 
	//-------------------------------------------------
	// Datamembers								
	//-------------------------------------------------

	// Bitmap
	Bitmap *m_BmpExplosionPtr;

	// Animation
	Sprite *m_ExplosionSpritePtr;
	double m_AnimationCount;


	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	Explosion(const Explosion& yRef);									
	Explosion& operator=(const Explosion& yRef);	
};

 
