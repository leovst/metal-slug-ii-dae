//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//---------------------------
// Includes
//---------------------------
#include "ObjectEntity.h"

#include "GameInfo.h"
#include "MetalSlug2.h"
#include "Environment.h"

//---------------------------
// Defines
//---------------------------
#define GAME_ENGINE (GameEngine::GetSingleton())
#define GAME_INFO (GameInfo::GetSingleton())

//---------------------------
// Constructor & Destructor
//---------------------------
//DOUBLE2 ObjectEntity::m_CameraPos = DOUBLE2(0,0);
bool ObjectEntity::m_ShowDebug = false;

ObjectEntity::ObjectEntity(DOUBLE2 pos, bool physicsEnabled)
	:m_Pos(pos)
	,m_Gravity(0, 300)
	,m_PhysicsEnabled(physicsEnabled)
	//,m_IsOutBounds(false)
	,m_HitMainPtr(nullptr)
	,m_CanDelete(false)
{
	m_HitMainPtr = new HitRegion();
}
ObjectEntity::~ObjectEntity()
{
	if (m_HitMainPtr != nullptr)
		delete m_HitMainPtr;
}

//---------------------------
// Methods - Member functions
//---------------------------

void ObjectEntity::Tick(double deltaTime)
{
	// Gravity
	if (m_PhysicsEnabled)
		m_Velocity += m_Gravity * deltaTime;

	// General movement
	Translate(m_Velocity * deltaTime);

	// Bounds
	TickCheckBounds();
}
void ObjectEntity::Paint()
{
	// Debug
	if (m_ShowDebug)
	{
		if (m_HitMainPtr != nullptr)
		{
			if (m_HitMainPtr->Exists())
			{
				GAME_ENGINE->SetColor(COLOR(255,0,0,127));
				GAME_ENGINE->SetTransformMatrix(GetMatView());
				GAME_ENGINE->FillHitRegion(m_HitMainPtr);
			}
		}
		MATRIX3X2 matTranslate, matTotal;
		matTranslate.SetAsTranslate(m_Pos);
		matTotal = matTranslate * GetMatView();

		GAME_ENGINE->SetColor(COLOR(255,0,0));
		GAME_ENGINE->SetTransformMatrix(matTotal);
		GAME_ENGINE->FillEllipse(0, 0, 2, 2);
	}
}

// Ticks
//-------------------------------------------------

int ObjectEntity::TickCheckBounds()
{
	// Get camera data
	DOUBLE2 cameraPos = GAME_INFO->GetEnvironment()->GetCameraPos();
	RECT2 screen;
	screen.left = cameraPos.x;
	screen.top = cameraPos.y;
	screen.right = screen.left + MetalSlug2::GAMEWIDTH;
	screen.bottom = screen.top + MetalSlug2::GAMEHEIGHT;

	int distanceOutside = -1;

	if (m_Pos.x < cameraPos.x || m_Pos.x > cameraPos.x + MetalSlug2::GAMEWIDTH)
		distanceOutside = (int)max(cameraPos.x - m_Pos.x, m_Pos.x - (cameraPos.x + MetalSlug2::GAMEWIDTH));
	else if(m_Pos.y < cameraPos.y || m_Pos.y > cameraPos.y + MetalSlug2::GAMEHEIGHT)
		distanceOutside = (int)max(cameraPos.y - m_Pos.y, m_Pos.y - (cameraPos.y + MetalSlug2::GAMEHEIGHT));

	return distanceOutside;
}

// Getters & Setters	
//-------------------------------------------------

// Position

DOUBLE2 ObjectEntity::GetPos()
{
	return m_Pos;
}
void ObjectEntity::SetPos(double x, double y)
{
	SetPos(DOUBLE2(x, y));
}
void ObjectEntity::SetPos(DOUBLE2 pos)
{
	m_Pos = pos;
	// Hitregion
	if (m_HitMainPtr != nullptr)
		if (m_HitMainPtr->Exists())
			m_HitMainPtr->SetPos(pos);
}
void ObjectEntity::Translate(double dx, double dy)
{
	Translate(DOUBLE2(dx,dy));
}
void ObjectEntity::Translate(DOUBLE2 d)
{
	m_Pos += d;
	// Hitregion
	if (m_HitMainPtr != nullptr)
		if (m_HitMainPtr->Exists())
			m_HitMainPtr->Move(d.x,d.y);
}

// Distance
DOUBLE2 ObjectEntity::DistanceTo(DOUBLE2 other)
{
	return DOUBLE2(abs(m_Pos.x - other.x), abs(m_Pos.y - other.y));
}

// Movement

DOUBLE2 ObjectEntity::GetVelocity()
{
	return m_Velocity;
}
void ObjectEntity::SetVelocity(double vx, double vy)
{
	m_Velocity = DOUBLE2(vx, vy);
}

// Visibility

int ObjectEntity::GetOutBounds()
{
	return TickCheckBounds();
}

// HitRegion

HitRegion *ObjectEntity::GetHitRegion()
{
	HitRegion *hitPtr = nullptr;
	if (m_HitMainPtr != nullptr)
		hitPtr = m_HitMainPtr;
	return hitPtr;
}

// View Matrix

MATRIX3X2 ObjectEntity::GetMatView()
{
	return GAME_INFO->GetEnvironment()->GetMatView();
}

// Deletion

bool ObjectEntity::GetDeleteStatus()
{
	return m_CanDelete;
}
void ObjectEntity::SetDeleteStatus(bool deleteStatus)
{
	m_CanDelete = deleteStatus;
}

// Debug
//-------------------------------------------------

void ObjectEntity::SetDebug(bool showDebug)
{
	m_ShowDebug = showDebug;
	//if (m_ShowDebug)
	//	GAME_ENGINE->ConsoleCreate();
}
bool ObjectEntity::GetDebug()
{
	return m_ShowDebug;
}
