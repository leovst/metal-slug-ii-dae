#pragma once
//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//-----------------------------------------------------
// Include Files
//-----------------------------------------------------
#include "GameEngine.h"

#include "Projectile.h"

// Forward declarations
class Sprite;

//-----------------------------------------------------
// Grenade Class									
//-----------------------------------------------------
class Grenade : public Projectile
{
public:
	Grenade(DOUBLE2 pos, DOUBLE2 dirVector);				// Constructor
	virtual ~Grenade();		// Destructor

	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------
	
	void Tick(double deltaTime);
	void Paint();

private: 

	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------
	
	//Ticks	
	//-------------------------------------------------

	void TickNPCCollisionHandling(double deltaTime);
	bool TickLevelCollisionHandling(double deltaTime);

	//-------------------------------------------------
	// Datamembers								
	//-------------------------------------------------
	
	// Animation
	Sprite *m_GrenadeSpritePtr;
	double m_AnimationCount;

	// General vars
	bool m_hasHitGroundOnce;

	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	Grenade(const Grenade& yRef);									
	Grenade& operator=(const Grenade& yRef);	
};

 
