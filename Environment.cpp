//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//---------------------------
// Includes
//---------------------------
#include "Environment.h"

#include "MetalSlug2.h"
#include "GameInfo.h"
#include "Sprite.h"

#include <string>
using namespace std;

//---------------------------
// Defines
//---------------------------
#define GAME_ENGINE (GameEngine::GetSingleton())
#define GAME_INFO (GameInfo::GetSingleton())

//---------------------------
// Constructor & Destructor
//---------------------------

Environment::Environment()
	:ObjectEntity(DOUBLE2(0,0), false)
	,m_BmpEnvironmentPtr(nullptr)
	,m_AnimationCount(0.0)
	,m_AnimationAngle(0.0)
	,m_TimeLeft(MISSION_TIME)
	,m_Timer(0.0)
	,m_IsCameraStopped(false)
{

}
Environment::~Environment()
{
	// Nothing to destroy
}

//---------------------------
// Methods - Member functions
//---------------------------

void Environment::Tick(double deltaTime)
{
	// Animation
	m_AnimationCount += deltaTime;
	m_AnimationAngle += deltaTime * M_PI;

	// Camera
	TickCamera(deltaTime);

	// Calculate ime left
	TickTimer(deltaTime);
}
void Environment::Paint()
{
	// Inheritance
	ObjectEntity::Paint();
}

// Ticks						
//-------------------------------------------------

void Environment::TickCamera(double deltaTime)
{

	// Follow player
	if ((GAME_ENGINE->IsKeyDown(VK_LEFT) || GAME_ENGINE->IsKeyDown(VK_RIGHT)) && !m_IsCameraStopped)
	{
		// Check left
		if (GAME_INFO->GetHero()->GetPos().x - m_CameraPos.x < 80)
			m_NewCameraPos.x = GAME_INFO->GetHero()->GetPos().x - 80;
		// Check right
		if (GAME_INFO->GetHero()->GetPos().x - m_CameraPos.x > 150)
			m_NewCameraPos.x = GAME_INFO->GetHero()->GetPos().x - 150;
	}
	// Ease in/out
	m_CameraPos += (m_NewCameraPos - m_CameraPos) * 0.05;

	// Limit to bounds
	double lastPoint = 10000;
	lastPoint = 3200;
	if (m_CameraPos.x < 0)
		m_CameraPos.x = 0;
	if (m_CameraPos.x > min(m_HitMainPtr->GetBounds().right, lastPoint) - MetalSlug2::GAMEWIDTH)
		m_CameraPos.x = min(m_HitMainPtr->GetBounds().right, lastPoint) - MetalSlug2::GAMEWIDTH;
}
void Environment::TickTimer(double deltaTime)
{
	// Time left
	m_Timer += deltaTime;
	
	if (m_TimeLeft == 0)
		GAME_INFO->GetHero()->Hit();
	else if (m_Timer >= 5)
	{
		--m_TimeLeft;
		m_Timer = 0;
	}
}

// Special Paints
//-------------------------------------------------

void Environment::PaintParallax(Sprite *spritePtr, double parallaxSpeed, DOUBLE2 offset)
{
	// Get amount of sprites to fit inside the screen
	// + make array with amount
	int spriteWidth = (int)spritePtr->GetSize().x;
	int repeated = 2 + 304 / spriteWidth;
	DOUBLE2 *pos = new DOUBLE2[repeated];

	// See where to start to paint depending on camera position
	int cameraOffset = (int)((m_CameraPos.x - m_CameraPos.x * parallaxSpeed) / spriteWidth);

	// Set and paint each sprite in array
	for (int i = 0; i < repeated; ++i)
	{
		pos[i].x = offset.x + cameraOffset * spriteWidth + i * spriteWidth + m_CameraPos.x * parallaxSpeed;
		pos[i].y = offset.y + m_CameraPos.y;

		MATRIX3X2 mat;
		mat.SetAsTranslate(pos[i]);
		spritePtr->PaintSprite(m_BmpEnvironmentPtr, mat, m_AnimationCount);
	}

	// Delete
	delete[] pos;
}

// Getters & Setters
//-------------------------------------------------

DOUBLE2 Environment::GetCameraPos()
{
	return m_CameraPos;
}
DOUBLE2 Environment::GetNewCameraPos()
{
	return m_NewCameraPos;
}
void Environment::TranslateCamera(double dx, double dy)
{
	TranslateCamera(DOUBLE2(dx,dy));
}
void Environment::TranslateCamera(DOUBLE2 d)
{
	m_NewCameraPos += d;
}

MATRIX3X2 Environment::GetMatView()
{
	MATRIX3X2 matCamera, matScale, matView;
	matCamera.SetAsTranslate(GetCameraPos());
	matScale.SetAsScale(MetalSlug2::GAMESCALE);
	matView = matCamera.Inverse() * matScale;
	return matView;
}

int Environment::GetTimeLeft()
{
	return m_TimeLeft;
}
void Environment::ResetTime()
{
	m_TimeLeft = MISSION_TIME;
	m_Timer = 0;
}