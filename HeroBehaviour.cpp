//-----------------------------------------------------
// Name, first name: Vansteenkiste Leo
// Group: 1DAE6
//-----------------------------------------------------

//---------------------------
// Includes
//---------------------------
#include "HeroBehaviour.h"

#include "GameInfo.h"

//---------------------------
// Defines
//---------------------------
#define GAME_ENGINE (GameEngine::GetSingleton())
#define GAME_INFO (GameInfo::GetSingleton())

//---------------------------
// Constructor & Destructor
//---------------------------

HeroBehaviour::HeroBehaviour()
	:m_MovementState(MovementState::STILL)
	,m_BehaviourState(BehaviourState::IDLE)
	,m_ShootingAngle(0.0)
	,m_MovementAnimation(0.0)
	,m_BehaviourAnimation(0.0)
{
	// nothing to create
}
HeroBehaviour::~HeroBehaviour()
{
	// nothing to destroy
}

//---------------------------
// Methods - Member functions
//---------------------------

void HeroBehaviour::Tick(double deltaTime)
{
	// Animation
	m_MovementAnimation += deltaTime;
	m_BehaviourAnimation += deltaTime;

	// Key input
	TickKeyInput(deltaTime);
}

// Ticks
//-------------------------------------------------

void HeroBehaviour::TickKeyInput(double deltaTime)
{
	// Adjust looking direction
	double angleSpeed = 6; // Abrupt change for Regular Gun
	if (GAME_INFO->GetHero()->HasHeavyGun())
		angleSpeed = 1; // Gradual change for Heavy gun

	// Key Input
	if (GAME_ENGINE->IsKeyDown(VK_UP))
	{
		if (m_ShootingAngle > -M_PI/2)
			m_ShootingAngle -= deltaTime * M_PI * angleSpeed;
	}
	else if (GAME_ENGINE->IsKeyDown(VK_DOWN))
	{
		if (m_ShootingAngle < M_PI/2)
			m_ShootingAngle += deltaTime * M_PI * angleSpeed;
	}
	else if (abs(m_ShootingAngle) > 0.02)
	{
		if (m_ShootingAngle > 0)
			m_ShootingAngle -= deltaTime * M_PI * angleSpeed;
		if (m_ShootingAngle < 0)
			m_ShootingAngle += deltaTime * M_PI * angleSpeed;
	}

	// Set angle to default values when overshooting
	if (abs(m_ShootingAngle) < 0.02)
		m_ShootingAngle = 0;
	if (m_ShootingAngle > M_PI/2-0.02)
		m_ShootingAngle = M_PI/2;
	if (m_ShootingAngle < -M_PI/2+0.02)
		m_ShootingAngle = -M_PI/2;
}

// Getters & Setters
//-------------------------------------------------

HeroBehaviour::MovementState HeroBehaviour::GetMovementState()
{
	return m_MovementState;
}
void HeroBehaviour::SetMovementState(MovementState movement)
{
	ResetMovementAnimation();
	// Will not continue endaction behaviour
	if (m_BehaviourState == BehaviourState::ENDACTION)
		m_BehaviourState = BehaviourState::IDLE;
	// Evolve in end action
	if (movement == MovementState::STILL && !IsLookingDown())
		SetBehaviourState(BehaviourState::ENDACTION);
	m_MovementState = movement;
}

HeroBehaviour::BehaviourState HeroBehaviour::GetBehaviourState()
{
	return m_BehaviourState;
}
void HeroBehaviour::SetBehaviourState(BehaviourState behaviour)
{
	ResetBehaviourAnimation();
	m_BehaviourState = behaviour;

}

double HeroBehaviour::GetShootingAngle()
{
	return m_ShootingAngle;
}
bool HeroBehaviour::IsLookingDown()
{
	if (m_ShootingAngle > 0.1)
		return true;
	else
		return false;
}
bool HeroBehaviour::IsLookingUp()
{
	if (m_ShootingAngle < -0.1)
		return true;
	else
		return false;
}

double HeroBehaviour::GetMovementAnimation()
{
	return m_MovementAnimation;
}
void HeroBehaviour::ResetMovementAnimation()
{
	m_MovementAnimation = 0;
}
double HeroBehaviour::GetBehaviourAnimation()
{
	return m_BehaviourAnimation;
}
void HeroBehaviour::ResetBehaviourAnimation()
{
	m_BehaviourAnimation = 0;
}
